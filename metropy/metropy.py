import mtpker as krnl
import numpy as np
import ctypes

class Frame:

    def __init__(self, sc):
        self.__num_of_atoms = sc.contents.num_of_atoms
        self.__num_of_types = sc.contents.num_of_types
        self.temperature = sc.contents.temperature

        self.x = np.zeros(self.__num_of_atoms)
        self.y = np.zeros(self.__num_of_atoms)
        self.z = np.zeros(self.__num_of_atoms)
        self.energies = np.zeros(self.__num_of_atoms)
        self.frozen = np.zeros(self.__num_of_atoms, dtype=np.intc)
        self.types = np.zeros(self.__num_of_atoms, dtype=np.intc)

        self.names = [None] * self.__num_of_types
        self.molmasses = np.zeros(self.__num_of_types)

        self.__dic = []

        for i in range(0, self.__num_of_types):
            self.molmasses[i] = sc.contents.molmasses[i]
            self.names[i] = sc.contents.names[i].decode()
            self.__dic.append(0)

        for i in range(0, self.__num_of_atoms):
            self.x[i] = sc.contents.atom_positions[sc.contents.atom_adresses[i]]
            self.y[i] = sc.contents.atom_positions[sc.contents.atom_adresses[i]+1]
            self.z[i] = sc.contents.atom_positions[sc.contents.atom_adresses[i]+2]
            self.energies[i] = sc.contents.energies[i]
            self.frozen[i] = sc.contents.frozen[i]
            self.types[i] = sc.contents.types[i]
            self.__dic[sc.contents.types[i]] += 1  

        self.energy = np.sum(self.energies)   

    def get_num_of_atoms(self):
        return self.__num_of_atoms

    def get_num_of_types(self):
        return self.__num_of_types

    def get_num_of_atoms_of_type(self, type):
        if isinstance(type,str):
            index = self.names.index(type)
            return self.__dic[index]

        return self.__dic[type]

    def get_energy_per_atoms(self):
        return np.sum(self.energies)/self.__num_of_atoms

    def save_to_xyz(self, filename, append = False):
        sc = krnl.sceneinfo_alloc()
        sc.contents.num_of_atoms = self.__num_of_atoms
        sc.contents.num_of_types = self.__num_of_types
        sc.contents.temperature = self.temperature

        positions = np.empty(self.__num_of_atoms * 3)
        atom_adresses = np.empty(self.__num_of_atoms, dtype=int)
        
        for i in range(0, self.__num_of_atoms):
            atom_adresses[i] = (i << 1) + i
            positions[atom_adresses[i]] = self.x[i]
            positions[atom_adresses[i] + 1] = self.y[i]
            positions[atom_adresses[i] + 2] = self.z[i]

        sc.contents.atom_positions = positions.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        sc.contents.atom_adresses = atom_adresses.ctypes.data_as(ctypes.POINTER(ctypes.c_size_t))
        sc.contents.energies = self.energies.ctypes.data_as(ctypes.POINTER(ctypes.c_double))
        sc.contents.frozen = self.frozen.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        sc.contents.types = self.types.ctypes.data_as(ctypes.POINTER(ctypes.c_int))
        
        length = len(self.names)

        select_type = (ctypes.c_char_p * length)
        names = select_type()

        for key, item in enumerate(self.names):
            names[key] = item.encode()

        sc.contents.names = names

        if append:
           krnl.sceneinfo_append_to_xyz(sc, filename.encode())
        else:
           krnl.sceneinfo_save_to_xyz(sc, filename.encode())

    def free(self):
        del(self.x)
        del(self.y)
        del(self.z)
        del(self.energies)
        del(self.types)
        del(self.frozen)
        del(self.names)
        del(self.molmasses)

def get_num_of_frames(filename):
    fname = filename.encode()
    return krnl.get_num_of_frames(fname)

def load_frames(filename, predicate):
    fname = filename.encode()
    frames = []
    finfo = krnl.finfo_create(fname, b'rb')

    if finfo:
        sc = krnl.load_next(finfo)
        while sc:
            cl = Frame(sc)
            if predicate(cl):
                frames.append(cl)
            else:
                cl.free()
            krnl.sceneinfo_free(sc)
            sc = krnl.load_next(finfo)

    krnl.finfo_destroy(finfo)
    return frames

def load_frames_range(filename, start, finish):
       fname = filename.encode()
       frames = []
       finfo = krnl.finfo_create(fname, b'rb')
       cur = 0

       if finfo:
           sc = krnl.load_next(finfo)    

           while sc:
              cl = Frame(sc)
              if cur >= start and cur < finish:
                 frames.append(cl)
              else:
                 cl.free()
              krnl.sceneinfo_free(sc)
              sc = krnl.load_next(finfo)
              cur += 1

       krnl.finfo_destroy(finfo)
       return frames

def append_to_xyz(frames, filename):
    for f in frames:
        f.save_to_xyz(filename, True)
        

def free_frames(frames):
    for frame in frames:
        frame.free()