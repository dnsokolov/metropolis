import ctypes
import os

class SceneInfo(ctypes.Structure):
	_fields_ = [
		("num_of_atoms", ctypes.c_size_t),
		("num_of_types", ctypes.c_size_t),
		("atom_positions", ctypes.POINTER(ctypes.c_double)),
		("atom_adresses", ctypes.POINTER(ctypes.c_size_t)),
		("types", ctypes.POINTER(ctypes.c_int)),
		("names", ctypes.POINTER(ctypes.c_char_p)),
		("molmasses", ctypes.POINTER(ctypes.c_double)),
		("frozen", ctypes.POINTER(ctypes.c_int)),
		("energies", ctypes.POINTER(ctypes.c_double)),
		("temperature", ctypes.c_double),
		("potflag", ctypes.c_uint),

		("step_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("harm_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("lj_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("morse_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("fs_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("exfs_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("gupta_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("sc_adress_matrix", ctypes.POINTER(ctypes.c_size_t)),

		("step_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("harm_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("lj_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("morse_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("gupta_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("fs_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),
		("exfs_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),	
		("sc_bond_matrix", ctypes.POINTER(ctypes.c_size_t)),

		("step_parameters", ctypes.POINTER(ctypes.c_double)),
		("harm_parameters", ctypes.POINTER(ctypes.c_double)),
		("lj_parameters", ctypes.POINTER(ctypes.c_double)),
		("morse_parameters", ctypes.POINTER(ctypes.c_double)),
		("gupta_parameters", ctypes.POINTER(ctypes.c_double)),
		("fs_parameters", ctypes.POINTER(ctypes.c_double)),
		("exfs_parameters", ctypes.POINTER(ctypes.c_double)),
		("sc_parameters", ctypes.POINTER(ctypes.c_double))
		]

class FileInfo(ctypes.Structure):
	_fields_ = [
		("fname", ctypes.c_char_p),
		("fptr", ctypes.c_void_p)
	]


lib = ctypes.CDLL(os.path.join(os.path.dirname(__file__), "libmtpker.so"))

finfo_create = lib.finfo_create
finfo_create.restype = ctypes.POINTER(FileInfo)
finfo_create.argtypes = [
	ctypes.c_char_p, 
	ctypes.c_char_p
	]

finfo_destroy = lib.finfo_destroy
finfo_destroy.argtypes = [ctypes.POINTER(FileInfo)]

finfo_rewind = lib.finfo_rewind
finfo_rewind.argtypes = [ctypes.POINTER(FileInfo)]

load_next = lib.fi_sceneinfo_load_next
load_next.restype = ctypes.POINTER(SceneInfo)
load_next.argtypes = [ctypes.POINTER(FileInfo)]

sceneinfo_print = lib.sceneinfo_print
sceneinfo_print.argtypes = [ctypes.POINTER(SceneInfo)]

sceneinfo_dump = lib.sceneinfo_dump
sceneinfo_dump.argtypes = [ctypes.POINTER(SceneInfo), ctypes.c_char_p]

sceneinfo_free = lib.sceneinfo_free
sceneinfo_free.argtypes = [ctypes.POINTER(SceneInfo)]

sceneinfo_get_energy = lib.sceneinfo_get_energy
sceneinfo_get_energy.restype = ctypes.c_double
sceneinfo_get_energy.argtypes = [ctypes.POINTER(SceneInfo)]

sceneinfo_save_to_xyz = lib.sceneinfo_save_to_xyz
sceneinfo_save_to_xyz.argtypes = [ctypes.POINTER(SceneInfo), ctypes.c_char_p]

sceneinfo_append_to_xyz = lib.sceneinfo_append_to_xyz
sceneinfo_append_to_xyz.argtypes = [ctypes.POINTER(SceneInfo), ctypes.c_char_p]

get_num_of_frames = lib.get_num_of_frames
get_num_of_frames.restype = ctypes.c_ulong
get_num_of_frames.argtypes = [ctypes.c_char_p]

sceneinfo_alloc = lib.sceneinfo_alloc
sceneinfo_alloc.restype = ctypes.POINTER(SceneInfo)
