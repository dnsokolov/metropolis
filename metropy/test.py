import metropy

frames = metropy.load_frames("/home/denis/calculations/Фазовые диаграммы Co-Au 1 итер/1.15 (копия)/Co0075_Au0025_Ag0000_[S00000_T0001]-3.6133[k=1.15]/Co0075_Au0025_Ag0000_[S00000_T0001]-3.6133[k=1.15]_dump.ens", lambda f: f.temperature == 100.0)

sorted_frames = sorted(frames, key = lambda frame: frame.energy)
clusters = sorted_frames[0:5]
metropy.append_to_xyz(clusters, "test.xyz")