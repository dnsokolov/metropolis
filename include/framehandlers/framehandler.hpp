#ifndef FRAMEHANDLER_HPP
#define FRAMEHANDLER_HPP

#include <string>
#include "sceneinfo.h"

class IFrameHandler {
    private:
    std::string _name;

    public:
    IFrameHandler(const std::string &name) : _name(name) {}

    std::string GetName() const {
        return _name;
    }

    virtual void operator() (SceneInfo *sc_info) = 0;
    virtual void End() = 0;
};

#endif