#ifndef COORD_NUMBER_FRAMENAHDLER_HPP
#define COORD_NUMBER_FRAMEHANDLER_HPP

#include "framehandler.hpp"
#include "utils.hpp"
#include <map>
#include <vector>
#include <utility>
#include <cstring>

class CoordNumberFrameHandler : public IFrameHandler {
    private:
    double _r;
    std::map<double,std::pair<double,double>> ZT;
    bool flag{false};
    double temperature{0.0};
    std::vector<double> z;
    
    public:
    CoordNumberFrameHandler(double r) : _r(r), IFrameHandler("Coordination Number") {}
    void operator() (SceneInfo *sc_info) override;
    void End() override;
    void GetZT(std::map<double,std::pair<double,double>> &zt) const;
};

#endif