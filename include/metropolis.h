#ifndef _METROPOLIS_H_
#define _METROPOLIS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "dSFMT/dSFMT.h"
#include "sceneinfo.h"
#include "runtimeaver.h"

#ifdef ENABLE_AVX
#include <immintrin.h>
#endif

#ifndef MTP_PI
#define MTP_PI 3.14159265358979323846
#endif

#ifndef MTP_TWO_PI
#define MTP_TWO_PI 6.283185307179586
#endif

#define kB 8.6173303E-5 
#define SQRT_R 0.9118367517353773

#define RND(min,max) genrand_close_open() * ((max) - (min)) + (min)

typedef struct MetropolisSimulation_tag {
    SceneInfo *scene;
    double max_translation;
    double time;
	double sigma;
    int maxwell_mode;
} MetropolisSimulation;

void mtp_run_simulation (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*get_energy)(int a, SceneInfo *sc_info)
);

void mtp_run_simulation_without_temperature (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*get_energy)(int a, SceneInfo *sc_info)
);

void mtp_run_simulation_with_dumping (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*get_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
);

void mtp_run_simulation_without_temperature_with_dumping (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*get_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
);

/*** Benchmarking version ***/

void mtp_run_simulation_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*get_energy)(int a, SceneInfo *sc_info)
);

void mtp_run_simulation_without_temperature_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*get_energy)(int a, SceneInfo *sc_info)
);

void mtp_run_simulation_with_dumping_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*get_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
);

void mtp_run_simulation_without_temperature_with_dumping_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*get_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
);

#ifdef __cplusplus
}
#endif


#ifndef ENABLE_AVX
#define TRANSLATE(p, tr) \
   (*p)     += (tr[0]);\
   (*(p+1)) += (tr[1]);\
   (*(p+2)) += (tr[2]); 

#define TRANSLATE_BACK(p, tr) \
   (*p)     -= (tr[0]);\
   (*(p+1)) -= (tr[1]);\
   (*(p+2)) -= (tr[2]);

#else
#define TRANSLATE(p, tr) \
   mm_pos = _mm256_load_pd((p));\
   mm_trans = _mm256_load_pd((tr));\
   mm_sum = _mm256_add_pd((mm_pos),(mm_trans));\
   _mm256_store_pd((p),(mm_sum));

#define TRANSLATE_BACK(p, tr) \
   mm_pos = _mm256_load_pd((p));\
   mm_trans = _mm256_load_pd((tr));\
   mm_sum = _mm256_sub_pd((mm_pos),(mm_trans));\
   _mm256_store_pd((p),(mm_sum));
#endif

#endif // METROPLIS_H