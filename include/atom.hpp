#ifndef _ATOM_H_
#define _ATOM_H_

#include <string>
#include <vector>
#include <iostream>

typedef struct Atom_tag {
    std::string ename;
    double x;
    double y;
    double z;
    double energy;
    double molmass;
    int freeze;
    std::vector<std::string> column_values;

    friend std::ostream& operator<< (std::ostream& out, const Atom_tag &atom);
} Atom;

std::ostream& operator<< (std::ostream &out, const Atom &atom);

#endif