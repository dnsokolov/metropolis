#ifndef _POTENTIALARRAY_H_
#define _POTENTIALARRAY_H_

#include "adressmatrix.hpp"

class PotentialArray
{
private:
    double *data;
    size_t size;
    size_t num_of_types;
    size_t num_of_params;

    AdressMatrix *adress_matrix;
    AdressMatrix *bond_matrix;

public:
    PotentialArray(size_t num_of_types, size_t num_of_params) {
        size = (num_of_types + num_of_types * (num_of_types - 1) / 2) * num_of_params;
        data = new double[size];

        adress_matrix = new AdressMatrix(num_of_types);
        adress_matrix->FillParamAdresses(num_of_params);

        bond_matrix = new AdressMatrix(num_of_types);

        this->num_of_params = num_of_params;
        this->num_of_types = num_of_types;

        for(size_t i = 0; i < size; ++i) {
            data[i] = 0.0;
        }
    }

    void AddParams(int comp1, int comp2, const double *params) {
        if(comp1 < num_of_types && comp2 < num_of_types) {
            size_t start = adress_matrix->Get(comp1, comp2);
            (*bond_matrix)(comp1, comp2) = 1;
            (*bond_matrix)(comp2, comp1) = 1;
            for(size_t i = 0; i < num_of_params; ++i) {
                data[start + i] = params[i];
            }
        }
    }
	
	void DeleteComponent(int comp) {
		size_t nnum_of_types = num_of_types - 1;
		size_t nsize = (nnum_of_types + nnum_of_types * (nnum_of_types - 1) / 2) * num_of_params;
		double *ndata = new double[nsize];
		
		AdressMatrix *nadress_matrix = new AdressMatrix(nnum_of_types);
        nadress_matrix->FillParamAdresses(num_of_params);
		
		AdressMatrix *nbond_matrix = new AdressMatrix(nnum_of_types);
		
		for(size_t i = 0; i < nsize; ++i) {
            ndata[i] = 0.0;
        }
		
		size_t ncompi, ncompj, start, nstart;
		
		for(size_t compi = 0; compi < num_of_types; ++compi) {
			for(size_t compj = 0; compj < num_of_types; ++compj) {
				if(compi <= compj && compi != comp && compj != comp) {

					ncompi = compi > comp ? (compi-1) : compi;
					ncompj = compj > comp ? (compj-1) : compj;

					nstart = nadress_matrix->Get(ncompi, ncompj);
					start = adress_matrix->Get(compi, compj);
					
					(*nbond_matrix)(ncompi, ncompj) = 1;
                    (*nbond_matrix)(ncompi, ncompj) = 1;
					
                    for(size_t i = 0; i < num_of_params; ++i) {
                       ndata[nstart + i] = data[start + i];
					}
				}
			}
		}
		
		num_of_types = nnum_of_types;
		size = nsize;
		
		delete adress_matrix;
		delete bond_matrix;
		delete data;
		
		adress_matrix = nadress_matrix;
		bond_matrix = nbond_matrix;
		data = ndata;		
	}

    size_t GetSize() const {
        return size;
    }

    AdressMatrix *GetAdressMatrix() {
        return adress_matrix;
    }

    AdressMatrix *GetBondMatrix() {
        return bond_matrix;
    }

    double *GetData() {
        return data;
    }

    ~PotentialArray() {
        delete [] data;
        delete adress_matrix;
        delete bond_matrix;
    }
};


#endif
