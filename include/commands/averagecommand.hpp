#ifndef AVERAGECOMMAND_HPP
#define AVERAGECOMMAND_HPP

#include "command.hpp"
#include "utils.hpp"

class AverageCommand : public ICommand {
    std::string GetName() override;
    std::string GetHelp() override;
    void Run(const std::vector<std::string> &args) override; 
};


#endif