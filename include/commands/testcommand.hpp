#ifndef TEST_HPP
#define TEST_HPP

#include "command.hpp" // Base class: <global>::ICommand


class TestCommand : public ICommand {
public:
   std::string GetName() override;
   std::string GetHelp() override;
   void Run(const std::vector<std::string> &args) override; 
};

#endif // TEST_HPP
