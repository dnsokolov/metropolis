#ifndef EQCOMMAND_HPP
#define EQCOMMAND_HPP

#include "command.hpp" // Base class: ICommand

class EqCommand : public ICommand
{
public:
	EqCommand();
	~EqCommand();

};

#endif // EQCOMMAND_HPP
