#ifndef CALORICCOMMAND_HPP
#define CALORICCOMMAND_HPP

#include "command.hpp"


class CaloricCommand : public ICommand {
	std::string GetName() override;
    std::string GetHelp() override;
    void Run(const std::vector<std::string> &args) override; 
};

#endif // CALORICCOMMAND_HPP
