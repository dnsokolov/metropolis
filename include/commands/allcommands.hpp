#ifndef _ALLCOMMANDS_H_
#define _ALLCOMMANDS_H_


#include "command.hpp"
#include "newcommand.hpp"
#include "runcommand.hpp"
#include "caloriccommand.hpp"
#include "clustercommand.hpp"
#include "testcommand.hpp"
#include "zcommand.hpp"
#include "averagecommand.hpp"

#endif