#ifndef _NEWCOMMAND_H_
#define _NEWCOMMAND_H_

#include "command.hpp"
#include "../propertyreader.hpp"
#include <sstream>



class NewCommand : public ICommand {
    private:
    PropertyReader *pr = nullptr;

    public:
    NewCommand(PropertyReader *pr) {
        this->pr = pr;
    }

    std::string GetName() override;
    std::string GetHelp() override;
    void Run(const std::vector<std::string> &args) override; 

};

#endif