#ifndef _RUNCOMMAND_H_
#define _RUNCOMMAND_H_

#include "command.hpp"
#include "../propertyreader.hpp"


class RunCommand : public ICommand {
public:  
   std::string GetName() override;
   std::string GetHelp() override;
   void Run(const std::vector<std::string> &args) override; 
};




#endif