#ifndef ZCOMMAND_HPP
#define ZCOMMAND_HPP

#include "commands/command.hpp"
#include "statensemble.hpp"
#include "framehandlers/coordnumberframehandler.hpp"
#include "utils.hpp"

class ZCommand : public ICommand {
    std::string GetName() override;
    std::string GetHelp() override;
    void Run(const std::vector<std::string> &args) override; 
};

#endif