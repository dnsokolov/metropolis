#ifndef CLUSTERCOMMAND_HPP
#define CLUSTERCOMMAND_HPP
#include "command.hpp"
#include <iostream>
#include "../utils.hpp"
#include "../cluster.hpp"
#include "../modifiers/allmodifiers.hpp"



class ClusterCommand : public ICommand {
	PropertyReader *pr;
public:
	ClusterCommand(PropertyReader *pr) {
		this->pr = pr;
	}
	
	std::string GetName() override;
    std::string GetHelp() override;
    void Run(const std::vector<std::string> &args) override; 
};

#endif // CLUSTERCOMMAND_HPP
