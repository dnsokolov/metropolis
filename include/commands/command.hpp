#ifndef _COMMAND_H_
#define _COMMAND_H_

#include <string>
#include <vector>


class ICommand {
    public:
    std::string virtual GetName() = 0;
    std::string virtual GetHelp() = 0;
    void virtual Run(const std::vector<std::string> &args) = 0;
};

#endif