#ifndef WRAPPER_THREAD_H
#define WRAPPER_THREAD_H

#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

typedef void* thread_t;

EXTERNC	unsigned int get_hardware_concurrency();
EXTERNC thread_t thread_create(void* (func)(void*), void* arg);
EXTERNC void thread_join(thread_t thr);
EXTERNC void thread_destroy(thread_t thr);

#endif
