#ifndef _XYZFORMAT_H_
#define _XYZFORMAT_H_

#include "format.hpp"

class XYZFormat : public IFormat {
    int frame;

    public:
    XYZFormat(int frame = 0) {
        this->frame = frame;
    }

    void Read(std::istream &istream, Cluster &cluster) override;
    void Write(const Cluster &cluster, std::ostream &outstream) override;
    static bool NextFrame(std::istream &ins, Cluster &cluster);
    static void LoadFrames(std::istream &ins, std::vector<Cluster> &frames);
    static void LoadFrames(const std::string &filename, std::vector<Cluster> &frames);

    static size_t GetNumFrames(std::istream &instream);
    static size_t GetNumFrames(const std::string &filename);
    
    static void ParseComment(Cluster &cluster, std::vector<std::string> &columns);
    static void ParseColumns(const Cluster &cluster, std::vector<std::string> &columns);
};

#endif