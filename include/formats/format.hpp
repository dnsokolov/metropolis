#ifndef _FORMAT_H_
#define _FORMAT_H_

#include <string>
#include "../cluster.hpp"

class Cluster;

class IFormat {
    public:
    virtual void Read(std::istream &instream, Cluster &cluster) = 0;
    virtual void Write(const Cluster &cluster, std::ostream &outstream) = 0; 
};

#endif