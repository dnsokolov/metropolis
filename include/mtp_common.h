#ifndef MTP_COMMON_H
#define MTP_COMMON_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdlib.h>
#include <string.h>


#define NPOS (size_t)(-1)

#ifdef __cplusplus
}
#endif

#endif