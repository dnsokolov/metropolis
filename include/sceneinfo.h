#ifndef _SCENEINFO_H_
#define _SCENEINFO_H_

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdint.h>
#include <math.h>
#include <stdio.h>
#include <stdlib.h>
#include "potentials.h"

#ifdef _WIN32
#include <malloc.h>
#endif // _WIN32


//**************************
typedef struct SceneInfo_tag {
    size_t num_of_atoms;          //num of atoms in the system
    size_t num_of_types;          //num of types of atoms in the system

    double *atom_positions;    //positions of atoms (size 3 * num_of_atoms)
    size_t *atom_adresses;     //atom_adresses[i] give start adress atom's i in positions
    int32_t *types;                //types of atoms(size num_of_atoms)
    char **names;             //list of name of components (size num_of_types)
    double *molmasses;         //list of mol masses of components (size num_of_types)
    int32_t *frozen;              //freezen atoms
    double *energies;          //energies of atoms
    
    double temperature;        //temperature
    unsigned int potflag;      //flag for enabling potentials
   
    size_t *step_adress_matrix;
    size_t *harm_adress_matrix;
    size_t *lj_adress_matrix;
    size_t *morse_adress_matrix;
    size_t *fs_adress_matrix;
    size_t *exfs_adress_matrix;
    size_t *gupta_adress_matrix;
    size_t *sc_adress_matrix;
 //   size_t *mbpc_adress_matrix;

    size_t *step_bond_matrix;   //matrix describes step bonds between types of atoms
    size_t *harm_bond_matrix;   //matrix describes harmonic bonds between types of atoms
    size_t *lj_bond_matrix;     //matrix describes lj bonds between types of atoms (size num_of_types * num_of_types)
    size_t *morse_bond_matrix;  //matrix describes morse bonds between types of atoms (-''-)
    size_t *gupta_bond_matrix;  //matrix describes gupta bonds between types of atoms (-''-)
    size_t *fs_bond_matrix;     //matrix describes finnis-sinclair bonds between types of atoms (-''-)
    size_t *exfs_bond_matrix;   //matrix describes extended finnis-sinclair bonds between types of atoms (-''-)
    size_t *sc_bond_matrix;     //matrix describes sutton-chen bonds between types of atoms (-''-)
 //   size_t *mbpc_bond_matrix;   //matrix describes many body perturbation component bond between types of atoms (-''-)

    double *step_parameters;   //step parameters potential (0 - r0, 1 - U0)
    double *harm_parameters;   //harmonic parameters potential (0 - k, 1 - r0, 2 - rcut)
    double *lj_parameters;     //lennard-jones parameters potential (0 - epsilon, 1 - sigma, 2 - rcut)
    double *morse_parameters;  //morse parameters potential (0 - D, 1 - a, 2 - re, 3 - rcut)
    double *gupta_parameters;  //gupta parameters potential (0 - A, 1 - p, 2 - r0, 3 - q, 4 - B, 5 - rcut)
    double *fs_parameters;     //finnis-sinclair parameters potential (0 - c, 1 - c0, 2 - c1, 3 - c2, 4 - d, 5 - beta, 6 - A, 7 - rcut)
    double *exfs_parameters;   //extended finnis-sinclair parameters potential (0 - c, 1 - c0, 2 - c1, 3 - c2, 4 - c3, 5 - c4, 6 - d, 7 - A, 8 - B, 9 - rcut)
    double *sc_parameters;     //sutton-chen parameters potential (0 - epsilon, 1 - a, 2 - n, 3 - m, 4 - c, 5 - rcut)
   // double *mbpc_parameters;   //many body perturbation component potential (0 - a, 1 - m, 2 - alpha, 3 - r0, 4 - epsilon, 5 - rcut)

} SceneInfo;

//*********************************
typedef struct FileInfo_tag {
	char *fname;
	FILE *fptr;
} FileInfo;

//*********************************


double get_dist(size_t i, size_t j, const SceneInfo *sc_info);
void sceneinfo_print(const SceneInfo *, FILE *);
void sceneinfo_dump(const SceneInfo *, const char *);
void sceneinfo_free(SceneInfo *);
double sceneinfo_get_energy(SceneInfo *);
void sceneinfo_save_to_xyz(SceneInfo *, const char *);
void sceneinfo_append_to_xyz(SceneInfo *, const char *);

SceneInfo *sceneinfo_load_next(FILE *fptr);
SceneInfo *sceneinfo_load_nth(const char *fname, size_t n_frame);
SceneInfo *fi_sceneinfo_load_next(FileInfo *finfo);
SceneInfo *sceneinfo_alloc();

size_t get_num_of_frames(const char *fname);

FileInfo *finfo_create(const char *fname, const char *attributes);
void finfo_destroy(FileInfo *finfo);
void finfo_rewind(FileInfo *finfo);

#define NUM_OF_TYPES(scene) scene->num_of_types
#define NUM_OF_ATOMS(scene) scene->num_of_atoms
#define TYPE_OF_ATOM(scene,atom) scene->types[atom]

#ifdef _WIN32
#ifdef ENABLE_AVX
#define DALIGNED_ALLOC(alignment,size) (double*) _aligned_malloc((size)*sizeof(double), (alignment));
#define FREE_ARRAY(name) _aligned_free(name);
#define DARRAY(name,size) double *name = (double*) _aligned_malloc((size)*sizeof(double),32);
#else
#define DARRAY(name,size) double name[(size)];
#define FREE_ARRAY(name);
#endif
#else // _WIN32 if defined unix or linux
#ifdef ENABLE_AVX
#define DALIGNED_ALLOC(alignment,size) (double*) aligned_alloc((alignment), (size) * sizeof(double));
#define DARRAY(name,size) double *name = (double *) aligned_alloc(32,(size) * sizeof(double));
#define FREE_ARRAY(name) free(name);
#else
//#define DALIGNED_ALLOC(alignment,size) (double*) aligned_alloc((alignment), (size));
#define DARRAY(name,size) double name[(size)];
#define FREE_ARRAY(name);
#endif
#endif // _WIN32


#ifdef __cplusplus
}
#endif

#endif // SCENEINFO_H