#ifndef CEPARAMSREADER_HPP
#define CEPARAMSREADER_HPP

#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <map>
#include "utils.hpp"

class CEParamsReader
{
	std::string _cefile;
	std::vector<std::string> components;
	std::map<std::string, double> ro;
	std::map<std::string, double> molmas;
public:
	CEParamsReader(const std::string &cefile);
	
	std::string ReadTBPotential( 
	                          const std::string &comp1,
							  const std::string &comp2) const;

	void GetComponents(std::vector<std::string> &comps) const;
	double GetMolMass(const std::string &comp) const;

};

#endif // CEPARAMSREADER_HPP
