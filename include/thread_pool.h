#ifndef THREAD_POOL_H
#define THREAD_POOL_H

#ifdef __cplusplus
#define EXTERNC extern "C"
#else
#define EXTERNC
#endif

typedef struct array_args_tag {
	void* data;
} array_args_t;

typedef void* thread_pool_t;

EXTERNC thread_pool_t thread_pool_create(size_t num_of_threads, void* (*task_func)(void*), array_args_t* args);
EXTERNC void thread_pool_do_work(thread_pool_t thpool);
EXTERNC void thread_pool_close(thread_pool_t thpool);

#endif
