#ifndef _PROPERTYREADER_H_
#define _PROPERTYREADER_H_

#include "utils.hpp"
#include <exception>

class PropertyReader {
    private:
    toml::table data;
    bool error{false};

    public:
    PropertyReader(const std::string &propertyfile) {
        try {
           data = toml::parse(propertyfile);
        }
        catch(const std::exception &exc) {
            std::cerr << "PropertyReader exception:\n" << exc.what() << std::endl;
            error = true;
        }
    } 

    bool IsError() const {
        return error;
    }

    std::string GetPotentialParams(
        const std::string &potname, 
        const std::string &comp1, 
        const std::string &comp2
    );

    template <typename T>
    void GetAtomProperty(
        const std::string &chemicalname, 
        const std::string &propertyname, 
        T &value
    ) {
        if(data.count(chemicalname) != 0) {
           auto table = toml::get<toml::Table>(data.at(chemicalname));
           if(table.count(propertyname) != 0)
              value = toml::get<T>(table.at(propertyname));
        }
    }
};

#endif