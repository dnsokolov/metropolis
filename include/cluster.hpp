#ifndef _CLUSTER_H_
#define _CLUSTER_H_

#include <vector>
#include <unordered_map>
#include <string>
#include "utils.hpp"
#include "formats/xyzformat.hpp"
#include "atom.hpp"
#include "vec3d.h"
#include "sceneinfo.h"
#include "potentialarray.hpp"
#include "potentials.h"
#include "successors/successor.hpp"

class XYZFormat;
class Cluster;

typedef std::vector<Cluster> Frames;

class Cluster {
    std::vector<double> positions;
    std::vector<size_t> adresses;
    std::vector<double> energies;
    std::vector<int> types;
    std::vector<int> frozen;
    std::vector<std::string> names;
    std::vector<char *> cnames;
    std::vector<double> molmasses;
	std::vector<size_t> type_counts;
    bool is_opened;
    double *ppos = nullptr;
    SceneInfo *sc_info = nullptr;
	std::string fname;
    double energy_per_atoms = 0.0;

    PotentialArray *guptaParams = nullptr;
    PotentialArray *ljParams = nullptr;
    PotentialArray *morseParams = nullptr;
    PotentialArray *fsParams = nullptr;
    PotentialArray *exfsParams = nullptr;
    PotentialArray *scParams = nullptr;
    PotentialArray *stepParams = nullptr;
    PotentialArray *harmParams = nullptr;
	
	void quick_sort(double *arr, size_t first, size_t last);
    
    public:
    std::string comment;
    std::unordered_map<std::string, std::string> properties;
    std::unordered_map<std::string, std::vector<std::string>> columns;

    Cluster(std::string filename = "", int frame = 0);
    
    ~Cluster() {   
        delete guptaParams;
        delete ljParams;
        delete morseParams;
        delete fsParams;
        delete exfsParams;
        delete scParams;
        delete stepParams;
        delete harmParams;

#ifdef ENABLE_AVX
		if(sc_info != nullptr && sc_info->atom_positions != nullptr)
		FREE_ARRAY(sc_info->atom_positions);
#endif

        if(sc_info != nullptr) delete sc_info;
    }

    bool IsOpened() {
        return is_opened;
    }
	
	std::string GetFileName() {
		return fname;
	}

    void SetMolMass(std::string component, double value) {
        size_t n;
        if((n = index_of(names, component)) != n_pos) {
            molmasses[n] = value;
        }
    }

    void SetMolMass(std::string line) {
        std::stringstream stream(line);
        std::string component;
        double value;
        stream >> component >> value;
        SetMolMass(component, value);
    }
    
    void AddAtom(double x, double y, double z, double energy, std::string type, int freeze) {
        
        positions.push_back(x);
        positions.push_back(y);
        positions.push_back(z);
        ppos = positions.data();

        energies.push_back(energy);
        frozen.push_back(freeze);

        int index;
        if((index = index_of<std::string>(names, type)) == n_pos) {
            names.push_back(type);
            molmasses.push_back(1.0);
            types.push_back(names.size()-1);
			type_counts.push_back(1);
        }
        else {
            types.push_back(index);
			++type_counts[index];
        }
    }

    void AddAtom(const Atom &atom, const std::vector<std::string> &clmns) {
        AddAtom(atom.x, atom.y, atom.z, atom.energy, atom.ename, atom.freeze);
        size_t counter = 0;
        for(const auto &clm : clmns) {
            if(clm == "Energy" || clm == "Freeze") continue;
            columns[clm].push_back(atom.column_values[counter]);
            ++counter;
        }
    }
	
	void DeleteAtom(size_t index);
	void SwapAtoms(size_t atom_i, size_t atom_j);
	void SetEnergies();

    double GetEnergyPerAtoms() {
        return energy_per_atoms;
    }
	
	double FindMinEnergy(int &index);
	double FindMaxEnergy(int &index);

    void Clear() {
        positions.clear();
        energies.clear();
        types.clear();
        frozen.clear();
        names.clear();
		columns.clear();
		properties.clear();
		
		delete guptaParams;
        delete ljParams;
        delete morseParams;
        delete fsParams;
        delete exfsParams;
        delete scParams;
        delete stepParams;
        delete harmParams;
		
		ppos = nullptr;
        sc_info = nullptr;
		
        comment = "";
    }

    void GetAtom(size_t iatom, Atom &atom) const;
    void AddPotentialParams(const std::string &params);

    void GetChemicalNames(std::vector<std::string> &names) const {
        names = this->names;
    }

    size_t NumOfAtoms() const {
        return energies.size();
    }

    size_t NumOfTypes() const {
        return names.size();
    }
	
	size_t NumOfAtoms(std::string component);

    SceneInfo *GetSceneInfo();
    SceneInfo *GetSceneInfoDup();

    void LoadFromFile(const std::string &filename, int frame = 0);
    void SaveToFile(const std::string &filename, bool append = false);

    void RefreshComment(bool appendEnergyColumn = false, bool appendFreezeColumn = false);

    void MoveAtom(size_t iatom, double t_x, double t_y, double t_z) {
        size_t adress = (iatom << 1) + iatom;

        ppos[adress] += t_x;
        ppos[adress + 1] += t_y;
        ppos[adress + 2] += t_z;
    }

    void Move(double t_x, double t_y, double t_z) {
        size_t num_of_atoms = NumOfAtoms();
        size_t adress;

        for(size_t i = 0; i < num_of_atoms; ++i) {
            adress = (i << 1) + i;
            ppos[adress] += t_x;
            ppos[adress + 1] += t_y;
            ppos[adress + 2] += t_z;
        }
    }

    void Rotate(double fi, const double *axis, bool fi_deg = true);

    double Dist(size_t i, size_t j) {
        size_t adress_i = (i << 1) + i;
        size_t adress_j = (j << 1) + i;

        double dr[] = {
                         ppos[adress_i]     - ppos[adress_j], 
                         ppos[adress_i + 1] - ppos[adress_j + 1],
                         ppos[adress_i + 2] - ppos[adress_j + 2]
                      };

        return Length(dr);
    }

    

    template<typename Func>
    void Move(double t_x, double t_y, double t_z, Func pred) {
        size_t num_of_atoms = NumOfAtoms();
        size_t adress;
        Atom atom;

        for(size_t i = 0; i < num_of_atoms; ++i) {
            GetAtom(i, atom);
            if(pred(i, atom)) {
                adress = (i << 1) + i;
                ppos[adress] += t_x;
                ppos[adress + 1] += t_y;
                ppos[adress + 2] += t_z;
            }
        }
    }

    template<typename Func>
    void Rotate(double fi, const double *axis, Func pred, bool fi_deg = true) {
        size_t num_of_atoms = NumOfAtoms();
        fi = fi_deg ? M_PI * fi / 180.0 : fi;
        double cos_fi = cos(fi);
        double sin_fi = sin(fi);
        double cross_prod[3];

        double uv, a, b, c;
        double *v;

        Atom atom;

        for(size_t i = 0; i < num_of_atoms; ++i) {
            GetAtom(i, atom);
            if(pred(i, atom)) {
               v = ppos + ((i << 1) + i);
               uv = Dot(axis, v);
               Cross(axis, v, cross_prod);

               a = axis[0] * uv;
               b = axis[1] * uv;
               c = axis[2] * uv;
               
               v[0] = (v[0] - a) * cos_fi + cross_prod[0] * sin_fi + a;
               v[1] = (v[1] - b) * cos_fi + cross_prod[1] * sin_fi + b;
               v[2] = (v[2] - c) * cos_fi + cross_prod[2] * sin_fi + c;
            }
        }
    }

    template<typename Func>
    void ForEach(Func func) {
        size_t num_of_atoms = NumOfAtoms();
        Atom atom;
        for(size_t i = 0; i < num_of_atoms; ++i) {
            GetAtom(i, atom);
            func(i, atom);
        }
    }
	
	
};

#endif
