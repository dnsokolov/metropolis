#ifndef RUNTIMEAVER_H
#define RUNTIMEAVER_H

#ifdef __cplusplus
extern "C" {
#endif

#include <stddef.h>
#include <stdlib.h>

struct runtime_aver_tag;
typedef struct runtime_aver_tag runtime_aver;

runtime_aver *create_runtime_aver(size_t reset_number);
void push_number_runtime_aver(runtime_aver *rta, double a);
double get_average_value_runtime_aver(runtime_aver *rta);
void destroy_runtime_aver(runtime_aver *rta);

#ifdef __cplusplus
}
#endif

#endif