#ifndef _POTENTIALS_H_
#define _POTENTIALS_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "cputime.h"
#include "sceneinfo.h"
#include "mtp_array_int.h"

/*#ifdef _WIN32
#include "thread_wrapper.h"
#else
#include <pthread.h>
#endif // _WIN32*/

#include "thread_wrapper.h"
#include "thread_pool.h"

#define EPSILON 0.00001
#define EQ_BY_EPS(a,b) fabs((a)-(b)) < EPSILON

#define NUM_OF_GUPTA_PARAMS 6
#define NUM_OF_LJ_PARAMS 3
#define NUM_OF_MORSE_PARAMS 4
#define NUM_OF_FS_PARAMS 8
#define NUM_OF_EXFS_PARAMS 10
#define NUM_OF_STEP_PARAMS 2
#define NUM_OF_HARM_PARAMS 3
#define NUM_OF_SC_PARAMS 6

#define LJ_FLAG 0x01
#define MORSE_FLAG 0x02
#define STEP_FLAG 0x04
#define GUPTA_FLAG 0x08
#define FS_FLAG 0x10
#define EXFS_FLAG 0x20
#define SC_FLAG 0x40
#define HARM_FLAG 0x80

typedef struct SceneInfo_tag SceneInfo;

typedef struct ThreadEnergyArg_tag {
   SceneInfo *scene;
   size_t thread_id;
   size_t start_atom;
   size_t finish_atom;
   double energy;
   double repulsive;
   double attractive;
   double attractive_gupta;
   double repulsive_gupta;
   double attractive_fs;
   double repulsive_fs;
   double attractive_exfs;
   double repulsive_exfs;
   double attractive_sc;
   double repulsive_sc;
} ThreadEnergyArg;

double get_lj_energy(int a, SceneInfo *c_info);
double get_morse_energy(int a, SceneInfo *c_info);
double get_gupta_energy(int a, SceneInfo *c_info);
double get_fs_energy(int a, SceneInfo *c_info);
double get_exfs_energy(int a, SceneInfo *c_info);
double get_sc_energy(int a, SceneInfo *c_info);
double get_step_energy(int a, SceneInfo *c_info);
double get_harm_energy(int a, SceneInfo *c_info);
double get_energy(int a, SceneInfo *c_info);
/*double get_energy_of_system(int moved_atom, SceneInfo *c_info, double (*get_energy_of_atom)(int a, SceneInfo *c_info, mtp_array_int *neighbours));*/

//************ Multithreading version ********************

void init_multithreading(size_t num_thread, SceneInfo* scene);
void destroy_multithreading();
void create_thread_args(SceneInfo *scene);
//void destroy_thread_args();

void* get_lj_energy_thread(void *arg);
void* get_morse_energy_thread(void *arg);
void* get_gupta_energy_thread(void *arg);
void* get_fs_energy_thread(void *arg);
void* get_exfs_energy_thread(void *arg);
void* get_sc_energy_thread(void *arg);
void* get_step_energy_thread(void *arg);
void* get_harm_energy_thread(void *arg);
void* get_energy_thread(void *arg);

double get_lj_energy_multhreading(int a, SceneInfo *c_info);
double get_morse_energy_multhreading(int a, SceneInfo *c_info);
double get_gupta_energy_multhreading(int a, SceneInfo *c_info);
double get_fs_energy_multhreading(int a, SceneInfo *c_info);
double get_exfs_energy_multhreading(int a, SceneInfo *c_info);
double get_sc_energy_multhreading(int a, SceneInfo *c_info);
double get_step_energy_multhreading(int a, SceneInfo *c_info);
double get_harm_energy_multhreading(int a, SceneInfo *c_info);
double get_energy_multhreading(int a, SceneInfo *c_info);

#ifdef __cplusplus
}
#endif

#endif