#ifndef CORROSIONSUCCESSOR_HPP
#define CORROSIONSUCCESSOR_HPP

#include "successor.hpp"

class CorrosionSuccessor : public ISuccessor {
	int del;
	int stop;
	std::string tomlfile;
public:
	CorrosionSuccessor() {
		del = 1;
		stop = 0;
	}

    std::string GetName() override;
	void LoadTomlFile(const std::string &tomlfile) override;
	bool Next(MTPSimulationConfig &config) override;
};

#endif // CORROSIONSUCCESSOR_HPP
