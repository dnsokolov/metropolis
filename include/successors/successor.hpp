#ifndef SUCCESSOR_HPP
#define SUCCESSOR_HPP

#include <string>
#include "../mtpsimulationconfig.hpp"
#include "../cluster.hpp"

class MTPSimulationConfig;

class ISuccessor {
public:
	virtual std::string GetName() = 0;
	virtual void LoadTomlFile(const std::string &tomlfile) = 0;
	virtual bool Next(MTPSimulationConfig &config) = 0;
};

#endif // SUCCESSOR_HPP