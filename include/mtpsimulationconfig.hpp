#ifndef _MTPSIMULATIONCONFIG_H_
#define _MTPSIMULATIONCONFIG_H_

#include <string>
#include <vector>
#include <fstream>
#include <exception>
#include "cluster.hpp"
#include "propertyreader.hpp"
#include "successors/successor.hpp"
#include "ceparamsreader.hpp"

class ISuccessor;
class Cluster;

class MTPSimulationConfig {
	std::vector<ISuccessor *> successors;
	ISuccessor *successor;
	
	bool is_seeded;
    bool error{false};
	
    public:
    std::string directory;
	
    size_t micro_steps;
    size_t macro_steps;
    size_t start_dumping_step;
	size_t ens_dump_every;
	size_t xyz_dump_every;
	
    double start_temperature;
    double finish_temperature;
    double step_temperature;
	
    double time; /*ps*/
	double sigma; 

    size_t num_of_threads;
	
    std::vector<std::string> potentials;
    std::vector<std::string> mol_masses;
    std::string load_cluster;
    std::string xyz_dump_file;
    std::string ans_dump_file;
	std::string caloric_file;
    std::string translation;
    int seed;

    MTPSimulationConfig();
	~MTPSimulationConfig();
	
    void LoadFromToml(const std::string &file);
    void SaveTomlFile(const std::string &mtp_config_file);
    void FillMolMassesAndPotentials(const Cluster &cluster, PropertyReader &reader);
    void FillMolMassesAndPotentials(const Cluster &cluster, CEParamsReader &cereader);
    void SetCluster(Cluster &cluster);
    void Run(bool benchmarking, FILE *out = stdout);
    
    bool IsError() const {
        return error;
    }
};

#endif