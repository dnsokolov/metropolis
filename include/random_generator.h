#ifndef RANDOM_GENERATOR_H
#define RANDOM_GENERATOR_H

#ifdef __cplusplus
extern "C" {
#endif

#include "dSFMT/dSFMT.h"
#include "metropolis.h"
#include <stddef.h>

struct rand_generator_tag;
typedef struct rand_generator_tag rand_generator_t;

rand_generator_t *rand_generator_create(MetropolisSimulation *sim, SceneInfo *sc, size_t capacity);
void rand_generator_get_translation(rand_generator_t *gen, int atom, double temperature, double *tr);
void rand_generator_destroy(rand_generator_t *gen);

#ifdef __cplusplus
}
#endif

#endif