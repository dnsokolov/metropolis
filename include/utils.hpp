#ifndef _UTILS_H_
#define _UTILS_H_

#include <algorithm>
#include <vector>
#include <fstream>
#include <string>
#include <cmath>
#include <filesystem>
#include <map>
#include <utility>
#include "toml/toml.hpp"

#ifndef _WIN32
#include <unistd.h>
#define MAX_PATH 4096
#else
#include <Windows.h>
#endif

const size_t n_pos = (size_t)(-1);

template <typename T>
size_t index_of(const std::vector<T>  &vecOfElements, const T &element)
{
    auto begin = vecOfElements.begin();
    auto end = vecOfElements.end();
	auto it = std::find(begin, end, element);
 
	if (it != end) return std::distance(begin, it);
	else return n_pos;
}

template <typename T>
T as(const std::string &str) {
	std::stringstream stream(str);
	T result;
	stream >> result;
	return result;
}

template <typename T>
std::string as_string(const T &value) {
	std::stringstream stream;
	stream << value;
	return stream.str();
}

bool contains_substr(const std::string &str, const std::string &substr);

void read_all(const std::string &filename, std::string &content);
void read_line(std::ifstream& stream, std::string& line);
void split(const std::string &str, std::vector<std::string> &tokens, char delim, size_t num_tokens = 0);
std::string get_executable_directory();
int get_time();
void average_data(const std::vector<double*> &data, size_t data_block_size, double &mean);
void average_data(const std::vector<double*> &data, size_t data_block_size, double &mean, double &sigma);
void average_data(const double *data, size_t data_size, double &mean, double &sigma);
void average_data(const double *data, size_t data_size, double &mean);
void average_data(const std::vector<double> &data, double &mean, double &sigma);
void average_data(const std::vector<double> &data, double &mean);
void save_data(const std::string &fname, const std::map<double,std::pair<double,double>> &functable);

#endif