#ifndef MTP_ARRAY_INT_H
#define MTP_ARRAY_INT_H

#ifdef __cplusplus
extern "C" {
#endif

#include "mtp_common.h"

struct mtp_array_int_tag;

typedef struct mtp_array_int_tag mtp_array_int;

mtp_array_int *mtp_array_int_create(size_t capacity);
void mtp_array_int_destroy(mtp_array_int *self);
void mtp_array_int_append(mtp_array_int *self, int a);
void mtp_array_int_append_if_not_exists(mtp_array_int *self, int a);
size_t mtp_array_int_index_of(mtp_array_int *self, int a);
size_t mtp_array_int_size(mtp_array_int *self);
size_t mtp_array_int_capacity(mtp_array_int *self);
int *mtp_array_int_data(mtp_array_int *self);
int mtp_array_int_nth(mtp_array_int *self, size_t i);
void mtp_array_int_remove_nth(mtp_array_int *self, size_t i);
void mtp_array_clear(mtp_array_int *self);

#ifdef __cplusplus
}
#endif

#endif