#ifndef THREAD_POOL_HPP
#define THREAD_POOL_HPP

#include "cputime.h"
#include <thread>
//#include <mutex>
#include <vector>
//#include <chrono>
//#include <iostream>

const int DELAY = 2;

struct ArrayArgs {
	void* data;
};

class ThreadPool {
private:
	std::vector<std::thread*> threads;
	//std::mutex* guards;
	
	void* (*_task_func)(void* args);
	volatile unsigned state;
    unsigned start_state;
	size_t _num_of_threads;
	unsigned *flags;

	void loop(int threadId, ArrayArgs* args, volatile unsigned *pState) {
		again:
			if (((*pState) & (~flags[threadId])) > 0) {	
				_task_func(args[threadId].data);
				*pState &= flags[threadId];
			}
			else if (state == (unsigned)(-1)) {
				return;
			}
		goto again;
	}

	unsigned two_pow(unsigned int n) {
		unsigned result = 1;
		for (unsigned int i = 0; i < n; ++i) {
			result *= 2;
		}
		return result;
	}

	unsigned get_state(size_t num_of_threads) {
		unsigned result{ 0 };
		for (unsigned i = 0; i < num_of_threads; ++i) {
			result += two_pow(i);
		}
		return result;
	}

public:
	ThreadPool(size_t num_of_threads, void* (*task_func)(void* args), ArrayArgs* args) :
		_num_of_threads(num_of_threads), _task_func(task_func), state(0)
	{
		start_state = get_state(num_of_threads);
		flags = new unsigned[num_of_threads];

		for (size_t i = 0; i < num_of_threads; ++i) {
			std::thread* thread = new std::thread(&ThreadPool::loop, *this, i, args, &state);
			thread->detach();
			threads.push_back(thread);
			flags[i] = ~(two_pow(i));
		}
	}

	void RunTasks() {
		state = start_state;

	    again:
		for (size_t i = 0; i < DELAY; ++i);
		if (!state) {
			return;
		}
		goto again;
	}

	void Stop() {
		state = (unsigned)(-1);
		delete[] flags;
	}
};

#endif // !THREAD_POOL_HPP

