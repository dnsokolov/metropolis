#ifndef MODIFIER_HPP
#define MODIFIER_HPP

#include <string>
#include <vector>
#include "../cluster.hpp"

class IModifier {
public:
	std::string virtual GetName() = 0;
    std::string virtual GetHelp() = 0;
	int virtual Apply(Cluster &cluster, const std::vector<std::string> &args) = 0;
};

#endif // MODIFIER_HPP