#ifndef ALLMODIFIERS_HPP
#define ALLMODIFIERS_HPP

#include "loadmodifier.hpp"
#include "infomodifier.hpp"
#include "deletemodifier.hpp"
#include "setmodifier.hpp"
#include "eqmodifier.hpp"

#endif // ALLMODIFIERS_HPP