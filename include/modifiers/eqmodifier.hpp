#ifndef EQMODIFIER_HPP
#define EQMODIFIER_HPP

#include "modifier.hpp" // Base class: IModifier

class EqModifier : public IModifier
{
public:
   std::string GetName() override;
   std::string GetHelp() override;
   int Apply(Cluster &cluster, const std::vector<std::string> &args) override;
};

#endif // EQMODIFIER_HPP
