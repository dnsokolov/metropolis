#ifndef LOADMODIFIER_HPP
#define LOADMODIFIER_HPP

#include "modifier.hpp"
#include "../utils.hpp"

class LoadModifier : public IModifier {
public:
   std::string GetName() override;
   std::string GetHelp() override;
   int Apply(Cluster &cluster, const std::vector<std::string> &args) override; 
};

#endif // LOADMODIFIER_HPP
