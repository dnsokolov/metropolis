#ifndef MINUSMODIFIER_HPP
#define MINUSMODIFIER_HPP

#include <algorithm>
#include "modifier.hpp"
#include "../utils.hpp"

class DeleteModifier : public IModifier{
public:
   std::string GetName() override;
   std::string GetHelp() override;
   int Apply(Cluster &cluster, const std::vector<std::string> &args) override;
};

#endif // MINUSMODIFIER_HPP
