#ifndef INFOMODIFIER_HPP
#define INFOMODIFIER_HPP

#include "modifier.hpp"
#include "../utils.hpp"
#include "../sceneinfo.h"

class InfoModifier : IModifier {
public:
   std::string GetName() override;
   std::string GetHelp() override;
   int Apply(Cluster &cluster, const std::vector<std::string> &args) override; 
};

#endif // INFOMODIFIER_HPP
