#ifndef SETMODIFIER_HPP
#define SETMODIFIER_HPP

#include "modifier.hpp"
#include "../propertyreader.hpp"

class SetModifier : public IModifier {
	PropertyReader *pr;
public:
   SetModifier(PropertyReader *pr) {
	  this->pr = pr;   
   }
   
   std::string GetName() override;
   std::string GetHelp() override;
   int Apply(Cluster &cluster, const std::vector<std::string> &args) override; 
};

#endif // SETMODIFIER_HPP
