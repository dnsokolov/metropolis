#ifndef _VEC3D_H_
#define _VEC3D_H_

#include <cmath>

const double e_x[] = {1.0, 0.0, 0.0};
const double e_y[] = {0.0, 1.0, 0.0};
const double e_z[] = {0.0, 0.0, 1.0};

inline double Dot(const double *v1, const double *v2) {
    return v1[0] * v2[0] + v1[1] * v2[1] + v1[2] * v2[2];
}

inline void Mul(double *v, double a) {
    v[0] *= a;
    v[1] *= a;
    v[2] *= a;
}

inline void Cross(const double *v1, const double *v2, double *result) {
    result[0] = v1[1] * v2[2] - v1[2] * v2[1];
    result[1] = v1[2] * v2[0] - v1[0] * v2[2];
    result[2] = v1[0] * v2[1] - v1[1] * v2[0];
}

inline double Length(const double *v) {
    return sqrt(Dot(v, v));
}

inline void Normalize(double *v) {
    double length = Length(v);
    if(length != 0.0) {
        Mul(v, 1.0/length);
    }
}

#endif