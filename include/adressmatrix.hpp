#ifndef _ADRESSMATRIX_H_
#define _ADRESSMATRIX_H_

#include <iostream>

class AdressMatrix
{
private:
    size_t N;
    size_t *data;

public:
    AdressMatrix(size_t N) {
        this->N = N;
        size_t size = N * N;
        data = new size_t[size];
        for(size_t i = 0; i < size; ++i) {
            data[i] = 0;
        }
    }

    size_t *pdata() {
        return data;
    }

    size_t& operator() (size_t i, size_t j) {
        return data[i * N + j];
    }

    size_t GetSize() const {
        return N;
    }

    size_t& Get(size_t i, size_t j) {
        return data[i * N + j];
    }

    void FillParamAdresses(size_t num_params) {
        size_t last_value = 0;
        for(size_t i = 0; i < N; ++i) {
            for(size_t j = i; j < N; ++j) {
                if(i == j) {
                    data[i * N + j] = last_value;
                }
                else {
                    data[i * N + j] = last_value;
                    data[j * N + i] = last_value;
                }
                last_value += num_params;
            }
        }
    }

    ~AdressMatrix() {
        delete [] data;
    }

    friend std::ostream& operator<< (std::ostream& out, const AdressMatrix &matrix);
};

std::ostream& operator<< (std::ostream& out, const AdressMatrix &matrix);

#endif