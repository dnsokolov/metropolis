#ifndef _STATENSEMBLE_H_
#define _STATENSEMBLE_H_

#include <vector>
#include <string>
#include <filesystem>
#include <algorithm>
#include "formats/xyzformat.hpp"
#include "sceneinfo.h"
#include "cluster.hpp"
#include "utils.hpp"
#include "framehandlers/framehandler.hpp"

enum Format {
    XYZ = 0,
    ENS,
    AUTO
};

class StatEnsemble {
private:
    std::vector<SceneInfo *> frames;
    bool freeflag = true;
    void load_auto(const std::string &fname);
    void load_xyz(const std::string &fname);
    void load_ens(const std::string &fname);
    
public:
    StatEnsemble(const std::string &fname, Format frm = AUTO) {
        switch(frm) {
            case AUTO:
               load_auto(fname);
               break;
            case XYZ:
                load_xyz(fname);
                break;
            case ENS:
                load_ens(fname);
                break;
            default:
                load_auto(fname);
                break;
        }
    }
    
    ~StatEnsemble() {
        if(freeflag) {
            auto end = frames.end();
            for(auto i = frames.begin(); i != end; ++i) {
                sceneinfo_free(*i);
            }
        }
    }
	
	void GetEnergy(double temperature, double &energy, double &sigma, size_t skip_num = 0);
	void SaveCaloricCurve(const std::string &fname);
    
    void ForEach(std::vector<IFrameHandler*> &funcs);
};

#endif