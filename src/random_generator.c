#include "random_generator.h"

struct rand_generator_tag {
    MetropolisSimulation *simulation;
    SceneInfo *scene;
    void (*rnd_func)(rand_generator_t *gen, int atom, double temperature, double *tr);
    double *rnd_data;
    size_t capacity;
};

void rnd_translation_func(rand_generator_t *gen, int atom, double temperature, double *tr) {

}

rand_generator_t *rand_generator_create(MetropolisSimulation *sim, SceneInfo *sc, size_t capacity) {
    rand_generator_t *gen = (rand_generator_t*) malloc(sizeof(struct rand_generator_tag));

    gen->simulation = sim;
    gen->scene = sc;
    gen->capacity = capacity;
    gen->rnd_data = (double*) malloc(sizeof(double) * capacity);

    if(gen->rnd_data != NULL) {
        return gen;
    }
    else {
        free(gen);
        return NULL;
    }
}

void rand_generator_get_translation(rand_generator_t *gen, int atom, double temperature, double *tr) {
    if(gen->capacity != 0) {

    }
}