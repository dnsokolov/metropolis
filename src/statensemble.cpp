#include "statensemble.hpp"

void StatEnsemble::load_auto(const std::string &fname) {
    std::string ext = std::filesystem::path(fname).extension().string();
    std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);
    
    if(ext == ".ens") {
        load_ens(fname);
        return;
    }
    
    if(ext == ".xyz") {
        load_xyz(fname);
        return;
    }
    
    load_ens(fname);
}

void StatEnsemble::load_ens(const std::string &fname) {
    FILE *fptr = fopen(fname.c_str(),"rb");
    if(fptr == NULL) return;
    
    SceneInfo *sc;
    while((sc = sceneinfo_load_next(fptr))) {
        frames.push_back(sc);
    }
    
    fclose(fptr);
}

void StatEnsemble::load_xyz(const std::string &fname) {
    std::ifstream instream(fname);
    if(!instream.is_open()) return;
    
    Cluster cluster;
    
    while(XYZFormat::NextFrame(instream, cluster)) {
        frames.push_back(cluster.GetSceneInfoDup());
        cluster.Clear();
    }
    
    instream.close();
}

void StatEnsemble::SaveCaloricCurve(const std::string &fname) {
	std::ofstream ofstream(fname);
	if(!ofstream.is_open()) return;
	
	if(frames.size() > 0) {
		double T = frames[0]->temperature;
		double energy, sigma;
		std::vector<double> energies;

		auto end = frames.end();
		
		for(auto i = frames.begin(); i != end; ++i) {
			if((*i)->temperature != T) {
				average_data(energies, energy, sigma);
				ofstream << T << " " << energy << " " << sigma << std::endl;
				energies.clear();
				T = (*i)->temperature;
			}

		    average_data((*i)->energies, (*i)->num_of_atoms, energy);
			energies.push_back(energy);
		}
		
		average_data(energies, energy, sigma);
		ofstream << T << " " << energy << " " << sigma << std::endl;
		
	}
	
	ofstream.close();
}

void StatEnsemble::GetEnergy(double temperature, double &energy, double &sigma, size_t skip_num) {
	energy = 0.0;
	sigma = 0.0;
	auto end = frames.end();
	std::vector<double> energies;
	
	bool break_flag = false;
	
	for(auto i = frames.begin(); i != end; ++i) {
		if(skip_num != 0) {
			if(EQ_BY_EPS(temperature, (*i)->temperature)) --skip_num;
			continue;
		}
		
		if(break_flag && !EQ_BY_EPS(temperature, (*i)->temperature)) {
			break;
		}
		
		if(EQ_BY_EPS(temperature, (*i)->temperature)) {
			average_data((*i)->energies,(*i)->num_of_atoms, energy);
			energies.push_back(energy);
			break_flag = true;
		}
	}

	average_data(energies, energy, sigma);
}

void StatEnsemble::ForEach(std::vector<IFrameHandler*> &handlers) {
	auto frame_end = frames.end();
	auto handler_end = handlers.end();

	size_t frame_num = 1;

	for(auto i = frames.begin(); i != frame_end; ++i) {
		std::cout<< "Processing frame <" << frame_num << ">..." << std::endl;
		for(auto j = handlers.begin(); j != handler_end; ++j) {
			std::cout << "Start frame handler '" << (*j)->GetName() << "'"  << std::endl;
			(*(*j))(*i);
			std::cout << "'" << (*j)->GetName() << "' is finished" << std::endl;
		}
		std::cout << std::endl;
		++frame_num;
	}

	for(auto i = handlers.begin(); i != handler_end; ++i) {
		(*i)->End();
	}
}