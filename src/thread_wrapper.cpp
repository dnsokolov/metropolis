#include "thread_wrapper.h"

#ifndef USE_WINAPI
#include <thread>
using std::thread;
#else
#ifdef _WIN32
#include <Windows.h>
#endif
#endif // !USE_WINAPI

unsigned int get_hardware_concurrency() {
	return thread::hardware_concurrency();
}

thread_t thread_create(void* (func)(void*), void* arg) {
#ifndef USE_WINAPI
	thread* t = new thread(func, arg);
	return t;
#else
#ifdef _WIN32
	HANDLE hThread = CreateThread(NULL, 0,(LPTHREAD_START_ROUTINE) func, arg, 0, NULL);
	return hThread;
#endif
    return 0;
#endif
}

void thread_join(thread_t thr) {
#ifndef USE_WINAPI
	thread* t = static_cast<thread*>(thr);
	t->join();
#else
#ifdef _WIN32
	WaitForSingleObject((HANDLE)thr, INFINITE);
#endif
#endif // !USE_WINAPI	
}

void thread_destroy(thread_t thr) {
#ifndef USE_WINAPI
	thread* t = static_cast<thread*>(thr);
	delete t;
#else
#ifdef _WIN32
	CloseHandle((HANDLE)thr);
#endif
#endif
}

