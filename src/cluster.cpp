#include "cluster.hpp"
#include <cstring>
#include <cstdlib>
#include <sstream>

#ifndef M_PI
#define M_PI 3.14159265358979323846
#endif

Cluster::Cluster(std::string filename, int frame) {
        ppos = positions.data();
        std::ifstream fs(filename);
        if(fs.is_open()) {
            XYZFormat xyz(frame);
            xyz.Read(fs, *this);
            is_opened = true;
			fname = filename;
        }
        else {
            comment = "";
            is_opened = false;
        }
}

size_t Cluster::NumOfAtoms(std::string component) {
	int itype = index_of<std::string>(names, component);
    if(itype != -1) {
       return type_counts[itype];		
    }
	return 0;
}

void Cluster::DeleteAtom(size_t index) {
	if(index < NumOfAtoms()) {
		
		int itype = types[index];
		size_t num_atoms_itype = type_counts[itype];
		
		energies.erase(energies.begin() + index);
		adresses.pop_back();
		
		
		size_t adress = (index << 1) + index;
		auto pos = positions.begin() + adress;
		positions.erase(pos, pos + 3);
	
		
		types.erase(types.begin() + index);
		frozen.erase(frozen.begin() + index);
		
		if(num_atoms_itype == 1) {
			auto end = types.end();
			for(auto i = types.begin(); i != end; ++i) {
				if((*i) > itype) --(*i);
			}
			
			molmasses.erase(molmasses.begin() + itype);
			names.erase(names.begin() + itype);
			type_counts.erase(type_counts.begin() + itype);
			
			if(guptaParams != nullptr) {
				guptaParams->DeleteComponent(itype);
			}
			
			if(ljParams != nullptr) {
				ljParams->DeleteComponent(itype);
			}
			
			if(morseParams != nullptr) {
				morseParams->DeleteComponent(itype);
			}
			
			if(fsParams != nullptr) {
				fsParams->DeleteComponent(itype);
			}
			
			if(exfsParams != nullptr) {
				exfsParams->DeleteComponent(itype);
			}
			
			if(scParams != nullptr) {
				scParams->DeleteComponent(itype);
			}
			
			if(stepParams != nullptr) {
				stepParams->DeleteComponent(itype);
			}
			
			if(harmParams != nullptr) {
				harmParams->DeleteComponent(itype);
			}
		}
		
		--type_counts[itype];
		
		delete sc_info;
		sc_info = nullptr;

		
		sc_info = GetSceneInfo();

		
		
		if(guptaParams != nullptr) {
			sc_info->potflag |= GUPTA_FLAG;
			sc_info->gupta_bond_matrix = guptaParams->GetBondMatrix()->pdata();
			sc_info->gupta_adress_matrix = guptaParams->GetAdressMatrix()->pdata();
			sc_info->gupta_parameters = guptaParams->GetData();
		}

		
		if(ljParams != nullptr) {
			sc_info->potflag |= LJ_FLAG;
			sc_info->lj_bond_matrix = ljParams->GetBondMatrix()->pdata();
			sc_info->lj_adress_matrix = ljParams->GetAdressMatrix()->pdata();
			sc_info->lj_parameters = ljParams->GetData();
		}
		
		if(morseParams != nullptr) {
			sc_info->potflag |= MORSE_FLAG;
			sc_info->morse_bond_matrix = morseParams->GetBondMatrix()->pdata();
			sc_info->morse_adress_matrix = morseParams->GetAdressMatrix()->pdata();
			sc_info->morse_parameters = morseParams->GetData();
		}

		
		if(fsParams != nullptr) {
			sc_info->potflag |= FS_FLAG;
			sc_info->fs_bond_matrix = fsParams->GetBondMatrix()->pdata();
			sc_info->fs_adress_matrix = fsParams->GetAdressMatrix()->pdata();
			sc_info->fs_parameters = fsParams->GetData();
		}
		
		if(exfsParams != nullptr) {
			sc_info->potflag |= EXFS_FLAG;
			sc_info->exfs_bond_matrix = exfsParams->GetBondMatrix()->pdata();
			sc_info->exfs_adress_matrix = exfsParams->GetAdressMatrix()->pdata();
			sc_info->exfs_parameters = exfsParams->GetData();
		}
		
		if(scParams != nullptr) {
			sc_info->potflag |= SC_FLAG;
			sc_info->sc_bond_matrix = scParams->GetBondMatrix()->pdata();
			sc_info->sc_adress_matrix = scParams->GetAdressMatrix()->pdata();
			sc_info->sc_parameters = scParams->GetData();
		}
		
		
		if(stepParams != nullptr) {
			sc_info->potflag |= STEP_FLAG;
			sc_info->step_bond_matrix = stepParams->GetBondMatrix()->pdata();
			sc_info->step_adress_matrix = stepParams->GetAdressMatrix()->pdata();
			sc_info->step_parameters = stepParams->GetData();
		}

		
		if(harmParams != nullptr) {
			sc_info->potflag |= HARM_FLAG;
			sc_info->harm_bond_matrix = harmParams->GetBondMatrix()->pdata();
			sc_info->harm_adress_matrix = harmParams->GetAdressMatrix()->pdata();
			sc_info->harm_parameters = harmParams->GetData();
		}
		
	}
}

double Cluster::FindMaxEnergy(int &index) {
	size_t size = energies.size();
	
	if(size == 0) {
		index = -1;
		return 0.0;
	}
	
	double max_energy = energies[0];
	index = 0;
	
	for(size_t i = 1; i < size; ++i) {
		if(energies[i] > max_energy) {
			index = i;
			max_energy = energies[i];
		}
	}
	
	return max_energy;
}

double Cluster::FindMinEnergy(int &index) {
	size_t size = energies.size();
	
	if(size == 0) {
		index = -1;
		return 0.0;
	}
	
	double min_energy = energies[0];
	index = 0;
	
	for(size_t i = 1; i < size; ++i) {
		if(energies[i] < min_energy) {
			index = i;
			min_energy = energies[i];
		}
	}
	
	return min_energy;
}

void Cluster::RefreshComment(bool appendEnergyColumn, bool appendFreezeColumn) {
        std::string str = "[";
        auto end = properties.end();
        auto end_clm = columns.end();

        for(auto i = properties.begin(); i != end; ++i) {
            str += i->first + ":" + i->second + " ";
        }

        str += "columns:";

        if(appendEnergyColumn) str += "Energy,";
        if(appendFreezeColumn) str += "Freeze,";

        for(auto i = columns.begin(); i != end_clm; ++i) {
            str += i->first +",";
        }

        str[str.length()-1] = ']';
        comment = str;

        /*size_t start = comment.find('[');
        size_t finish = comment.find(']');



         if(start != std::string::npos && finish != std::string::npos) {
            size_t count = finish - start + 1;
            std::cout.flush();
            comment.replace(start, count, str);
        } 
        else {
           comment += " " + str;  
        }*/
}

void Cluster::GetAtom(size_t i, Atom &atom) const {
    size_t adress = (i << 1) + i;
    atom.x = positions[adress];
    atom.y = positions[adress + 1];
    atom.z = positions[adress + 2];
    atom.ename = names[types[i]];
    atom.molmass = molmasses[types[i]];
    atom.freeze = frozen[i];
    atom.energy = energies[i];
    auto end = columns.end();
    for(auto it = columns.begin(); it != end; ++it) {
        atom.column_values.push_back(it->second[i]);
    }
}

void Cluster::SetEnergies() {
	SceneInfo *sc_info = GetSceneInfo();
	size_t num_of_atoms = NumOfAtoms();
    double e = 0.0;
	
	for(size_t i = 0; i < num_of_atoms; ++i) {
		e += get_energy(i, sc_info);
	}

    energy_per_atoms = e / num_of_atoms;
}

void Cluster::SaveToFile(const std::string &filename, bool append) {
        std::ofstream fs;
        if(append) fs.open(filename, std::ios_base::out | std::ios_base::app);
        else fs.open(filename, std::ios_base::out | std::ios_base::trunc);
        if(fs.is_open()) {
            std::string content;
            XYZFormat xyz;

            #ifdef ENABLE_AVX
            SceneInfo *sc_info = GetSceneInfo();
            positions.clear();
			size_t avx_adress;

			for (size_t i = 0; i < sc_info->num_of_atoms; ++i) {
				avx_adress = sc_info->atom_adresses[i];
				positions.push_back(sc_info->atom_positions[avx_adress]);
				positions.push_back(sc_info->atom_positions[avx_adress+1]);
				positions.push_back(sc_info->atom_positions[avx_adress+2]);
			}
            #endif  

            xyz.Write(*this, fs);
        }
        fs.close();
}

void Cluster::LoadFromFile(const std::string &filename, int frame) {
        is_opened = false;
        Clear();
        std::ifstream content(filename);

        if(sc_info != nullptr) {
            delete sc_info;
            sc_info = nullptr;
        }
        
        if(content.is_open()) {
           XYZFormat xyz(frame);
           xyz.Read(content, *this);
           is_opened = true;
		   fname = filename;
        }
}

void Cluster::SwapAtoms(size_t atom_i, size_t atom_j) {
	size_t adress_i = (atom_i << 1) + atom_i;
	size_t adress_j = (atom_j << 1) + atom_j;
	
	double pos_i;	
	pos_i = positions[adress_i];
	positions[adress_i] = positions[adress_j];
	positions[adress_j] = pos_i;
	
	pos_i = positions[adress_i + 1];
	positions[adress_i + 1] = positions[adress_j + 1];
	positions[adress_j + 1] = pos_i;
	
	pos_i = positions[adress_i + 2];
	positions[adress_i + 2] = positions[adress_j + 2];
	positions[adress_j + 2] = pos_i;
	
	double energy_i = energies[atom_i];
	energies[atom_i] = energies[atom_j];
	energies[atom_j] = energy_i;
	
	int type_i = types[atom_i];
	types[atom_i] = types[atom_j];
	types[atom_j] = type_i;
	
	int frozen_i = frozen[atom_i];
	frozen[atom_i] = frozen[atom_j];
	frozen[atom_j] = frozen_i;
	
	auto end = columns.end();
	for(auto i = columns.begin(); i != end; ++i) {
		std::string str_i = i->second[atom_i];
		i->second[atom_i] = i->second[atom_j];
        i->second[atom_j] = str_i; 
	}
	
}

void Cluster::Rotate(double fi, const double *axis, bool fi_deg) {
        size_t num_of_atoms = NumOfAtoms();
        fi = fi_deg ? M_PI * fi / 180.0 : fi;
        double cos_fi = cos(fi);
        double sin_fi = sin(fi);
        double cross_prod[3];

        double uv, a, b, c;
        double *v;

        for(size_t i = 0; i < num_of_atoms; ++i) {
            v = ppos + ((i << 1) + i);
            uv = Dot(axis, v);
            Cross(axis, v, cross_prod);

            a = axis[0] * uv;
            b = axis[1] * uv;
            c = axis[2] * uv;

            v[0] = (v[0] - a) * cos_fi + cross_prod[0] * sin_fi + a;
            v[1] = (v[1] - b) * cos_fi + cross_prod[1] * sin_fi + b;
            v[2] = (v[2] - c) * cos_fi + cross_prod[2] * sin_fi + c;
        }
} 

SceneInfo *Cluster::GetSceneInfo() {
    if(sc_info == nullptr) {
        sc_info = new SceneInfo;
        memset(sc_info, 0, sizeof(SceneInfo));
        sc_info->num_of_atoms = NumOfAtoms();
        sc_info->num_of_types = NumOfTypes();
        sc_info->molmasses = molmasses.data();

        #ifdef ENABLE_AVX
        size_t bytes =  (4 * sc_info->num_of_atoms) * sizeof(double);
		size_t avx_adress, adress;
        sc_info->atom_positions = DALIGNED_ALLOC(32, bytes);
		for (size_t i = 0; i < sc_info->num_of_atoms; ++i) {
			avx_adress = i << 2;
			adress = (i << 1) + i;
			memcpy(sc_info->atom_positions + avx_adress, positions.data() + adress, 3 * sizeof(double));
		}
        #else
        sc_info->atom_positions = positions.data(); 
        #endif

        if(adresses.size() != sc_info->num_of_atoms) {
		   adresses.clear();
           for(size_t i = 0; i < sc_info->num_of_atoms; ++i) {
               #ifdef ENABLE_AVX
               adresses.push_back(i << 2);
               #else
               adresses.push_back((i << 1) + i);
               #endif
           }
		}
		
        sc_info->atom_adresses = adresses.data();
        sc_info->types = types.data();

        if(cnames.size() != sc_info->num_of_types){
			cnames.clear();
			for(size_t i = 0; i < sc_info->num_of_types; ++i) {
               cnames.push_back(const_cast<char*>(names[i].c_str()));
            }
		}
        

        sc_info->names = cnames.data();

        sc_info->frozen = frozen.data();
        sc_info->energies = energies.data();
        sc_info->potflag = 0;
        
        auto key = properties.find("temperature");
        if(key != properties.end()) {
            sc_info->temperature = as<double>(properties["temperature"]);
        }
        else {
            sc_info->temperature = 0.0;
        }
        
        return sc_info;
    }
    else return sc_info;
}

SceneInfo *Cluster::GetSceneInfoDup() {
    SceneInfo *sc = (SceneInfo*)malloc(sizeof(SceneInfo));
    memset(sc, 0, sizeof(SceneInfo));
    sc->num_of_atoms = NumOfAtoms();
    sc->num_of_types = NumOfTypes();
    
    sc->molmasses = (double *)malloc(sizeof(double) * molmasses.size());
    memcpy(sc->molmasses, molmasses.data(), molmasses.size() * sizeof(double));

    #ifdef ENABLE_AVX
	size_t bytes = (4 * sc_info->num_of_atoms) * sizeof(double);
	size_t avx_adress, adress;
	sc_info->atom_positions = DALIGNED_ALLOC(32, bytes);
	for (size_t i = 0; i < sc_info->num_of_atoms; ++i) {
		avx_adress = i << 2;
		adress = (i << 1) + i;
		memcpy(sc_info->atom_positions + avx_adress, positions.data() + adress, 3 * sizeof(double));
	}
    #else
    sc->atom_positions = (double *) malloc(sizeof(double) * positions.size());
    memcpy(sc->atom_positions, positions.data(), positions.size() * sizeof(double));
    #endif

    sc->atom_adresses = (size_t *)malloc(sizeof(size_t) * sc->num_of_atoms);
    for(size_t i = 0; i < sc->num_of_atoms; ++i) {
        #ifdef ENABLE_AVX
            sc->atom_adresses[i] = (i << 2);
        #else
            sc->atom_adresses[i] = (i << 1) + i;
        #endif
    }
    
    sc->types = (int *)malloc(sizeof(int) * types.size());
    memcpy(sc->types, types.data(), types.size() * sizeof(double));
    
    sc->names = (char **)malloc(sizeof(char *) * names.size());
    for(size_t i = 0; i < sc->num_of_types; ++i) {
        sc->names[i] = strdup(names[i].c_str());
    }
    
    sc->frozen = (int *)malloc(sizeof(int) * frozen.size());
    memcpy(sc->frozen, frozen.data(), frozen.size() * sizeof(int));
    
    sc->energies = (double *)malloc(sizeof(double) * energies.size());
    memcpy(sc->energies, energies.data(), sizeof(double) * energies.size());
    
    auto key = properties.find("temperature");
    if(key != properties.end()) {
        sc->temperature = as<double>(properties["temperature"]);
    }
    else {
        sc->temperature = 0.0;
    }
    
    return sc;
}

void Cluster::AddPotentialParams(const std::string &params) {
    if(sc_info == nullptr) GetSceneInfo();

    std::stringstream stream(params);
    std::string potential;
    std::string comp1;
    std::string comp2;
    int i_comp1, i_comp2;
    double *pparams = nullptr;

    stream >> potential >> comp1 >> comp2;

    if((i_comp1 = index_of(names, comp1)) != -1 && (i_comp2 = index_of(names, comp2)) != -1) {

        if(potential == "gupt") {
            if(guptaParams == nullptr) {
               guptaParams = new PotentialArray(NumOfTypes(), NUM_OF_GUPTA_PARAMS);
               sc_info->potflag |= GUPTA_FLAG;
               sc_info->gupta_bond_matrix = guptaParams->GetBondMatrix()->pdata();
               sc_info->gupta_adress_matrix = guptaParams->GetAdressMatrix()->pdata();
               sc_info->gupta_parameters = guptaParams->GetData();
            }

            pparams = new double[NUM_OF_GUPTA_PARAMS];

            for(size_t i = 0; i < NUM_OF_GUPTA_PARAMS; ++i) {
                stream >> pparams[i];
            }

            guptaParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

        if(potential == "lj") {
            if(ljParams == nullptr) {
               ljParams = new PotentialArray(NumOfTypes(), NUM_OF_LJ_PARAMS);
               sc_info->potflag |= LJ_FLAG;
               sc_info->lj_bond_matrix = ljParams->GetBondMatrix()->pdata();
               sc_info->lj_adress_matrix = ljParams->GetAdressMatrix()->pdata();
               sc_info->lj_parameters = ljParams->GetData();
            }

            pparams = new double[NUM_OF_LJ_PARAMS];

            for(size_t i = 0; i < NUM_OF_LJ_PARAMS; ++i) {
                stream >> pparams[i];
            }

            ljParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

        if(potential == "morse") {
            if(morseParams == nullptr) {
               morseParams = new PotentialArray(NumOfTypes(), NUM_OF_MORSE_PARAMS);
               sc_info->potflag |= MORSE_FLAG;
               sc_info->morse_bond_matrix = morseParams->GetBondMatrix()->pdata();
               sc_info->morse_adress_matrix = morseParams->GetAdressMatrix()->pdata();
               sc_info->morse_parameters = morseParams->GetData();
            }

            pparams = new double[NUM_OF_MORSE_PARAMS];

            for(size_t i = 0; i < NUM_OF_MORSE_PARAMS; ++i) {
                stream >> pparams[i];
            }

            morseParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

        if(potential == "fs") {
            if(fsParams == nullptr) {
               fsParams = new PotentialArray(NumOfTypes(), NUM_OF_FS_PARAMS);
               sc_info->potflag |= FS_FLAG;
               sc_info->fs_bond_matrix = fsParams->GetBondMatrix()->pdata();
               sc_info->fs_adress_matrix = fsParams->GetAdressMatrix()->pdata();
               sc_info->fs_parameters = fsParams->GetData();
            }

            pparams = new double[NUM_OF_FS_PARAMS];

            for(size_t i = 0; i < NUM_OF_FS_PARAMS; ++i) {
                stream >> pparams[i];
            }

            fsParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

        if(potential == "exfs") {
            if(exfsParams == nullptr) {
               exfsParams = new PotentialArray(NumOfTypes(), NUM_OF_EXFS_PARAMS);
               sc_info->potflag |= EXFS_FLAG;
               sc_info->exfs_bond_matrix = exfsParams->GetBondMatrix()->pdata();
               sc_info->exfs_adress_matrix = exfsParams->GetAdressMatrix()->pdata();
               sc_info->exfs_parameters = exfsParams->GetData();
            }

            pparams = new double[NUM_OF_EXFS_PARAMS];

            for(size_t i = 0; i < NUM_OF_EXFS_PARAMS; ++i) {
                stream >> pparams[i];
            }

            exfsParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

        if(potential == "sc") {
            if(scParams == nullptr) {
               scParams = new PotentialArray(NumOfTypes(), NUM_OF_SC_PARAMS);
               sc_info->potflag |= SC_FLAG;
               sc_info->sc_bond_matrix = scParams->GetBondMatrix()->pdata();
               sc_info->sc_adress_matrix = scParams->GetAdressMatrix()->pdata();
               sc_info->sc_parameters = scParams->GetData();
            }

            pparams = new double[NUM_OF_SC_PARAMS];

            for(size_t i = 0; i < NUM_OF_SC_PARAMS; ++i) {
                stream >> pparams[i];
            }

            scParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

         if(potential == "step") {
            if(stepParams == nullptr) {
               stepParams = new PotentialArray(NumOfTypes(), NUM_OF_STEP_PARAMS);
               sc_info->potflag |= STEP_FLAG;
               sc_info->step_bond_matrix = stepParams->GetBondMatrix()->pdata();
               sc_info->step_adress_matrix = stepParams->GetAdressMatrix()->pdata();
               sc_info->step_parameters = stepParams->GetData();
            }

            pparams = new double[NUM_OF_STEP_PARAMS];

            for(size_t i = 0; i < NUM_OF_STEP_PARAMS; ++i) {
                stream >> pparams[i];
            }

            stepParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }

        if(potential == "harm") {
            if(harmParams == nullptr) {
               harmParams = new PotentialArray(NumOfTypes(), NUM_OF_HARM_PARAMS);
               sc_info->potflag |= HARM_FLAG;
               sc_info->harm_bond_matrix = harmParams->GetBondMatrix()->pdata();
               sc_info->harm_adress_matrix = harmParams->GetAdressMatrix()->pdata();
               sc_info->harm_parameters = harmParams->GetData();
            }

            pparams = new double[NUM_OF_HARM_PARAMS];

            for(size_t i = 0; i < NUM_OF_HARM_PARAMS; ++i) {
                stream >> pparams[i];
            }

            harmParams->AddParams(i_comp1, i_comp2, pparams);
            return;
        }
    }

	if(pparams != nullptr)
    delete [] pparams;
}