#include "mtpsimulationconfig.hpp"
#include "metropolis.h"
#include "utils.hpp"
#include "statensemble.hpp"
#include "successors/successors.hpp"

CorrosionSuccessor* corrosion;

MTPSimulationConfig::MTPSimulationConfig() {
        directory = "";
        micro_steps = 600;
        macro_steps = 100;
        start_dumping_step = 90;
		xyz_dump_every = 10;
		ens_dump_every = 50;
        start_temperature = 293.15;
        finish_temperature = 1293.15;
        step_temperature = 10.0;
        time = 0.01;
		sigma = 2.0;
        load_cluster = "";
        xyz_dump_file = "";
        ans_dump_file = "";
		caloric_file = "";
        translation = "maxwell";
        seed = get_time();
		successor = nullptr;
		is_seeded = false;
        num_of_threads = 1;
		
		corrosion = new CorrosionSuccessor;
		successors.push_back((ISuccessor*)corrosion);
}

MTPSimulationConfig::~MTPSimulationConfig() {
	delete corrosion;
}

void MTPSimulationConfig::LoadFromToml(const std::string &file) {
    try {
        auto data = toml::parse(file);
        if(data.count("directory")) directory = toml::get<std::string>(data.at("directory"));
        if(data.count("micro-steps")) micro_steps = toml::get<size_t>(data.at("micro-steps"));
        if(data.count("macro-steps")) macro_steps = toml::get<size_t>(data.at("macro-steps"));
        if(data.count("start-dumping-step")) start_dumping_step = toml::get<size_t>(data.at("start-dumping-step"));
		if(data.count("xyz-dump-every")) xyz_dump_every = toml::get<size_t>(data.at("xyz-dump-every"));
		if(data.count("ens-dump-every")) ens_dump_every = toml::get<size_t>(data.at("ens-dump-every"));
        if(data.count("start-temperature")) start_temperature = toml::get<double>(data.at("start-temperature"));
        if(data.count("finish-temperature")) finish_temperature = toml::get<double>(data.at("finish-temperature"));
        if(data.count("step-temperature")) step_temperature = toml::get<double>(data.at("step-temperature"));
        if(data.count("potentials")) potentials = toml::get<std::vector<std::string>>(data.at("potentials"));
        if(data.count("mol-masses")) mol_masses = toml::get<std::vector<std::string>>(data.at("mol-masses"));
        if(data.count("load-cluster")) load_cluster = toml::get<std::string>(data.at("load-cluster"));
        if(data.count("xyz-dump-file")) xyz_dump_file = toml::get<std::string>(data.at("xyz-dump-file"));
        if(data.count("ens-dump-file")) ans_dump_file = toml::get<std::string>(data.at("ens-dump-file"));
		if(data.count("caloric-file")) caloric_file = toml::get<std::string>(data.at("caloric-file"));
        if(data.count("translation")) translation = toml::get<std::string>(data.at("translation"));
        if(data.count("time")) time = toml::get<double>(data.at("time"));
		if(data.count("sigma")) sigma = toml::get<double>(data.at("sigma"));
        if(data.count("seed")) seed = toml::get<int>(data.at("seed"));
        if(data.count("num_of_threads")) num_of_threads = toml::get<int>(data.at("num_of_threads"));
		
		if(data.count("successor")) {
			std::string successor_name = toml::get<std::string>(data.at("successor"));
			bool flag = false;
			
			auto end = successors.end();
			for(auto i = successors.begin(); i != end; ++i) {				
				if((*i)->GetName() == successor_name) {
					(*i)->LoadTomlFile(file);
					successor = *i;
					flag = true;
				}
			}
			
			if(!flag) {
				std::cout << "Successor '" << successor_name << "' is not found." << std::endl;
			}
		}
    }
    catch (const std::exception &exc) {
        std::cerr << "MTPSimulationConfig exception:\n" << exc.what() << std::endl;
        error = true;
    }
}

void MTPSimulationConfig::SaveTomlFile(
        const std::string &mtp_config_file    
    ) {
        std::ofstream outf(mtp_config_file);
        outf << "directory = \"" << directory << "\""<< std::endl;
        outf << std::endl;
        
        outf << "micro-steps = " << micro_steps << std::endl;
        outf << "macro-steps = " << macro_steps << std::endl;
        outf << "start-dumping-step = " << start_dumping_step << std::endl;
		outf << "xyz-dump-every = " << xyz_dump_every << " # dump every " << xyz_dump_every << " macrostep in xyz-file"<< std::endl;
		outf << "ens-dump-every = " << ens_dump_every << " # dump every " << ens_dump_every << " microstep in ens-file"<< std::endl;

        outf << std::endl;
        outf << "start-temperature = " << std::fixed << std::setprecision(2) << start_temperature << std::endl;
        outf << "finish-temperature = " << std::fixed << std::setprecision(2) << finish_temperature << std::endl;
        outf << "step-temperature = " << std::fixed << std::setprecision(2) << step_temperature << std::endl; 
        outf << std::endl;

        auto pot_end = potentials.end();
        outf << "potentials = [" << std::endl;
        for(auto  i = potentials.begin(); i != pot_end; ++i) {
            outf << "   \"" <<*i << "\"," << std::endl;
        }      
        outf << "]" << std::endl; 

        outf << std::endl;

        auto molmas_end = mol_masses.end();
        outf << "mol-masses = [" << std::endl;
        for(auto i = mol_masses.begin(); i != molmas_end; ++i) {
            outf << "   \"" << *i << "\"," << std::endl;
        }
        outf << "]" << std::endl; 
        outf << std::endl;

        outf << "load-cluster = \"" << load_cluster << "\"" << std::endl;
        outf << "xyz-dump-file = \"" << xyz_dump_file << "\"" << std::endl;
        outf << "ens-dump-file = \"" << ans_dump_file << "\"" << std::endl;
		outf << "caloric-file = \"" << caloric_file << "\"" << std::endl;

        outf << std::endl;
        outf << "translation = \"" << translation << "\"" << std::endl;
        outf << "time = " << time << " #ps" << std::endl;
		outf << "sigma = " << sigma << std::endl;

        outf << std::endl;
        outf << "seed = " << seed << std::endl;
        outf << "num_of_threads = " << num_of_threads << std::endl;
		
		outf.close();
}

void MTPSimulationConfig::FillMolMassesAndPotentials(const Cluster &cluster, PropertyReader &reader) {
        potentials.clear();
        mol_masses.clear();

        std::vector<std::string> names;

        cluster.GetChemicalNames(names);
        size_t num_of_types = cluster.NumOfTypes();    

        for(size_t i = 0; i < num_of_types; ++i) {
            std::stringstream stream;  
            double molmas = 1.0;
            reader.GetAtomProperty(names[i], "molmas", molmas);
            stream << names[i] << " " << molmas;
            mol_masses.push_back(stream.str());

            for(size_t j = i; j < num_of_types; ++j) {
                potentials.push_back(reader.GetPotentialParams("", names[i], names[j]));
            }
        }

}

void MTPSimulationConfig::FillMolMassesAndPotentials(const Cluster &cluster, CEParamsReader &reader) {
        potentials.clear();
        mol_masses.clear();

        std::vector<std::string> names;

        cluster.GetChemicalNames(names);
        size_t num_of_types = cluster.NumOfTypes();    

        for(size_t i = 0; i < num_of_types; ++i) {
            std::stringstream stream;  
            double molmas = reader.GetMolMass(names[i]);
            stream << names[i] << " " << molmas;
            mol_masses.push_back(stream.str());

            for(size_t j = i; j < num_of_types; ++j) {
                potentials.push_back(reader.ReadTBPotential(names[i], names[j]));
            }
        }

}

void MTPSimulationConfig::SetCluster(Cluster &cluster) {
    auto end_molmas = mol_masses.end();
    auto end_potentials = potentials.end();

    for(auto i = mol_masses.begin(); i != end_molmas; ++i) {
        cluster.SetMolMass(*i);
    }

    for(auto i = potentials.begin(); i != end_potentials; ++i) {
        cluster.AddPotentialParams(*i);
    }
}

inline bool CheckFinish(double tcurrent, double tfinish, double dt) {
    if(dt > 0.0) {
        return tcurrent <= tfinish;
    }
    else {
        return tcurrent >= tfinish;
    }
    
}

void SetPotentialFunction(bool multithreading, double (**pot_func)(int a, SceneInfo *sc), unsigned potflag) {
    if(!multithreading) {
          if(potflag == LJ_FLAG) *pot_func = get_lj_energy;
          if(potflag == MORSE_FLAG) *pot_func = get_lj_energy;
          if(potflag == GUPTA_FLAG) *pot_func = get_gupta_energy; 
          if(potflag == FS_FLAG) *pot_func = get_fs_energy;
          if(potflag == EXFS_FLAG) *pot_func = get_exfs_energy;
          if(potflag == SC_FLAG) *pot_func = get_sc_energy;
          if(potflag == STEP_FLAG) *pot_func = get_step_energy;
          if(potflag == HARM_FLAG) *pot_func = get_harm_energy;
          if(pot_func == nullptr) *pot_func = get_energy;
       }
       else {
          if(potflag == LJ_FLAG) *pot_func = get_lj_energy_multhreading;
          if(potflag == MORSE_FLAG) *pot_func = get_lj_energy_multhreading;
          if(potflag == GUPTA_FLAG) *pot_func = get_gupta_energy_multhreading;
          if(potflag == FS_FLAG) *pot_func = get_fs_energy_multhreading;
          if(potflag == EXFS_FLAG) *pot_func = get_exfs_energy_multhreading;
          if(potflag == SC_FLAG) *pot_func = get_sc_energy_multhreading;
          if(potflag == STEP_FLAG) *pot_func = get_step_energy_multhreading;
          if(potflag == HARM_FLAG) *pot_func = get_harm_energy_multhreading;
          if(pot_func == nullptr) *pot_func = get_energy_multhreading;
       }
}

bool LoadXYZConfig(const std::string &directory, const std::string &xyzlast, 
                   Cluster &cluster, double &tstart, size_t &mcstep, 
                   const std::string load_cluster, double start_temperature, FILE *out) {

    if(std::filesystem::exists(xyzlast)) {
           cluster.LoadFromFile(xyzlast, -1);
           tstart = as<double>(cluster.properties["temperature"]);
           mcstep = as<size_t>(cluster.properties["mcstep"]) + 1;
           return true;
    }
    else if (std::filesystem::exists(load_cluster)) {
           std::filesystem::create_directory(directory);
           cluster.LoadFromFile(load_cluster, -1);
           tstart = start_temperature;
           mcstep = 0;
           return true;
    }

    return false;
}

void SetMTPSimulation(MetropolisSimulation &mtp, const MTPSimulationConfig &cf, 
                      SceneInfo *sc) {
    mtp.scene = sc;
    mtp.maxwell_mode = (cf.translation == "maxwell");

    if(!mtp.maxwell_mode) mtp.max_translation = as<double>(cf.translation);
    mtp.time = cf.time;
	mtp.sigma = cf.sigma;
}

void RunSimulation(MetropolisSimulation &sim, 
                MTPSimulationConfig &mtpconfig, 
                double temperature, 
                double (*p_get_energy)(int, SceneInfo *), 
                int macrostep, const std::string &ansdump, 
                FILE *out, 
                bool benchmarking) {

    if(!benchmarking) {
        if(EQ_BY_EPS(temperature, 0.0) && macrostep < mtpconfig.start_dumping_step - 1) {
           fprintf(out," relaxing)\n");
           mtp_run_simulation_without_temperature(&sim, mtpconfig.micro_steps, p_get_energy);
        }
        else if(EQ_BY_EPS(temperature, 0.0) && macrostep >= mtpconfig.start_dumping_step) {
           fprintf(out," dumping)\n");
           mtp_run_simulation_without_temperature_with_dumping(&sim, mtpconfig.micro_steps, p_get_energy, ansdump.c_str(), mtpconfig.ens_dump_every);
        } 
        else if(macrostep < mtpconfig.start_dumping_step - 1) {
            fprintf(out," relaxing)\n");
            mtp_run_simulation(&sim, mtpconfig.micro_steps, temperature, p_get_energy); 
        }
        else {
            fprintf(out," dumping)\n");
            mtp_run_simulation_with_dumping(&sim, mtpconfig.micro_steps, temperature, p_get_energy, ansdump.c_str(), mtpconfig.ens_dump_every);
        }
    }
    else {
        if(EQ_BY_EPS(temperature, 0.0) && macrostep < mtpconfig.start_dumping_step - 1) {
           fprintf(out," relaxing)\n");
           mtp_run_simulation_without_temperature_bench(&sim, mtpconfig.micro_steps, p_get_energy);
        }
        else if(EQ_BY_EPS(temperature, 0.0) && macrostep >= mtpconfig.start_dumping_step) {
           fprintf(out," dumping)\n");
           mtp_run_simulation_without_temperature_with_dumping_bench(&sim, mtpconfig.micro_steps, p_get_energy, ansdump.c_str(), mtpconfig.ens_dump_every);
        } 
        else if(macrostep < mtpconfig.start_dumping_step - 1) {
            fprintf(out," relaxing)\n");
            mtp_run_simulation_bench(&sim, mtpconfig.micro_steps, temperature, p_get_energy); 
        }
        else {
            fprintf(out," dumping)\n");
            mtp_run_simulation_with_dumping_bench(&sim, mtpconfig.micro_steps, temperature, p_get_energy, ansdump.c_str(), mtpconfig.ens_dump_every);
        }
    }
}

void SaveResultCluster(MTPSimulationConfig &mtp, 
                 Cluster &cluster, 
                 const std::string &xyzdump, 
                 const std::string &xyzlast,
                 size_t macrostep,
                 double temperature,
                 FILE *out)                  {
    cluster.properties["temperature"] = as_string(temperature);
    cluster.properties["mcstep"] = as_string(macrostep);
    cluster.RefreshComment(true, true);
	cluster.SaveToFile(xyzlast);
			
	if(xyzdump != "" && (macrostep+1) % mtp.xyz_dump_every == 0) {
		fprintf(out, "     Changing '%s'...\n", xyzdump.c_str());
		cluster.SaveToFile(xyzdump, true);
	}
}

void CreateCaloricCurve(const std::string &ansdump, const std::string &caloric, FILE *out) {
    if(std::filesystem::exists(ansdump)) {
	    	fprintf(out, "Calculating caloric curve...\n");
	    	StatEnsemble ensemble(ansdump);
	    	ensemble.SaveCaloricCurve(caloric);
	}
}

void MTPSimulationConfig::Run(bool benchmarking, FILE *out) {
	while(true) {
       Cluster cluster;
       SceneInfo *scene = nullptr;
       double tstart;
       size_t mcstep;
       bool multithreading = false; // (num_of_threads > 1)
       std::string xyzlast = directory + "/" + "_last_xyz";
       double (*p_get_energy)(int, SceneInfo *) = nullptr;
       MetropolisSimulation simulation;

	   fprintf(out, "Preparing simulation...\n");

	   if(successor != nullptr) {
		   std::string mode = "'" + successor->GetName() + "' mode";
		   fprintf(out, "%s\n", mode.c_str());
	   }

       if(!LoadXYZConfig(directory, xyzlast, cluster, tstart, mcstep, 
                     load_cluster, start_temperature, out)) {
            fprintf(out, "Could not find file '%s'. Simulation is terminated\n", load_cluster.c_str());
            return;
        }

       SetCluster(cluster);
       scene = cluster.GetSceneInfo();

       if(scene->potflag == 0) {
          fprintf(out, "\n");
		  fprintf(out, "Potentials is not specified. Simulation is terminated.\n");
          fprintf(out, "\n");
          return;
       }

       SetPotentialFunction(multithreading, &p_get_energy, scene->potflag);
       SetMTPSimulation(simulation, *this, scene);

	   if (multithreading) {
		   init_multithreading(num_of_threads, scene);
#ifndef _WIN32
		   fprintf(out, "Multethreading is enabled...\nnum_of_threads = %lu\n", num_of_threads);
#else
		   fprintf(out, "Multethreading is enabled...\nnum_of_threads = %zu\n", num_of_threads);
#endif
		   fprintf(out, "num_of_cores = %u\n", get_hardware_concurrency());
	   }
    
	   std::string xyzdump = xyz_dump_file == "" ? "" : directory + "/" + xyz_dump_file;
       std::string ansdump = ans_dump_file == "" ? "" : directory + "/" + ans_dump_file;
	   std::string caloric = caloric_file  == "" ? "" : directory + "/" + caloric_file;
	
       if(tstart <= finish_temperature) step_temperature = fabs(step_temperature);
       else step_temperature = -fabs(step_temperature);

       fprintf(out, "\n");
       sceneinfo_print(scene, out);
       fprintf(out,"\n");

       fprintf(out, "Start temperature:  %f K\n", tstart);
       fprintf(out, "Finish temperature: %f K\n", finish_temperature);
       fprintf(out, "Step temperature:   %f K\n", step_temperature);
       fprintf(out,"\n");
 
#ifndef _WIN32
	   fprintf(out, "Number of microsteps: %lu\n", micro_steps);
	   fprintf(out, "Number of macrosteps: %lu\n", macro_steps);
       fprintf(out, "Start dumping with : %lu macrostep\n", start_dumping_step);
#else
	   fprintf(out, "Number of microsteps: %zu\n", micro_steps);
	   fprintf(out, "Number of macrosteps: %zu\n", macro_steps);
	   fprintf(out, "Start dumping with : %zu macrostep\n", start_dumping_step);
#endif

       if(!is_seeded) {
	      fprintf(out, "\n");
          fprintf(out, "Number generator is seeded by number: %d\n", seed);
	      fprintf(out, "\n");
          init_gen_rand(seed);
		  is_seeded = true;
	   }
    
       fprintf(out,"Running simulation...\n");
       fprintf(out, "\n");
	
       for(double T = tstart; CheckFinish(T, finish_temperature, step_temperature); T += step_temperature) {
           fprintf(out, "---> Start temperature %f K\n", T);
           for(size_t i = mcstep; i < macro_steps; ++i) {

#ifndef  _WIN32
			   fprintf(out, "     start macrostep %lu (%f K ", i + 1, T);
#else
			   fprintf(out, "     start macrostep %zu (%f K ", i + 1, T);
#endif // ! _WIN32
               RunSimulation(simulation, *this, T, p_get_energy, i, ansdump, out, benchmarking);
               SaveResultCluster(*this, cluster, xyzdump, xyzlast, i, T, out);
            }

            fprintf(out,"\n");
            mcstep = 0;
        }
		
	    CreateCaloricCurve(ansdump, caloric, out);
        fprintf(out, "Simulation is finished...\n\n");
		
		if(successor == nullptr) break;
		if(!successor->Next(*this)) break;	

        if(multithreading) destroy_multithreading();
	}
}