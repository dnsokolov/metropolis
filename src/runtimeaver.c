#include "runtimeaver.h"

struct runtime_aver_tag {
    size_t n;
    size_t reset_number;
    double average_value;
};

runtime_aver *create_runtime_aver(size_t reset_number) {
    runtime_aver *rta = (runtime_aver *) malloc(sizeof(runtime_aver));
    rta->n = 0;
    rta->reset_number = reset_number;
    rta->average_value = 0.0;
    return rta;
}

void push_number_runtime_aver(runtime_aver *rta, double a) {
    if(rta->n == 0) {
        rta->average_value = a;
        rta->n = 1;
        return;
    }

    ++rta->n;
    rta->average_value = ((rta->n-1) * rta->average_value + a)/rta->n;

    if(rta->n >= rta->reset_number) rta->n = 0;

}

double get_average_value_runtime_aver(runtime_aver *rta) {
    return rta->average_value;
}

void destroy_runtime_aver(runtime_aver *rta) {
    free(rta);
}
