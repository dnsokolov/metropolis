#include "atom.hpp"

std::ostream& operator<< (std::ostream &out, const Atom &atom) {
    out << atom.ename << " (" << atom.x << ", " << atom.y << ", " << atom.z << ") " << atom.energy << " " << atom.freeze;
    return out;
}