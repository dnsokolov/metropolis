#include "threadpool.hpp"
#include "thread_pool.h"

thread_pool_t thread_pool_create(size_t num_of_threads, void* (*task_func)(void*), array_args_t* args) {
	ArrayArgs* _args = (ArrayArgs*)args;
	ThreadPool *pool = new ThreadPool(num_of_threads, task_func, _args);
	return pool;
}

void thread_pool_do_work(thread_pool_t ppool) {
	ThreadPool* pool = static_cast<ThreadPool*>(ppool);
	pool->RunTasks();
}

void thread_pool_close(thread_pool_t ppool) {
	ThreadPool* pool = static_cast<ThreadPool*>(ppool);
	pool->Stop();
	delete pool;
}