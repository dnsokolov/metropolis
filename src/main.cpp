#include "commands/allcommands.hpp"
#include "sceneinfo.h"
#include "cluster.hpp"

using namespace std;

bool print_information(int argc, char **argv, std::vector<ICommand*> &commands) {

    if(argc == 1) {
       string banner[] = {
            R"===(----------------------------------------------)===",
            R"===(             _               ___      _ _     )===",
            R"===(  /\/\   ___| |_ _ __ ___   / _ \___ | (_)___ )===",
            R"===( /    \ / _ \ __| '__/ _ \ / /_)/ _ \| | / __|)===",
            R"===(/ /\/\ \  __/ |_| | | (_) / ___/ (_) | | \__ \)===",
            R"===(\/    \/\___|\__|_|  \___/\/    \___/|_|_|___/)===",
            R"===(                                              )===",
            R"===(----------------------------------------------)===",
            R"===( Standart edition                       v 1.7 )===",
            R"===(----------------------------------------------)==="
        };

        for(size_t i = 0; i < 10; ++i) {
           cout << banner[i] << endl;
        }

        #ifdef ENABLE_AVX
        cout << "AVX version" << endl << endl;
        #endif

        cout << "(C) Tver State University 2020" << endl << endl;
        cout << "Authors:\n Denis Sokolov,\n Nikolay Sdobnyakov,\n Andrey Kolosov,\n Vladimir Myasnichenko,\n"
            " Pavel Ershov,\n Sergey Bogdanov" << endl << endl;
        cout << "Email: dnsokolov@msn.com" << endl;
			
        cout << endl; 
	    cout << "The program is dedicated to Sokolova Valentina Ivanovna" << endl;
	    cout << endl; 

        if(argc == 1) {
           cout << "Enter 'mtp help' for more information" << endl;
           cout << endl;
           return true;
        }
    }

    string argv_1(argv[1]);

    if(argc == 2 && argv_1 == "help") {
        auto end = commands.end();
        cout << endl;
        for(auto i = commands.begin(); i != end; ++i) {
            cout << "> " <<(*i)->GetHelp() << endl;
            cout << endl;
        }
        return true;
    }

    
    if(argc == 3 && argv_1 == "help") {
        string argv_2(argv[2]);
        auto end = commands.end();
        cout << endl;
        for(auto i = commands.begin(); i != end; ++i) {
            if(argv_2 == (*i)->GetName()){
               cout << "> " <<(*i)->GetHelp() << endl;
               cout << endl;
               return true;
            }
        }
        cout << "Can not find help iformation for '" << argv_2 << "' command" << endl;
        cout << endl;
        return true;
    }

    return false;
}

int main(int argc, char **argv) {
	string property_file = get_executable_directory() + "/properties.toml";
    if(!filesystem::exists(property_file)) {
		cout << "'"<< property_file << "' is not found. Program is terminated." << endl;
		cout << endl;
		return 1;
	} 
	

    PropertyReader pr(property_file);
    if(pr.IsError()) {
        cout << "Program is terminated." << std::endl;
        return 2;
    }

    vector<ICommand*> commands;

    NewCommand newCommand(&pr);
    commands.push_back((ICommand*)&newCommand);

    RunCommand runCommand;
    commands.push_back((ICommand*)&runCommand);
	
	CaloricCommand caloricCommand;
	commands.push_back((ICommand*)&caloricCommand);
	
	ClusterCommand clusterCommand(&pr);
	commands.push_back((ICommand*)&clusterCommand);

    ZCommand zCommand;
    commands.push_back((ICommand*)&zCommand);

    AverageCommand avCommand;
    commands.push_back((ICommand*) &avCommand);
	
	TestCommand testCommand;
	commands.push_back((ICommand*)&testCommand);

    if(!print_information(argc, argv, commands) && argc > 1) {
        string cmd = argv[1];
        vector<string> args;

        if(argc > 2) {
            for(size_t i = 2; i < argc; ++i) {
                args.push_back(argv[i]);
            }
        }

        auto end = commands.end();
        for(auto i = commands.begin(); i != end; ++i) {
            if((*i)->GetName() == cmd) {
                (*i)->Run(args);
                return 0;
            }
        }

        cout << endl;
        cout << "Can not find command '" << cmd << "'" << endl;
        cout << endl;
    }

    return 0;
}