#include "modifiers/deletemodifier.hpp"

std::string DeleteModifier::GetName() {
	return "del";
}

std::string DeleteModifier::GetHelp() {
	std::string help = "'del startindex [count]' or 'del list num1 num2 ...' - deletes atoms in cluster, starting with 'startindex' in the amount of 'count' pieces."
	"If specified parametr 'list' then deletes atoms with indexes 'num1', 'num2', ...";
	return help;
}

int DeleteModifier::Apply(Cluster &cluster, const std::vector<std::string> &args) {
	
	size_t asize = args.size();
	
	if(asize == 0) {
		std::cout << "Wrong usage. Enter 'help del' for more information." << std::endl;
        return 1; 
	}
	
	if(asize >= 1 ) {
		if(args[0] == "list") {
			if(asize < 2) return 0;
			std::vector<size_t> indexes;
			
			for(size_t i = 1; i < asize; ++i) {
				size_t index = as<size_t>(args[i]);
				indexes.push_back(index);
			}
			
			std::sort(indexes.begin(), indexes.end(), [](size_t i, size_t j) {
				return i > j;
			});
			
			auto end = indexes.end();
			for(auto i = indexes.begin(); i != end; ++i) {
				cluster.DeleteAtom(*i);
			}
			
			std::cout << "Cluster is changed." << std::endl;
			return 0;
		}
		else if(args[0] == "minen"){
			std::cout << "Deleting atom with minimal energy..." << std::endl;
            int index; 
			double energy = cluster.FindMinEnergy(index); 
			
			if(index == -1) return 2;
			
			Atom a;
			cluster.GetAtom(index, a);
			std::cout << "Atom with minimal energy (index = " << index <<"): " << a << std::endl;
			cluster.DeleteAtom(index);
			std::cout << "Cluster is changed." << std::endl;
			return 0;
		}
		else if(args[0] == "maxen"){
			std::cout << "Deleting atom with maximal energy..." << std::endl;
            int index; 
			double energy = cluster.FindMaxEnergy(index); 
			
			if(index == -1) return 2;
			
			Atom a;
			cluster.GetAtom(index, a);
			std::cout << "Atom with maximal energy (index = " << index <<"): " << a << std::endl;
			cluster.DeleteAtom(index);
			std::cout << "Cluster is changed." << std::endl;
			return 0;
		}
		else {
			size_t index, count;
			index = as<size_t>(args[0]);
			
			count = asize > 1 ? as<size_t>(args[1]) : 1;
			
			for(size_t i = 0; i < count; ++i) {
				cluster.DeleteAtom(index);
			}
			
			std::cout << "Cluster is changed." << std::endl;
			return 0;
		}
	}
	
	return 0;
}

