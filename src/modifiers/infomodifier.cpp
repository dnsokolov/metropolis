#include "modifiers/infomodifier.hpp"

std::string InfoModifier::GetName() {
	return "info";
}

std::string InfoModifier::GetHelp() {
	std::string help = "'info [atom]' - get information about current cluster. "
	"If specified 'atom' parameter (index of atom) then will printing information about this atom.";
	return help;
}

int InfoModifier::Apply(Cluster &cluster, const std::vector<std::string> &args) {
	if(args.size() >= 1) {
		
		if(args[0] == "maxen") {
			int index;
			double energy = cluster.FindMaxEnergy(index);
			if(index != -1) {
				Atom a;
		        cluster.GetAtom(index, a);
				std::cout <<"Atom '" << index << "': "<< a << std::endl;
			}
			return 0;
		}
		
		if(args[0] == "minen") {
			int index;
			double energy = cluster.FindMinEnergy(index);
			if(index != -1) {
				Atom a;
		        cluster.GetAtom(index, a);
				std::cout <<"Atom '" << index << "': "<< a << std::endl;
			}
			return 0;
		}
		
		if(args[0] == "scene") {
			SceneInfo *sc = cluster.GetSceneInfo();
			sceneinfo_print(sc, stdout);
			return 0;
		}
		
		size_t index = as<size_t>(args[0]);
		Atom a;
		cluster.GetAtom(index, a);
		std::cout <<"Atom '" << index << "': "<< a << std::endl;
		return 0;
	}
	
	if(cluster.GetFileName() != "") {
		std::cout << "Cluster is loaded from '" << cluster.GetFileName() << "'" << std::endl << std::endl;
	}
	std::cout << "Number of atoms in the cluster: '" << cluster.NumOfAtoms() << "'" << std::endl;
    std::cout << "Number of components in the cluster: '" << cluster.NumOfTypes() << "'" << std::endl; 
	if(cluster.NumOfTypes() > 0) {
		std::cout << std::endl << "Components:";
		std::vector<std::string> components;
		cluster.GetChemicalNames(components);
		
		for(auto &name : components) {
			std::cout << " '" << name << "'";
		}
		
		std::cout << std::endl << std::endl;
		
		for(auto &name : components) {
			std::cout << "Number of '" << name << "': '" << cluster.NumOfAtoms(name) << "' atoms" << std::endl;
		}
		
		std::cout << std::endl;
	}

	return 0;
}

