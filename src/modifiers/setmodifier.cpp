#include "modifiers/setmodifier.hpp"
#include "ceparamsreader.hpp"

std::string SetModifier::GetName() {
	return "set";
}

std::string SetModifier::GetHelp() {
	std::string help = "'set option ' - setting some 'option' to cluster. Example 'set gupt ...' - setting gupta parameters for calculating energy.";
	return help;
}

int SetModifier::Apply(Cluster &cluster, const std::vector<std::string> &args) {
	
	size_t asize = args.size();
	
	if(asize == 0) {
		std::cout << "'option' is not specified. For more information enter 'help set'" << std::endl;
		return 1;
	}
	
	std::string option = args[0];
	std::vector<std::string> potentials = {
		"gupt",
		"lj",
		"morse",
		"fs",
		"exfs",
		"step",
		"harm",
		"sc"
	};
	
	if(index_of<std::string>(potentials, option) != -1) {
		
		std::cout << "Setting '" << option << "' potential parameters..." << std::endl;
		size_t num_of_types = cluster.NumOfTypes();
		std::string potstr;
		std::vector<std::string> names;
		cluster.GetChemicalNames(names);
		
		for(size_t i = 0; i < num_of_types; ++i) {
			potstr = pr->GetPotentialParams(option, names[i], names[i]);
			if(potstr != "") {
				std::cout << potstr << std::endl;
				cluster.AddPotentialParams(potstr);
			}
			
			for(size_t j = i + 1; j < num_of_types; ++j) {
				potstr = pr->GetPotentialParams(option, names[i], names[j]);
				if(potstr != "") {
					std::cout << potstr << std::endl;
					cluster.AddPotentialParams(potstr);
				}
			}
		}
		
		return 0;
	}
	
	if(option == "energy") {
		std::cout << "Setting energy to atoms in the cluster..." << std::endl;
		cluster.SetEnergies();
		
		std::cout << "Energy per atom: " << cluster.GetEnergyPerAtoms() << std::endl;
		return 0;
	}

	if(option == "ce") {
		std::filesystem::path current_path = std::filesystem::current_path();
        std::string ext, cefile = "";

		for (const auto &entry : std::filesystem::directory_iterator(current_path)) {
            if(entry.is_regular_file()) {
                ext = entry.path().extension().string();
                std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

                if(cefile == "" && ext == ".dat") {
                    cefile = entry.path().filename().string();
					break;
                }
            }
        }

        std::cout << "CE file: " << cefile << std::endl;
		CEParamsReader reader(cefile);
		std::cout << "Setting '" << option << "' potential parameters..." << std::endl;
		size_t num_of_types = cluster.NumOfTypes();
		std::string potstr;
		std::vector<std::string> names;
		cluster.GetChemicalNames(names);
		
		for(size_t i = 0; i < num_of_types; ++i) {
			potstr = reader.ReadTBPotential(names[i], names[i]);
			
			if(potstr != "") {
				std::cout << potstr << std::endl;
				cluster.AddPotentialParams(potstr);
			}
			
			for(size_t j = i + 1; j < num_of_types; ++j) {
				potstr = reader.ReadTBPotential(names[i], names[j]);
				if(potstr != "") {
					std::cout << potstr << std::endl;
					cluster.AddPotentialParams(potstr);
				}
			}
		}
		
		return 0;
		
	}
	
	std::cout << "Option '" << option << "' is not found" << std::endl;
	return 2;
}

