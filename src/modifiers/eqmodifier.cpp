#include "modifiers/eqmodifier.hpp"
#include <algorithm>
#include "atom.hpp"

std::string EqModifier::GetName() {
	return "eq";
}

std::string EqModifier::GetHelp() {
	std::string help = "'eq xyzfile' - compares with a cluster from a xyzfile. ";
	return help;
}

int EqModifier::Apply(Cluster &cluster, const std::vector<std::string> &args) {
   
	if(args.size() == 0) {
		std::cout << "Wrong usage. No xyzfile specified." << std::endl;
        return 1; 
	}
	
	if(!std::filesystem::exists(args[0])) {
		std::cout << "File '" << args[0] << "' is not found" << std::endl;
		return 2;
	}
	
	Cluster l_cluster(args[0]);
	size_t num_of_atoms = l_cluster.NumOfAtoms();
	
	if(num_of_atoms != cluster.NumOfAtoms()) {
		std::cout << "The number of atoms does not match" << std::endl;
		return 0;
	}
	
	if(l_cluster.NumOfTypes() != cluster.NumOfTypes()) {
		std::cout << "The number of types does not match" << std::endl;
		return 0;
	}
	
	std::vector<std::string> l_chem_names;
	std::vector<std::string> chem_names;
	
	l_cluster.GetChemicalNames(l_chem_names);
	cluster.GetChemicalNames(chem_names);
	
	auto l_end = l_chem_names.end();
	auto end = chem_names.end();
	
	for(auto i = l_chem_names.begin(); i != l_end; ++i) {
		if(std::find(chem_names.begin(), end, *i) == end) {
			std::cout << "Chemical names does not match" << std::endl;
			return 0;
        }
	}
	
	Atom a,b;
	double energy_dif = 0.0;
	double position_dif = 0.0;
	int name_dif = 0;
	double dif;
	
	
	for(size_t i = 0; i < num_of_atoms; ++i) {
		cluster.GetAtom(i, a);
		l_cluster.GetAtom(i, b);
		
		dif = a.energy - b.energy;
		energy_dif += dif*dif;
		
		dif = a.x - b.x;
		position_dif += dif*dif;
		
		dif = a.y - b.y;
		position_dif += dif*dif;
		
		dif = a.z - b.z;
		position_dif += dif*dif;
		
		name_dif += (a.ename == b.ename)? 0 : 1;
	}
	
	energy_dif = std::sqrt(energy_dif/num_of_atoms);
	position_dif = std::sqrt(position_dif/num_of_atoms);
	
	std::cout << "Difference in energy:    " << energy_dif << std::endl;
	std::cout << "Difference in positions: " << position_dif << std::endl;
	std::cout << "Difference in names:     " << name_dif << std::endl;
	
    return 0;	
}

