#include "modifiers/loadmodifier.hpp"

std::string LoadModifier::GetName() {
	return "load";
}

std::string LoadModifier::GetHelp() {
	std::string help = "'load filename.xyz [frame]' - change current cluster from 'filename.xyz'";
	return help;
}

int LoadModifier::Apply(Cluster &cluster, const std::vector<std::string> &args) {
	if(args.size() == 0) {
		std::cout << "File name is not specified" << std::endl;
		return 1;
	}
	
	if(!std::filesystem::exists(args[0])) {
		std::cout << "File '" << args[0] << "' is not found" << std::endl;
		return 2;
	}
	
	int frame = args.size() >= 2 ? as<int>(args[1]) : 0;
	cluster.LoadFromFile(args[0], frame);
	std::cout << "Cluster is loaded from '" << args[0] << "' frame '" << frame << "'" << std::endl;
	return 0;
}

