#include "framehandlers/coordnumberframehandler.hpp"

double *get_hyst(SceneInfo *sc_info, double r) {
    double *hyst = (double *) malloc(sc_info->num_of_atoms * sizeof(double));
    memset(hyst, 0, sc_info->num_of_atoms * sizeof(double));
    for(size_t i = 0; i < sc_info->num_of_atoms; ++i) {
        for(size_t j = i + 1; j < sc_info->num_of_atoms; ++j) {
            if(get_dist(i, j, sc_info) <= r) {
                ++hyst[i]; ++hyst[j];
            }
        }
    }
    return hyst;
}

void CoordNumberFrameHandler::operator()(SceneInfo *sc_info) {
    if(flag && temperature != sc_info->temperature) {
        double zt = 0.0, sigma = 0.0;
        average_data(z, zt, sigma);
        ZT.try_emplace(temperature, std::make_pair(zt, sigma));
        z.clear();
    }

    temperature = sc_info->temperature;
    flag = true;

    double *hyst = get_hyst(sc_info, _r);
    double zt = 0.0;

    average_data(hyst, sc_info->num_of_atoms, zt);
    z.push_back(zt);
    free(hyst);
}

void CoordNumberFrameHandler::End() {
    double zt = 0.0, sigma = 0.0;
    average_data(z, zt, sigma);
    ZT.try_emplace(temperature, std::make_pair(zt, sigma));
    z.clear();
    flag = false;
}

void CoordNumberFrameHandler::GetZT(std::map<double,std::pair<double,double>> &zt) const {
    zt = ZT;
}
