#include "successors/corrosionsuccessor.hpp"

std::string CorrosionSuccessor::GetName() {
	return "corrosion";
}

void CorrosionSuccessor::LoadTomlFile(const std::string &filename) {
	auto data = toml::parse(filename);
	std::string name = GetName();
	tomlfile = filename;
	
	if(data.count(name) != 0) {
		toml::Table table = toml::get<toml::Table>(data.at(name));
		if(table.count("delete") != 0) del = toml::get<int>(table.at("delete"));
		if(table.count("stop") != 0) stop = toml::get<int>(table.at("stop"));
	}
}

bool CorrosionSuccessor::Next(MTPSimulationConfig &config) {	
	if(!std::filesystem::exists(config.directory + "/_last_xyz")) return false;
	Cluster cluster(config.directory + "/_last_xyz");
	
	size_t max = -1;
	cluster.properties["mcstep"] = as_string(max);
	
	if(cluster.NumOfAtoms() - del < stop) return false;
	config.SetCluster(cluster);
	int index;
	
	std::string cluster_name = "";
	std::vector<std::string> names;
	cluster.GetChemicalNames(names);
	
	for(const auto &name: names) {
		cluster_name += name + as_string<size_t>(cluster.NumOfAtoms(name)) + "_";
	}
	
	cluster.SaveToFile(config.directory + "/" + cluster_name + ".xyz");
	
	std::string energyfile = config.directory + "/energies.dat";
	if(!std::filesystem::exists(energyfile)) {
		std::ofstream enstream(energyfile);
		enstream << "N,atom E,eV/atom Sigma,eV/atom" << std::endl;
		enstream.close();
	}
	
	std::ofstream outfile;
	outfile.open(energyfile, std::ios_base::app);
	
	SceneInfo *sc = cluster.GetSceneInfo();
	double energy, sigma;
	
	std::cout << cluster.NumOfAtoms() << std::endl;
	
	average_data(sc->energies, sc->num_of_atoms, energy, sigma);
	outfile << sc->num_of_atoms << " " << energy << " " << sigma << std::endl;
	outfile.close();
	
	Atom a;
	
	for(size_t i = 0; i < del; ++i) {
		cluster.SetEnergies();
		cluster.FindMaxEnergy(index);
		if(index < 0) return false;
		
		std::cout << "index = " << index << std::endl;
		
		cluster.GetAtom(index, a);
		cluster.DeleteAtom(index);
		
		std::cout << "Atom '" << index << "' : " << a << " is deleted" << std::endl;
		
	}
	
	cluster.RefreshComment(true, true);
	cluster.SaveToFile(config.directory + "/_last_xyz");
	return true;
}

