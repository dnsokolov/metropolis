#include "mtp_array_int.h"

struct mtp_array_int_tag {
    int *data;
    size_t size;
    size_t capacity;
};

mtp_array_int *mtp_array_int_create(size_t capacity) {
    capacity = capacity == 0 ? 10 : capacity;
    mtp_array_int *array = (mtp_array_int *) malloc(sizeof(mtp_array_int));
    array->data = (int*) malloc(sizeof(int) * capacity);
    array->capacity = capacity;
    array->size = 0;
}

size_t mtp_array_int_capacity(mtp_array_int *self) {
    return self->capacity;
}

size_t mtp_array_int_size(mtp_array_int *self) {
    return self->size;
}

void mtp_array_int_destroy(mtp_array_int *self) {
    free(self->data);
    free(self);
}

int *mtp_array_int_data(mtp_array_int *self) {
    return self->data;
}

int mtp_array_int_nth(mtp_array_int *self, size_t i) {
    return self->data[i];
}

void mtp_array_int_append(mtp_array_int *self, int a) {
    ++self->size;

    if(self->size > self->capacity) {
        self->capacity *= 2;
        self->data = (int*) realloc(self->data, sizeof(int) * self->capacity);
    }

    self->data[self->size - 1] = a;
}

void mtp_array_int_append_if_not_exists(mtp_array_int *self, int a) {
    if(mtp_array_int_index_of(self, a) == NPOS) {
        mtp_array_int_append(self, a);
    }
}

size_t mtp_array_int_index_of(mtp_array_int *self, int a) {
    size_t i = 0;
    for(;i<self->size;++i) {
        if(a == self->data[i]) return i;
    }
    return NPOS;
}

void mtp_array_int_remove_nth(mtp_array_int *self, size_t i) {

    if(i == self->size) {
        --self->size;
        return;
    }

    size_t bytes = (self->size - (i + 1)) * sizeof(int);
    int *temp = (int *) malloc(bytes);
    memcpy(temp, (self->data + i + 1), bytes);
    memcpy(self->data + i, temp, bytes);
    --self->size;
    free(temp);
}

void mtp_array_int_clear(mtp_array_int *self) {
    self->size = 0;
}
