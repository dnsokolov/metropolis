#include "metropolis.h"
#include <math.h>
#include <string.h>

int _num_launch = 0;

void rnd_translation(MetropolisSimulation *simulation, int atom, double temperature, double *tr) {
    tr[0] = RND(-simulation->max_translation, simulation->max_translation);
    tr[1] = RND(-simulation->max_translation, simulation->max_translation);
    tr[2] = RND(-simulation->max_translation, simulation->max_translation);
}

/*https://en.wikipedia.org/wiki/Box%E2%80%93Muller_transform*/
void maxwell_translation(MetropolisSimulation *simulation, int atom, double temperature, double *tr) {
	double r1, r2, z1, z2, z3, abs_z; 
	double molar_massa = simulation->scene->molmasses[simulation->scene->types[atom]];
	
	again1:
       r1 = genrand_open_close();
       r2 = genrand_close_open();
	
       z1 = sqrt(-2.0 * log(r1)) * cos(MTP_TWO_PI * r2);
	   abs_z = abs(z1);	
	if(simulation->sigma > 0 && abs_z > simulation->sigma) goto again1;
	
    tr[0] = z1 * SQRT_R * sqrt(temperature/molar_massa) * simulation->time; /*A*/

    again2:
       r1 = genrand_open_close();
       r2 = genrand_close_open();
	
       z2 = sqrt(-2.0 * log(r1)) * cos(MTP_TWO_PI * r2);
	   abs_z = abs(z2);	
	if(simulation->sigma > 0 && abs_z > simulation->sigma) goto again2;
	
    tr[1] = z2 * SQRT_R * sqrt(temperature/molar_massa) * simulation->time; /*A*/	

    again3:
       r1 = genrand_open_close();
       r2 = genrand_close_open();
	
       z3 = sqrt(-2.0 * log(r1)) * cos(MTP_TWO_PI * r2);
	   abs_z = abs(z3);	
	if(simulation->sigma > 0 && abs_z > simulation->sigma) goto again3;
	
    tr[2] = z3 * SQRT_R * sqrt(temperature/molar_massa) * simulation->time; /*A*/ 	
}

double get_full_energy(SceneInfo *sc_info, double (*pget_energy)(int a, SceneInfo *sc_info)) {
    double e = 0.0;
    for(size_t i = 0; i < sc_info->num_of_atoms; ++i) {
        e += pget_energy(i, sc_info);
    }
    return e;
}

void mtp_run_simulation (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*pget_energy)(int a, SceneInfo *sc_info)
) {
    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans, 4);
	double time1, time2;

    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif

    trans[3] = 0.0;

    scene->temperature = temperature;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        for(size_t a = 0; a < num_of_atoms; ++a) {
            if(scene->frozen[a]) continue;

            //E1 = pget_energy(a, scene);
            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, temperature, trans);
            TRANSLATE(pos,trans);

            //E2 = pget_energy(a, scene);    
            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                p = exp(-(E2-E1)/(kB * temperature));
                if(genrand_close_open() > p) {
                    TRANSLATE_BACK(pos,trans);
                    scene->energies[a] = E1;
                }
            }
        }
    }

   // for(size_t i = 0; i < num_of_atoms; ++i) get_energy(i, scene);
	FREE_ARRAY(trans);
}

void mtp_run_simulation_with_dumping (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*pget_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
) {
    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);// __attribute__((aligned(32)));
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif

    trans[3] = 0.0;

    scene->temperature = temperature;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        for(size_t a = 0; a < num_of_atoms; ++a) {
            if(scene->frozen[a]) continue;

            //E1 = pget_energy(a, scene);
            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, temperature, trans);
            TRANSLATE(pos,trans);

            //E2 = pget_energy(a, scene);    
            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                p = exp(-(E2-E1)/(kB * temperature));
                if(genrand_close_open() > p) {
                    TRANSLATE_BACK(pos, trans);
                    scene->energies[a] = E1;
                }
            }
        }
		
        for(size_t k = 0; k < num_of_atoms; ++k) get_energy(k, scene);
	
	    if(strcmp(dump_file,"") && (i % dump_every == 0)) 
			sceneinfo_dump(scene, dump_file);
    }

	FREE_ARRAY(trans);
}

void mtp_run_simulation_without_temperature (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*pget_energy)(int a, SceneInfo *sc_info)
) {
    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);// __attribute__((aligned(32)));
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif

    trans[3] = 0.0;

    scene->temperature = 0.0;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        for(size_t a = 0; a < num_of_atoms; ++a) {
            if(scene->frozen[a]) continue;

            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, simulation->max_translation, trans);
            TRANSLATE(pos,trans);

            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                TRANSLATE_BACK(pos, trans);
                scene->energies[a] = E1;
            }
        }
    }

    //for(size_t i = 0; i < num_of_atoms; ++i) get_energy(i, scene);
	FREE_ARRAY(trans);
}

void mtp_run_simulation_without_temperature_with_dumping (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*pget_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
) {
    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);// __attribute__((aligned(32)));
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif
    trans[3] = 0.0;

    scene->temperature = 0.0;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        for(size_t a = 0; a < num_of_atoms; ++a) {
            if(scene->frozen[a]) continue;

            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, simulation->max_translation, trans);
            TRANSLATE(pos,trans);

            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                TRANSLATE_BACK(pos,trans);
                scene->energies[a] = E1;
            }
            
        }
        
		
        for(size_t k = 0; k < num_of_atoms; ++k) get_energy(k, scene);
		
		if(strcmp(dump_file,"") && (i % dump_every == 0)) 
			sceneinfo_dump(scene, dump_file);
    }
	FREE_ARRAY(trans);
}

/********* Benchmarking version ************/

void mtp_run_simulation_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*pget_energy)(int a, SceneInfo *sc_info)
) {
    runtime_aver *perAtom = create_runtime_aver(100000);
    runtime_aver *perSteps = create_runtime_aver(100000);
    double now = getCPUtime(), atomNow, stepNow, finish;

    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif
    trans[3] = 0.0;

    scene->temperature = temperature;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        stepNow = getCPUtime();

        for(size_t a = 0; a < num_of_atoms; ++a) {
            atomNow = getCPUtime();
            if(scene->frozen[a]) continue;

            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, temperature, trans);
            TRANSLATE(pos,trans);

            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                p = exp(-(E2-E1)/(kB * temperature));
                if(genrand_close_open() > p) {
                    TRANSLATE_BACK(pos,trans);
                    scene->energies[a] = E1;
                }
            }
            push_number_runtime_aver(perAtom, getCPUtime() - atomNow);
        }

        push_number_runtime_aver(perSteps, getCPUtime() - stepNow);
    }

    //for(size_t i = 0; i < num_of_atoms; ++i) get_energy(i, scene);
    
    finish = getCPUtime();

    printf("Benchmarking\n--------------------------------\n");
    printf("Time:        %f (s)       |\n", finish - now);
    printf("Microstep:   %f (s)       |\n", get_average_value_runtime_aver(perSteps));
    printf("Translation: %f (s)       |\n", get_average_value_runtime_aver(perAtom));
    printf("TPSA:        %u (Hz/atom)      |\n", (unsigned) (1.0/ num_of_atoms / get_average_value_runtime_aver(perAtom)));
    printf("--------------------------------\n");

    destroy_runtime_aver(perAtom);
    destroy_runtime_aver(perSteps);

	FREE_ARRAY(trans);
}

void mtp_run_simulation_with_dumping_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double temperature,
    double (*pget_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
) {
    runtime_aver *perAtom = create_runtime_aver(100000);
    runtime_aver *perSteps = create_runtime_aver(100000);
    double now = getCPUtime(), atomNow, stepNow, finish;

    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);// __attribute__((aligned(32)));
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif

    trans[3] = 0.0;

    scene->temperature = temperature;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        stepNow = getCPUtime();

        for(size_t a = 0; a < num_of_atoms; ++a) {
            atomNow = getCPUtime();

            if(scene->frozen[a]) continue;

            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, temperature, trans);
            TRANSLATE(pos,trans);

            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                p = exp(-(E2-E1)/(kB * temperature));
                if(genrand_close_open() > p) {
                    TRANSLATE_BACK(pos,trans);
                    scene->energies[a] = E1;
                }
            }

            push_number_runtime_aver(perAtom, getCPUtime() - atomNow);
        }
		
        for(size_t k = 0; k < num_of_atoms; ++k) get_energy(k, scene);
	
	    if(strcmp(dump_file,"") && (i % dump_every == 0)) 
			sceneinfo_dump(scene, dump_file);
        
        push_number_runtime_aver(perSteps, getCPUtime() - stepNow);
    }

    finish = getCPUtime();

    printf("Benchmarking\n--------------------------------\n");
    printf("Time:        %f (s)       |\n", finish - now);
    printf("Microstep:   %f (s)       |\n", get_average_value_runtime_aver(perSteps));
    printf("Translation: %f (s)       |\n", get_average_value_runtime_aver(perAtom));
    printf("TPSA:        %u (Hz/atom)      |\n", (unsigned) (1.0/ num_of_atoms / get_average_value_runtime_aver(perAtom)));
    printf("--------------------------------\n");

    destroy_runtime_aver(perAtom);
    destroy_runtime_aver(perSteps);

	FREE_ARRAY(trans);
}

void mtp_run_simulation_without_temperature_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*pget_energy)(int a, SceneInfo *sc_info)
) {
    runtime_aver *perAtom = create_runtime_aver(100000);
    runtime_aver *perSteps = create_runtime_aver(100000);
    double now = getCPUtime(), atomNow, stepNow, finish;

    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);// __attribute__((aligned(32)));
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif

    trans[3] = 0.0;

    scene->temperature = 0.0;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        stepNow = getCPUtime();

        for(size_t a = 0; a < num_of_atoms; ++a) {
            atomNow = getCPUtime();

            if(scene->frozen[a]) continue;

            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, simulation->max_translation, trans);
            TRANSLATE(pos,trans);

            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                TRANSLATE_BACK(pos,trans);
                scene->energies[a] = E1;
            }

            push_number_runtime_aver(perAtom, getCPUtime() - atomNow);
        }

        push_number_runtime_aver(perSteps, getCPUtime() - stepNow);
    }

    for(size_t i = 0; i < num_of_atoms; ++i) get_energy(i, scene);

    finish = getCPUtime();

    printf("Benchmarking\n--------------------------------\n");
    printf("Time:        %f (s)       |\n", finish - now);
    printf("Microstep:   %f (s)       |\n", get_average_value_runtime_aver(perSteps));
    printf("Translation: %f (s)       |\n", get_average_value_runtime_aver(perAtom));
    printf("TPSA:        %u (Hz/atom)      |\n", (unsigned) (1.0/ num_of_atoms / get_average_value_runtime_aver(perAtom)));
    printf("--------------------------------\n");

    destroy_runtime_aver(perAtom);
    destroy_runtime_aver(perSteps);

	FREE_ARRAY(trans);
}

void mtp_run_simulation_without_temperature_with_dumping_bench (
    MetropolisSimulation *simulation,
    size_t num_of_steps_per_atom, 
    double (*pget_energy)(int a, SceneInfo *sc_info),
    const char *dump_file,
	size_t dump_every
) {
    runtime_aver *perAtom = create_runtime_aver(100000);
    runtime_aver *perSteps = create_runtime_aver(100000);
    double now = getCPUtime(), atomNow, stepNow, finish;

    void (*gen_translation) (MetropolisSimulation *sim, int atom, double temp, double *tr);
    size_t num_of_atoms = simulation->scene->num_of_atoms;
    SceneInfo *scene = simulation->scene;
    double E1, E2, *pos, p;
	DARRAY(trans,4);// __attribute__((aligned(32)));
    #ifdef ENABLE_AVX
    __m256d mm_pos, mm_trans, mm_sum;
    #endif

    trans[3] = 0.0;

    scene->temperature = 0.0;
    if(simulation->time) gen_translation = maxwell_translation;
    else gen_translation = rnd_translation;

    for(size_t i = 0; i < num_of_steps_per_atom; ++i) {
        stepNow = getCPUtime();

        for(size_t a = 0; a < num_of_atoms; ++a) {
            atomNow = getCPUtime();

            if(scene->frozen[a]) continue;

            E1 = get_full_energy(scene, pget_energy);

            pos = scene->atom_positions + scene->atom_adresses[a];
            gen_translation(simulation, a, simulation->max_translation, trans);
            TRANSLATE(pos,trans);

            E2 = get_full_energy(scene, pget_energy);

            if(E2 > E1) {
                TRANSLATE_BACK(pos, trans);
                scene->energies[a] = E1;
            }

            push_number_runtime_aver(perAtom, getCPUtime() - atomNow);
        }
        
		
        for(size_t k = 0; k < num_of_atoms; ++k) get_energy(k, scene);
		
		if(strcmp(dump_file,"") && (i % dump_every == 0)) 
			sceneinfo_dump(scene, dump_file);

        push_number_runtime_aver(perSteps, getCPUtime() - stepNow);
    }

    finish = getCPUtime();

    printf("Benchmarking\n--------------------------------\n");
    printf("Time:        %f (s)       |\n", finish - now);
    printf("Microstep:   %f (s)       |\n", get_average_value_runtime_aver(perSteps));
    printf("Translation: %f (s)       |\n", get_average_value_runtime_aver(perAtom));
    printf("TPSA:        %u (Hz/atom)      |\n", (unsigned) (1.0/ num_of_atoms / get_average_value_runtime_aver(perAtom)));
    printf("--------------------------------\n");

    destroy_runtime_aver(perAtom);
    destroy_runtime_aver(perSteps);

	FREE_ARRAY(trans);
}