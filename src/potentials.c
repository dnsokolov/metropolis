#include "potentials.h"

/*#ifndef _WIN32
pthread_t *_threads = NULL;
#else
thread_t* _threads = NULL;
#endif*/

size_t _num_of_threads = 0;
ThreadEnergyArg *_thread_args = NULL;
thread_pool_t* _pool = NULL;
array_args_t* _array_args = NULL;
size_t _num_of_atoms = 0;
size_t _current_atom = (size_t)(-1);

void init_multithreading(size_t num_thread, SceneInfo *scene) {
	_num_of_threads = num_thread - 1;
    if(_num_of_threads <= 0) return;

	create_thread_args(scene);
	void* (*task)(void* args) = NULL;

	if (scene->potflag == LJ_FLAG) task = get_lj_energy_thread;
	if (scene->potflag == MORSE_FLAG) task = get_morse_energy_thread;
	if (scene->potflag == GUPTA_FLAG) task = get_gupta_energy_thread;
	if (scene->potflag == FS_FLAG) task = get_fs_energy_thread;
	if (scene->potflag == EXFS_FLAG) task = get_exfs_energy_thread;
	if (scene->potflag == SC_FLAG) task = get_sc_energy_thread;
	if (scene->potflag == HARM_FLAG) task = get_harm_energy_thread;
	if (scene->potflag == STEP_FLAG) task = get_step_energy_thread;
	if (task == NULL) task = get_energy_thread;

	_array_args = (array_args_t*)malloc(sizeof(array_args_t) * _num_of_threads);

	for (size_t i = 0; i < _num_of_threads; ++i) _array_args[i].data = &_thread_args[i];
	_pool = thread_pool_create(_num_of_threads, task, _array_args);
}

void destroy_multithreading() {
    _num_of_threads = 0;
	thread_pool_close(_pool);
    free(_thread_args);
}

void create_thread_args(SceneInfo *scene) {
    if(_thread_args == NULL) _thread_args = (ThreadEnergyArg *) malloc(sizeof(ThreadEnergyArg) * _num_of_threads);
    if(_num_of_atoms != scene->num_of_atoms) {
	    int remainder = scene->num_of_atoms % _num_of_threads;
        int offset = 0;
        int finish = 0;

        for (size_t i = 0; i < _num_of_threads; ++i) {
		   _thread_args[i].start_atom = finish;
           _thread_args[i].scene = scene;
           _thread_args[i].repulsive = 0.0;
           _thread_args[i].attractive = 0.0;

		    if (remainder > 0) {
			  --remainder;
			  offset = 1;
		    }
		    else {
			   offset = 0;
		    }

		    finish = _thread_args[i].start_atom + (scene->num_of_atoms / _num_of_threads) + offset;
		    _thread_args[i].finish_atom = finish;
		    _thread_args[i].thread_id = i;
	    }
    }
}

double get_lj_energy(int a, SceneInfo *c_info) {
    double e = 0.0;
    double dist, d, rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    int adress;
    int lj_adress;
    double *ljp = c_info->lj_parameters;

    for(int i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;
        if(c_info->lj_bond_matrix[adress] == 0) continue;
        lj_adress = c_info->lj_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = ljp[ lj_adress + 2];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
            d = ljp[lj_adress + 1] / dist;
            e += ljp[lj_adress] * (pow(d, 12) - pow(d,6));
        }
    }

    c_info->energies[a] = e;
    return c_info->energies[a];
}

double get_morse_energy(int a, SceneInfo *c_info) {
    double e = 0.0;
    double dist, d, rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    int adress;
    int morse_adress;
    double *morsep = c_info->morse_parameters;

    for(int i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;
        if(c_info->morse_bond_matrix[adress] == 0) continue;
        morse_adress = c_info->morse_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = morsep[ morse_adress + 3];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
           d =  (1.0 - exp(-morsep[morse_adress + 1] * (dist - morsep[morse_adress + 2])));
           e += morsep[morse_adress] * d * d;
        }
    }

    c_info->energies[a] = e;
    return c_info->energies[a];
}

double get_gupta_energy(int a, SceneInfo *c_info) {
    double repulsive = 0.0;
    double attractive = 0.0;
    double dist, d,  rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t gupta_adress;
    double *gp = c_info->gupta_parameters;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->gupta_bond_matrix[adress] == 0) continue;
        gupta_adress = c_info->gupta_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = gp[gupta_adress + 5];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
            d = dist/gp[gupta_adress] - 1.0;
            repulsive += gp[gupta_adress + 1] * exp(-gp[gupta_adress + 2] * d );
            attractive += gp[gupta_adress + 3] * gp[gupta_adress + 3] * exp(-2.0 * gp[gupta_adress + 4] * d);
        }
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_fs_energy(int a, SceneInfo *c_info) {
    double repulsive = 0.0;
    double attractive = 0.0;
    double dist, d, d2, d3, rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t fs_adress;
    double *fsp = c_info->fs_parameters;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->fs_bond_matrix[adress] == 0) continue;
        fs_adress = c_info->fs_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = fsp[fs_adress + 7];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
            if(dist < fsp[fs_adress]) {
                d = dist - fsp[fs_adress];
                repulsive += 0.5 * d * d * (fsp[fs_adress + 1] + fsp[fs_adress + 2] * dist + fsp[fs_adress + 3] * dist * dist);
            }
            if(dist < fsp[fs_adress + 4]) {
                d = dist - fsp[fs_adress + 4];
                d2 = d * d;
                d3 = d2 * d;
                attractive += fsp[fs_adress + 6] * fsp[fs_adress + 6] * (d2 + fsp[fs_adress + 5] * d3 / fsp[fs_adress + 4]);
            }
        }
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_exfs_energy(int a, SceneInfo *c_info) {
    double repulsive = 0.0;
    double attractive = 0.0;
    double dist, rcut,  x2, x3, x4, d, y2, y4;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t exfs_adress;
    double *exfsp = c_info->exfs_parameters;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->exfs_bond_matrix[adress] == 0) continue;
        exfs_adress = c_info->exfs_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = exfsp[exfs_adress + 9];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
            if(dist < exfsp[exfs_adress]) {
               d = dist - exfsp[exfs_adress];
               x2 = dist * dist;
               x3 = x2 * dist;
               x4 = x2 * x2;
               repulsive += 0.5 * d * d * (exfsp[exfs_adress + 1] + exfsp[exfs_adress + 2] * dist + exfsp[exfs_adress + 3] * x2 + exfsp[exfs_adress + 4] * x3 + exfsp[exfs_adress + 5] * x4);
            }
            if(dist < exfsp[exfs_adress + 6]) {
               y2 = (dist - exfsp[exfs_adress + 6]) * (dist - exfsp[exfs_adress + 6]);
               y4 = y2 * y2;
               attractive += exfsp[exfs_adress + 7] * exfsp[exfs_adress + 7] * (y2 + exfsp[exfs_adress + 8] * exfsp[exfs_adress + 8] * y4) ;
            }
        }
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_sc_energy(int a, SceneInfo *c_info) {
    double repulsive = 0.0;
    double attractive = 0.0;
    double dist, rcut, x;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t sc_adress;
    double *scp = c_info->sc_parameters;
    double eps, aa, n, m, c;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->sc_bond_matrix[adress] == 0) continue;
        sc_adress = c_info->sc_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = scp[sc_adress + 5];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
            eps = scp[sc_adress];
            aa = scp[sc_adress + 1];
            n = scp[sc_adress + 2];
            m = scp[sc_adress + 3];
            c = scp[sc_adress + 4];
            x = aa/dist;
            repulsive += 0.5 * eps * pow(x, n);
            attractive += c * eps * c * eps * pow(x, m);
        }
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_step_energy(int a, SceneInfo *c_info) {

    double dist, e = 0.0;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t step_adress;
    double *stepp = c_info->step_parameters;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->step_bond_matrix[adress] == 0) continue;
        step_adress = c_info->step_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        if(dist < stepp[step_adress]) {
            e += stepp[step_adress + 1];
        }
    }

    c_info->energies[a] = e;
    return c_info->energies[a];
}

double get_harm_energy(int a, SceneInfo *c_info) {

    double dist, e = 0.0, rcut, d;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t harm_adress;
    double *harmp = c_info->harm_parameters;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->harm_bond_matrix[adress] == 0) continue;
        harm_adress = c_info->harm_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = harmp[harm_adress + 2];
        if(dist < rcut || EQ_BY_EPS(rcut, 0.0)) {
           d = (dist - harmp[harm_adress + 1]);
           e += harmp[harm_adress] * d * d;
        }
    }

    c_info->energies[a] = e;
    return c_info->energies[a];
}

double get_energy(int a, SceneInfo *c_info) {
    double dist, e = 0.0, rcut, d;
    double repulsive_gupta = 0.0, attractive_gupta = 0.0;
    double repulsive_sc = 0.0, attractive_sc = 0.0;
    double repulsive_fs = 0.0, attractive_fs = 0.0;
    double repulsive_exfs = 0.0, attractive_exfs = 0.0;
    double x2, x3, x4;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;

    size_t param_adress;
    double *pp;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        dist = get_dist(i, a, c_info);

        if(c_info->potflag & LJ_FLAG) {
            if(c_info->lj_bond_matrix[adress] != 0) {
               param_adress = c_info->lj_adress_matrix[adress];
               pp = c_info->lj_parameters;
               rcut = pp[param_adress + 2];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
                  d = pp[param_adress + 1] / dist;
                  e += pp[param_adress] * (pow(d, 12) - pow(d,6));
               }
           }
        }

        if(c_info->potflag & MORSE_FLAG) {
            if(c_info->morse_bond_matrix[adress] != 0) {
               param_adress = c_info->morse_adress_matrix[adress];
               pp = c_info->morse_parameters;
               rcut = pp[param_adress + 2];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
                  d =  (1.0 - exp(-pp[param_adress + 1] * (dist - pp[param_adress + 2])));
                  e += pp[param_adress] * d * d;
               }
            }
        }

        if(c_info->potflag & STEP_FLAG) {
            if(c_info->step_bond_matrix[adress] != 0) {
               param_adress = c_info->step_adress_matrix[adress];
               pp = c_info->step_parameters;
               if(dist < pp[param_adress]) {
                  e += pp[param_adress + 1];
               }
            }
        }

        if(c_info->potflag & GUPTA_FLAG) {
            if(c_info->gupta_bond_matrix[adress] != 0) {
               param_adress = c_info->gupta_adress_matrix[adress];
               pp = c_info->gupta_parameters;
               rcut = pp[param_adress + 5];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
                   d = dist/pp[param_adress] - 1.0;
                   repulsive_gupta += pp[param_adress + 1] * exp(-pp[param_adress + 2] * d );
                   attractive_gupta += pp[param_adress + 3] * pp[param_adress + 3] * exp(-2.0 * pp[param_adress + 4] * d);
                }
            }
        }

        if(c_info->potflag & FS_FLAG) {
            if(c_info->fs_bond_matrix[adress] != 0) {
               param_adress = c_info->fs_adress_matrix[adress];
               pp = c_info->fs_parameters;
               rcut = pp[param_adress + 7];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                  if(dist < pp[param_adress]) {
                     d = dist - pp[param_adress];
                     repulsive_fs += 0.5 * d * d * (pp[param_adress + 1] + pp[param_adress + 2] * dist + pp[param_adress + 3] * dist * dist);
                  }
                  if(dist < pp[param_adress + 4]) {
                     d = dist - pp[param_adress + 4];
                     x2 = d * d;
                     x3 = x2 * d;
                     attractive_fs += pp[param_adress + 6] * pp[param_adress + 6] * (x2 + pp[param_adress + 5] * x3 / pp[param_adress + 4]);
                  }
               }
            }
        }

        if(c_info->potflag & EXFS_FLAG) {
            if(c_info->exfs_bond_matrix[adress] != 0) {
               param_adress = c_info->exfs_adress_matrix[adress];
               pp = c_info->exfs_parameters;
               rcut = pp[param_adress + 9];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                  if(dist < pp[param_adress]) {
                     d = dist - pp[param_adress];
                     x2 = dist * dist;
                     x3 = x2 * dist;
                     x4 = x2 * x2;
                     repulsive_exfs += 0.5 * d * d * (pp[param_adress + 1] + pp[param_adress + 2] * dist + pp[param_adress + 3] * x2 + pp[param_adress + 4] * x3 + pp[param_adress + 5] * x4);
                 }
                 if(dist < pp[param_adress + 6]) {
                     x2 = (dist - pp[param_adress + 6]) * (dist - pp[param_adress + 6]);
                     x4 = x2 * x2;
                     attractive_exfs += pp[param_adress + 7] * pp[param_adress + 7] * (x2 + pp[param_adress + 8] * pp[param_adress + 8] * x4) ;
                 }
               }
            }
        }

        if(c_info->potflag & SC_FLAG) {
            if(c_info->sc_bond_matrix[adress] != 0) {
               param_adress = c_info->sc_adress_matrix[adress];
               pp = c_info->sc_parameters;
               rcut = pp[param_adress + 5];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                  d = pp[param_adress + 1]/dist;
                  repulsive_sc += 0.5 * pp[param_adress] * pow(d, pp[param_adress + 2]);
                  attractive_sc += pp[param_adress + 4] * pp[param_adress] * pp[param_adress + 4] * pp[param_adress] * pow(d, pp[param_adress + 3]);
               }
            }
        }

        if(c_info->potflag & HARM_FLAG) {
            if(c_info->harm_bond_matrix[adress] != 0) {
               param_adress = c_info->harm_adress_matrix[adress];
               pp = c_info->harm_parameters;
               rcut = pp[param_adress + 2];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                   d = (dist - pp[param_adress + 1]);
                   e += pp[param_adress] * d * d;
               }
            }
        }
    }

    c_info->energies[a] = e + repulsive_gupta + repulsive_fs + repulsive_exfs + repulsive_sc 
    - sqrt(attractive_gupta) - sqrt(attractive_fs) - sqrt(attractive_exfs) - sqrt(attractive_sc);
    
    return c_info->energies[a];
}

//************************** Thread version *****************************

void* get_lj_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    double e = 0.0;
    double dist, d, rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    int adress;
    int lj_adress;
    double *ljp = c_info->lj_parameters;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;
        if(c_info->lj_bond_matrix[adress] == 0) continue;
        lj_adress = c_info->lj_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = ljp[ lj_adress + 2];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
            d = ljp[lj_adress + 1] / dist;
            e += ljp[lj_adress] * (pow(d, 12) - pow(d,6));
        }
    }

    thread_arg->energy = e;
    return NULL;
}

void* get_morse_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    double e = 0.0;
    double dist, d, rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    int adress;
    int morse_adress;
    double *morsep = c_info->morse_parameters;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;
        if(c_info->morse_bond_matrix[adress] == 0) continue;
        morse_adress = c_info->morse_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = morsep[ morse_adress + 3];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
           d =  (1.0 - exp(-morsep[morse_adress + 1] * (dist - morsep[morse_adress + 2])));
           e += morsep[morse_adress] * d * d;
        }
    }

    thread_arg->energy = e;
    return NULL;
}

void* get_gupta_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    thread_arg->repulsive = 0.0;
    thread_arg->attractive = 0.0;

    double dist, d,  rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t gupta_adress;
    double *gp = c_info->gupta_parameters;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
		//printf("thread %zu (%zu-%d)\n", thread_arg->thread_id, i, a);
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->gupta_bond_matrix[adress] == 0) continue;
        gupta_adress = c_info->gupta_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = gp[gupta_adress + 5];

        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
            d = dist/gp[gupta_adress] - 1.0;
            thread_arg->repulsive += gp[gupta_adress + 1] * exp(-gp[gupta_adress + 2] * d );
            thread_arg->attractive += gp[gupta_adress + 3] * gp[gupta_adress + 3] * exp(-2.0 * gp[gupta_adress + 4] * d);
        }
    }

    return NULL;
}

void* get_fs_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    thread_arg->repulsive = 0.0;
    thread_arg->attractive = 0.0;
    double dist, d, d2, d3, rcut;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t fs_adress;
    double *fsp = c_info->fs_parameters;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->fs_bond_matrix[adress] == 0) continue;
        fs_adress = c_info->fs_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = fsp[fs_adress + 7];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
            if(dist < fsp[fs_adress]) {
                d = dist - fsp[fs_adress];
                thread_arg->repulsive += 0.5 * d * d * (fsp[fs_adress + 1] + fsp[fs_adress + 2] * dist + fsp[fs_adress + 3] * dist * dist);
            }
            if(dist < fsp[fs_adress + 4]) {
                d = dist - fsp[fs_adress + 4];
                d2 = d * d;
                d3 = d2 * d;
                thread_arg->attractive += fsp[fs_adress + 6] * fsp[fs_adress + 6] * (d2 + fsp[fs_adress + 5] * d3 / fsp[fs_adress + 4]);
            }
        }
    }

    return NULL;
}

void* get_exfs_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    thread_arg->repulsive = 0.0;
    thread_arg->attractive = 0.0;
    double dist, rcut,  x2, x3, x4, d, y2, y4;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t exfs_adress;
    double *exfsp = c_info->exfs_parameters;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->exfs_bond_matrix[adress] == 0) continue;
        exfs_adress = c_info->exfs_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = exfsp[exfs_adress + 9];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
            if(dist < exfsp[exfs_adress]) {
               d = dist - exfsp[exfs_adress];
               x2 = dist * dist;
               x3 = x2 * dist;
               x4 = x2 * x2;
               thread_arg->repulsive += 0.5 * d * d * (exfsp[exfs_adress + 1] + exfsp[exfs_adress + 2] * dist + exfsp[exfs_adress + 3] * x2 + exfsp[exfs_adress + 4] * x3 + exfsp[exfs_adress + 5] * x4);
            }
            if(dist < exfsp[exfs_adress + 6]) {
               y2 = (dist - exfsp[exfs_adress + 6]) * (dist - exfsp[exfs_adress + 6]);
               y4 = y2 * y2;
               thread_arg->attractive += exfsp[exfs_adress + 7] * exfsp[exfs_adress + 7] * (y2 + exfsp[exfs_adress + 8] * exfsp[exfs_adress + 8] * y4) ;
            }
        }
    }

    return NULL;
}

void* get_sc_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    thread_arg->repulsive = 0.0;
    thread_arg->attractive = 0.0;
    double dist, rcut, x;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t sc_adress;
    double *scp = c_info->sc_parameters;
    double eps, aa, n, m, c;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->sc_bond_matrix[adress] == 0) continue;
        sc_adress = c_info->sc_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = scp[sc_adress + 5];
        if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
            eps = scp[sc_adress];
            aa = scp[sc_adress + 1];
            n = scp[sc_adress + 2];
            m = scp[sc_adress + 3];
            c = scp[sc_adress + 4];
            x = aa/dist;
            thread_arg->repulsive += 0.5 * eps * pow(x, n);
            thread_arg->attractive += c * eps * c * eps * pow(x, m);
        }
    }

    return NULL;
}

void* get_step_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    double dist, e = 0.0;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t step_adress;
    double *stepp = c_info->step_parameters;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->step_bond_matrix[adress] == 0) continue;
        step_adress = c_info->step_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        if(dist < stepp[step_adress]) {
            e += stepp[step_adress + 1];
        }
    }

    thread_arg->energy = e;
    return NULL;
}

void* get_harm_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    double dist, e = 0.0, rcut, d;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;
    size_t harm_adress;
    double *harmp = c_info->harm_parameters;

    for(size_t i = 0; i < num_of_atoms; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        if(c_info->harm_bond_matrix[adress] == 0) continue;
        harm_adress = c_info->harm_adress_matrix[adress];
        dist = get_dist(i, a, c_info);
        rcut = harmp[harm_adress + 2];
        if(dist < rcut || EQ_BY_EPS(rcut, 0.0)) {
           d = (dist - harmp[harm_adress + 1]);
           e += harmp[harm_adress] * d * d;
        }
    }

    thread_arg->energy = e;
    return NULL;
}

void* get_energy_thread(void *arg) {
    ThreadEnergyArg *thread_arg = (ThreadEnergyArg *) arg;
    SceneInfo *c_info = thread_arg->scene;
    int a = _current_atom;

    double dist, e = 0.0, rcut, d;

    thread_arg->repulsive_gupta = 0.0; 
    thread_arg->attractive_gupta = 0.0;
    thread_arg->repulsive_sc = 0.0; 
    thread_arg->attractive_sc = 0.0;
    thread_arg->repulsive_fs = 0.0; 
    thread_arg->attractive_fs = 0.0;
    thread_arg->repulsive_exfs = 0.0;
    thread_arg->attractive_exfs = 0.0;

    double x2, x3, x4;
    int a_type = c_info->types[a];
    int i_type;
    size_t num_of_types = c_info->num_of_types;
    size_t num_of_atoms = c_info->num_of_atoms;
    size_t adress;

    size_t param_adress;
    double *pp;

    for(size_t i = thread_arg->start_atom; i < thread_arg->finish_atom; ++i) {
        if(i == a) continue;
        i_type = c_info->types[i];
        adress = i_type * num_of_types + a_type;  
        dist = get_dist(i, a, c_info);

        if(c_info->potflag & LJ_FLAG) {
            if(c_info->lj_bond_matrix[adress] != 0) {
               param_adress = c_info->lj_adress_matrix[adress];
               pp = c_info->lj_parameters;
               rcut = pp[param_adress + 2];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
                  d = pp[param_adress + 1] / dist;
                  e += pp[param_adress] * (pow(d, 12) - pow(d,6));
               }
           }
        }

        if(c_info->potflag & MORSE_FLAG) {
            if(c_info->morse_bond_matrix[adress] != 0) {
               param_adress = c_info->morse_adress_matrix[adress];
               pp = c_info->morse_parameters;
               rcut = pp[param_adress + 2];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
                  d =  (1.0 - exp(-pp[param_adress + 1] * (dist - pp[param_adress + 2])));
                  e += pp[param_adress] * d * d;
               }
            }
        }

        if(c_info->potflag & STEP_FLAG) {
            if(c_info->step_bond_matrix[adress] != 0) {
               param_adress = c_info->step_adress_matrix[adress];
               pp = c_info->step_parameters;
               if(dist < pp[param_adress]) {
                  e += pp[param_adress + 1];
               }
            }
        }

        if(c_info->potflag & GUPTA_FLAG) {
            if(c_info->gupta_bond_matrix[adress] != 0) {
               param_adress = c_info->gupta_adress_matrix[adress];
               pp = c_info->gupta_parameters;
               rcut = pp[param_adress + 5];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut,0.0)) {
                   d = dist/pp[param_adress] - 1.0;
                   thread_arg->repulsive_gupta += pp[param_adress + 1] * exp(-pp[param_adress + 2] * d );
                   thread_arg->attractive_gupta += pp[param_adress + 3] * pp[param_adress + 3] * exp(-2.0 * pp[param_adress + 4] * d);
                }
            }
        }

        if(c_info->potflag & FS_FLAG) {
            if(c_info->fs_bond_matrix[adress] != 0) {
               param_adress = c_info->fs_adress_matrix[adress];
               pp = c_info->fs_parameters;
               rcut = pp[param_adress + 7];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                  if(dist < pp[param_adress]) {
                     d = dist - pp[param_adress];
                     thread_arg->repulsive_fs += 0.5 * d * d * (pp[param_adress + 1] + pp[param_adress + 2] * dist + pp[param_adress + 3] * dist * dist);
                  }
                  if(dist < pp[param_adress + 4]) {
                     d = dist - pp[param_adress + 4];
                     x2 = d * d;
                     x3 = x2 * d;
                     thread_arg->attractive_fs += pp[param_adress + 6] * pp[param_adress + 6] * (x2 + pp[param_adress + 5] * x3 / pp[param_adress + 4]);
                  }
               }
            }
        }

        if(c_info->potflag & EXFS_FLAG) {
            if(c_info->exfs_bond_matrix[adress] != 0) {
               param_adress = c_info->exfs_adress_matrix[adress];
               pp = c_info->exfs_parameters;
               rcut = pp[param_adress + 9];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                  if(dist < pp[param_adress]) {
                     d = dist - pp[param_adress];
                     x2 = dist * dist;
                     x3 = x2 * dist;
                     x4 = x2 * x2;
                     thread_arg->repulsive_exfs += 0.5 * d * d * (pp[param_adress + 1] + pp[param_adress + 2] * dist + pp[param_adress + 3] * x2 + pp[param_adress + 4] * x3 + pp[param_adress + 5] * x4);
                 }
                 if(dist < pp[param_adress + 6]) {
                     x2 = (dist - pp[param_adress + 6]) * (dist - pp[param_adress + 6]);
                     x4 = x2 * x2;
                     thread_arg->attractive_exfs += pp[param_adress + 7] * pp[param_adress + 7] * (x2 + pp[param_adress + 8] * pp[param_adress + 8] * x4) ;
                 }
               }
            }
        }

        if(c_info->potflag & SC_FLAG) {
            if(c_info->sc_bond_matrix[adress] != 0) {
               param_adress = c_info->sc_adress_matrix[adress];
               pp = c_info->sc_parameters;
               rcut = pp[param_adress + 5];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                  d = pp[param_adress + 1]/dist;
                  thread_arg->repulsive_sc += 0.5 * pp[param_adress] * pow(d, pp[param_adress + 2]);
                  thread_arg->attractive_sc += pp[param_adress + 4] * pp[param_adress] * pp[param_adress + 4] * pp[param_adress] * pow(d, pp[param_adress + 3]);
               }
            }
        }

        if(c_info->potflag & HARM_FLAG) {
            if(c_info->harm_bond_matrix[adress] != 0) {
               param_adress = c_info->harm_adress_matrix[adress];
               pp = c_info->harm_parameters;
               rcut = pp[param_adress + 2];
               if((rcut > 0.0 && dist <= rcut) || EQ_BY_EPS(rcut, 0.0)) {
                   d = (dist - pp[param_adress + 1]);
                   e += pp[param_adress] * d * d;
               }
            }
        }
    }

    thread_arg->energy = e;
    return NULL;
}

//**************************** Multithreading ************************

double get_lj_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);
    double e = 0.0;

    for(size_t i = 0; i < _num_of_threads; ++i) {
        e += _thread_args[i].energy;
    }

    c_info->energies[a] = e;
    return e;
}

double get_morse_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double e = 0.0;
    for(size_t i = 0; i < _num_of_threads; ++i) {
        e += _thread_args[i].energy;
    }

    c_info->energies[a] = e;
    return e;
}

double get_gupta_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    

    double repulsive = 0.0;
    double attractive = 0.0;
   // printf("ok\n");

    for(size_t i = 0; i < _num_of_threads; ++i) {
        
        repulsive += _thread_args[i].repulsive;
        attractive += _thread_args[i].attractive;
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_fs_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double repulsive = 0.0;
    double attractive = 0.0;

    for(size_t i = 0; i < _num_of_threads; ++i) {
        repulsive += _thread_args[i].repulsive;
        attractive += _thread_args[i].attractive;
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_exfs_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double repulsive = 0.0;
    double attractive = 0.0;

    for(size_t i = 0; i < _num_of_threads; ++i) {
        repulsive += _thread_args[i].repulsive;
        attractive += _thread_args[i].attractive;
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_sc_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double repulsive = 0.0;
    double attractive = 0.0;

    for(size_t i = 0; i < _num_of_threads; ++i) {
        repulsive += _thread_args[i].repulsive;
        attractive += _thread_args[i].attractive;
    }

    c_info->energies[a] = repulsive - sqrt(attractive);
    return c_info->energies[a];
}

double get_step_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double e = 0.0;
    for(size_t i = 0; i < _num_of_threads; ++i) {
        e += _thread_args[i].energy;
    }

    c_info->energies[a] = e;
    return e;
}

double get_harm_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double e = 0.0;
    for(size_t i = 0; i < _num_of_threads; ++i) {
        e += _thread_args[i].energy;
    }

    c_info->energies[a] = e;
    return e;
}

double get_energy_multhreading(int a, SceneInfo *c_info) {
	_current_atom = a;
	thread_pool_do_work(_pool);

    double e = 0.0;
    double repulsive_gupta = 0.0;
    double attractive_gupta = 0.0;
    double repulsive_fs = 0.0;
    double attractive_fs = 0.0;
    double repulsive_exfs = 0.0;
    double attractive_exfs = 0.0;
    double repulsive_sc = 0.0;
    double attractive_sc = 0.0;

    for(size_t i = 0; i < _num_of_threads; ++i) {
        e += _thread_args[i].energy;
        repulsive_gupta += _thread_args[i].repulsive_gupta;
        repulsive_fs += _thread_args[i].repulsive_fs;
        repulsive_exfs += _thread_args[i].repulsive_exfs;
        repulsive_sc+= _thread_args[i].repulsive_sc;
        attractive_gupta += _thread_args[i].attractive_gupta;
        attractive_fs += _thread_args[i].attractive_fs;
        attractive_exfs += _thread_args[i].attractive_exfs;
        attractive_sc += _thread_args[i].attractive_sc;
    }

    c_info->energies[a] = e + repulsive_gupta + repulsive_sc + repulsive_fs + repulsive_exfs -
                          sqrt(attractive_gupta) - sqrt(attractive_sc) - sqrt(attractive_fs) - sqrt(attractive_exfs);
    return c_info->energies[a];
}