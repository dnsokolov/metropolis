#include "sceneinfo.h"
#include <string.h>

FileInfo *finfo_create(const char *fname, const char *attributes) {
	FileInfo *p_finfo = (FileInfo *) malloc(sizeof(FileInfo));
	p_finfo->fname = strdup(fname);
	p_finfo->fptr = fopen(fname, attributes);
	return p_finfo;
}

void finfo_destroy(FileInfo *finfo) {
	free(finfo->fname);
	fclose(finfo->fptr);
	free(finfo);
}

void finfo_rewind(FileInfo *finfo) {
	rewind(finfo->fptr);
}

double get_dist(size_t i, size_t j, const SceneInfo *sc_info) {
    double dr[] = {
        sc_info->atom_positions[sc_info->atom_adresses[i]] - sc_info->atom_positions[sc_info->atom_adresses[j]],
        sc_info->atom_positions[sc_info->atom_adresses[i] + 1] - sc_info->atom_positions[sc_info->atom_adresses[j] + 1],
        sc_info->atom_positions[sc_info->atom_adresses[i] + 2] - sc_info->atom_positions[sc_info->atom_adresses[j] + 2]
    };
    return sqrt(dr[0]*dr[0] + dr[1]*dr[1] + dr[2]*dr[2]);
} 

size_t get_num_of_frames(const char *fname) {
	size_t frames = 0;
	FILE *fp = fopen(fname,"rb");
	SceneInfo *sc = NULL;
	
	while((sc = sceneinfo_load_next(fp))) {
		++frames;
		free(sc);
		sc = NULL;
	}
	
	fclose(fp);
	return frames;
}

void sceneinfo_print(const SceneInfo *scene, FILE *out) {
    size_t adress;

    fprintf(out, "Information about the scene\n");
#ifndef  _WIN32
	fprintf(out, "Number of atoms in the cluster: %lu\n", scene->num_of_atoms);
	fprintf(out, "Number of components in the cluster: %lu\n", scene->num_of_types);
    fprintf(out, "Temperature: %f\n", scene->temperature);
#else
	fprintf(out, "Number of atoms in the cluster: %zu\n", scene->num_of_atoms);
	fprintf(out, "Number of components in the cluster: %zu\n", scene->num_of_types);
#endif // ! _WIN32

   
    fprintf(out,"\n");
    fprintf(out, "Components:\n");

    for(size_t i = 0; i < scene->num_of_types; ++i) {
#ifndef _WIN32
		fprintf(out, "Component %lu ---> %s (molar massa %f)\n", i, scene->names[i], scene->molmasses[i]);
#else
		fprintf(out, "Component %zu ---> %s (molar massa %f)\n", i, scene->names[i], scene->molmasses[i]);
#endif // !_WIN32  
    }

    fprintf(out,"\n");
    
    if(scene->potflag) {
        fprintf(out, "Potentials:\n");

        if(scene->potflag & LJ_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "lj %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->lj_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_LJ_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->lj_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & MORSE_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "morse %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->morse_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_MORSE_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->morse_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & GUPTA_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "gupt %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->gupta_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_GUPTA_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->gupta_parameters[adress + k]);
                    }            
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & FS_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "fs %s %s ", scene->names[i], scene->names[j]);
                     adress = scene->fs_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_FS_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->fs_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & EXFS_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "exfs %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->exfs_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_EXFS_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->exfs_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & SC_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "sc %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->sc_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_SC_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->sc_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & STEP_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "step %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->step_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_STEP_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->step_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }

        if(scene->potflag & HARM_FLAG) {
            for(size_t i = 0; i < scene->num_of_types; ++i) {
                for(size_t j = i; j < scene->num_of_types; ++j) {
                    fprintf(out, "harm %s %s ", scene->names[i], scene->names[j]);
                    adress = scene->harm_adress_matrix[i * scene->num_of_types + j];
                    for(size_t k = 0; k < NUM_OF_HARM_PARAMS; ++k) {
                        fprintf(out, "%f ",  scene->harm_parameters[adress + k]);
                    }
                    fprintf(out, "\n");
                }
            }
        }
    }

}

void sceneinfo_dump(const SceneInfo *scene, const char *filename) {
    FILE *fptr;
    size_t length;
	double* positions = NULL;
	size_t avx_adress, adress, *adresses;
    const char *newframe = "###frame (format developed by Sokolov Denis (devoted Sokolova V.I.))###";
    
    fptr = fopen(filename, "a+b");
    fwrite(newframe, sizeof(char), strlen(newframe) + 1, fptr);
    
    fwrite(&(scene->num_of_atoms), sizeof(size_t), 1, fptr);
    fwrite(&(scene->num_of_types), sizeof(size_t), 1, fptr);

#ifndef  ENABLE_AVX
	fwrite(scene->atom_positions, sizeof(double), 3 * scene->num_of_atoms, fptr);
    fwrite(scene->atom_adresses, sizeof(size_t), scene->num_of_atoms, fptr);
#else
	positions = (double*)malloc(3 * scene->num_of_atoms * sizeof(double));
	for (size_t i = 0; i < scene->num_of_atoms; ++i) {
		adress = (i << 1) + i;
		avx_adress = scene->atom_adresses[i];
		memcpy(positions + adress, scene->atom_positions + avx_adress, 3 * sizeof(double));
	}
	fwrite(positions, sizeof(double), 3 * scene->num_of_atoms, fptr);
	free(positions);
    
    adresses = (size_t*) malloc(sizeof(size_t) * scene->num_of_atoms);
    for(size_t i = 0; i < scene->num_of_atoms; ++i) {
        adresses[i] = (i<<1) + i;
    }
    fwrite(adresses, sizeof(size_t), scene->num_of_atoms, fptr);
    free(adresses);
#endif // ! ENABLE_AVX

    fwrite(scene->types, sizeof(int), scene->num_of_atoms, fptr);
    
    for(size_t i = 0; i < scene->num_of_types; ++i) {
        fwrite(scene->names[i], sizeof(char), strlen(scene->names[i]) + 1, fptr);
    }
    
    fwrite(scene->molmasses, sizeof(double), scene->num_of_types, fptr);
    fwrite(scene->frozen, sizeof(int), scene->num_of_atoms, fptr);
    fwrite(scene->energies, sizeof(double), scene->num_of_atoms, fptr);
    fwrite(&(scene->temperature), sizeof(double), 1, fptr);
    
    fclose(fptr);
}

SceneInfo *fi_sceneinfo_load_next(FileInfo *finfo) {
	return sceneinfo_load_next(finfo->fptr);
}

SceneInfo *sceneinfo_load_next(FILE *fptr) {
    char buffer[128];
    SceneInfo *scene;
    size_t num;
    
    num = fread(buffer, 128, 1, fptr);
    if(feof(fptr)) return NULL;
    fseek(fptr, -(128 - strlen(buffer) - 1), SEEK_CUR);
    
    scene = (SceneInfo *) malloc(sizeof(SceneInfo));
    num = fread(&(scene->num_of_atoms), sizeof(size_t), 1, fptr);
    num = fread(&(scene->num_of_types), sizeof(size_t), 1, fptr);
    #ifndef ENABLE_AVX
    scene->atom_positions = (double *) malloc(sizeof(double) * scene->num_of_atoms * 3);
    #else
    scene->atom_positions = DALIGNED_ALLOC(32, sizeof(double) * scene->num_of_atoms * 4);
    double *positions = (double *) malloc(sizeof(double) * scene->num_of_atoms * 3);
    memset(scene->atom_positions, 0, sizeof(double) * scene->num_of_atoms * 4);
    #endif
    scene->atom_adresses = (size_t *) malloc(sizeof(size_t) * scene->num_of_atoms);
    scene->types = (int *) malloc(sizeof(int) * scene->num_of_atoms);
    scene->names = (char **) malloc(sizeof(char *) * scene->num_of_types);
    scene->molmasses = (double *) malloc(sizeof(double) * scene->num_of_types);
    scene->frozen = (int *) malloc(sizeof(int) * scene->num_of_atoms);
    scene->energies = (double *) malloc(sizeof(double) * scene->num_of_atoms);
    
    #ifndef ENABLE_AVX
    num = fread(scene->atom_positions, sizeof(double), scene->num_of_atoms * 3, fptr);
    num = fread(scene->atom_adresses, sizeof(size_t), scene->num_of_atoms, fptr);
	for (size_t i = 0; i < scene->num_of_atoms; ++i) {
		scene->atom_adresses[i] = (i << 1) + i;
	}
    #else
    num = fread(positions, sizeof(double), scene->num_of_atoms * 3, fptr);
	num = fread(scene->atom_adresses, sizeof(size_t), scene->num_of_atoms, fptr);
	//num = scene->atom_adresses[1];
    for(size_t i = 0; i < scene->num_of_atoms; ++i) {
        memcpy(scene->atom_positions + (i<<2), positions + ((i<<1) + i), sizeof(double) * 3);
        scene->atom_adresses[i] = (i << 2);
    }
    free(positions);
    #endif

    num = fread(scene->types, sizeof(int), scene->num_of_atoms, fptr);
    
    for(size_t i = 0; i < scene->num_of_types; ++i) {
        num = fread(buffer, 128, 1, fptr);
        scene->names[i] = strdup(buffer);
        fseek(fptr, -(128 - strlen(buffer) - 1), SEEK_CUR);
    }
    
    num = fread(scene->molmasses, sizeof(double), scene->num_of_types, fptr);
    num = fread(scene->frozen, sizeof(int), scene->num_of_atoms, fptr);
    num = fread(scene->energies, sizeof(double), scene->num_of_atoms, fptr);
    num = fread(&(scene->temperature), sizeof(double), 1, fptr);
    
    scene->potflag = 0;
    
    scene->step_adress_matrix = NULL;
    scene->step_bond_matrix = NULL;
    scene->step_parameters = NULL;
    
    scene->harm_adress_matrix = NULL;
    scene->harm_bond_matrix = NULL;
    scene->harm_parameters = NULL;
    
    scene->lj_adress_matrix = NULL;
    scene->lj_bond_matrix = NULL;
    scene->lj_parameters = NULL;
    
    scene->morse_adress_matrix = NULL;
    scene->morse_bond_matrix = NULL;
    scene->morse_parameters = NULL;
    
    scene->fs_adress_matrix = NULL;
    scene->fs_bond_matrix = NULL;
    scene->fs_parameters = NULL;
    
    scene->exfs_adress_matrix = NULL;
    scene->exfs_bond_matrix = NULL;
    scene->exfs_parameters = NULL;
    
    scene->gupta_adress_matrix = NULL;
    scene->gupta_bond_matrix = NULL;
    scene->gupta_parameters = NULL;
    
    scene->sc_adress_matrix = NULL;
    scene->sc_bond_matrix = NULL;
    scene->sc_parameters = NULL;
    
    return scene;
}

SceneInfo *sceneinfo_load_nth(const char *fname, size_t n_frame) {
	size_t c_frame = 0;
	SceneInfo *sc = NULL;
	FILE *fp = fopen(fname,"rb");
	
	while((sc = sceneinfo_load_next(fp))) {
		if(c_frame == n_frame) {
			fclose(fp);
			return sc;
		}
		
		free(sc);
		sc = NULL;
		++c_frame;
	}
	
	fclose(fp);	
	return sc;
}

void sceneinfo_free(SceneInfo *sc_info) {
#ifndef  ENABLE_AVX
	free(sc_info->atom_positions);
#else
	FREE_ARRAY(sc_info->atom_positions);
#endif // ! ENABLE_AVX

    
    free(sc_info->atom_adresses);
    free(sc_info->types);
    
    for(size_t i = 0; i < sc_info->num_of_types; ++i) {
        free(sc_info->names[i]);
    }
    
    free(sc_info->names);
    free(sc_info->molmasses);
    free(sc_info->frozen);
    free(sc_info->energies);
    
    free(sc_info->step_adress_matrix);
    free(sc_info->step_bond_matrix);
    free(sc_info->step_parameters);
    
    free(sc_info->harm_adress_matrix);
    free(sc_info->harm_bond_matrix);
    free(sc_info->harm_parameters);
    
    free(sc_info->lj_adress_matrix);
    free(sc_info->lj_bond_matrix);
    free(sc_info->lj_parameters);
    
    free(sc_info->morse_adress_matrix);
    free(sc_info->morse_bond_matrix);
    free(sc_info->morse_parameters);
    
    free(sc_info->gupta_adress_matrix);
    free(sc_info->gupta_bond_matrix);
    free(sc_info->gupta_parameters);
    
    free(sc_info->fs_adress_matrix);
    free(sc_info->fs_bond_matrix);
    free(sc_info->fs_parameters);
    
    free(sc_info->exfs_adress_matrix);
    free(sc_info->exfs_bond_matrix);
    free(sc_info->exfs_parameters);
    
    free(sc_info->sc_adress_matrix);
    free(sc_info->sc_bond_matrix);
    free(sc_info->sc_parameters);
    
    free(sc_info);
}

double sceneinfo_get_energy(SceneInfo *sc) {
    double energy = 0.0;
    for(size_t i = 0; i < sc->num_of_atoms; ++i) {
        energy += sc->energies[i];
    }
    return energy;
}

void sceneinfo_save_to_xyz(SceneInfo *sc, const char *fname) {
    FILE *fp = fopen(fname,"w");
    fprintf(fp, "%lu\n", sc->num_of_atoms);
    fprintf(fp,"[temperature:%f columns:Energy,Freeze]\n", sc->temperature);
    for(size_t i = 0; i < sc->num_of_atoms; ++i) {
        fprintf(fp,"%s %f %f %f %f %i\n", sc->names[sc->types[i]], 
                                          sc->atom_positions[sc->atom_adresses[i]],
                                          sc->atom_positions[sc->atom_adresses[i] + 1],
                                          sc->atom_positions[sc->atom_adresses[i] + 2],
                                          sc->energies[i], sc->frozen[i]);
    }
    fclose(fp);
}

void sceneinfo_append_to_xyz(SceneInfo *sc, const char *fname) {
    FILE *fp = fopen(fname,"a");
    fprintf(fp, "%lu\n", sc->num_of_atoms);
    fprintf(fp,"[temperature:%f columns:Energy,Freeze]\n", sc->temperature);
    for(size_t i = 0; i < sc->num_of_atoms; ++i) {
        fprintf(fp,"%s %f %f %f %f %i\n", sc->names[sc->types[i]], 
                                          sc->atom_positions[sc->atom_adresses[i]],
                                          sc->atom_positions[sc->atom_adresses[i] + 1],
                                          sc->atom_positions[sc->atom_adresses[i] + 2],
                                          sc->energies[i], sc->frozen[i]);
    }
    fclose(fp);
}

SceneInfo *sceneinfo_alloc() {
    SceneInfo *sc = (SceneInfo *) malloc(sizeof(SceneInfo));
    return sc;
}