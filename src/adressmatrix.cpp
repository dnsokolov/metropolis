#include "adressmatrix.hpp"

std::ostream& operator<< (std::ostream& out, const AdressMatrix &matrix) {

    for(size_t i = 0; i < matrix.N; ++i) {
        for(size_t j = 0; j < matrix.N; ++j) {
            out << matrix.data[i * matrix.N + j] << " ";
        }
        if(i < matrix.N - 1) out << std::endl;
    }

    return out;
}