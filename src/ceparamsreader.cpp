#include "ceparamsreader.hpp"

CEParamsReader::CEParamsReader(const std::string &cefile) : _cefile(cefile) {
		std::ifstream fs(_cefile, std::ifstream::in);
		if(!fs.is_open()) return;
		std::string line, comp;
		read_line(fs, line);
		std::stringstream strstr;

		strstr.str(line);

		while(!strstr.eof()) {
			strstr >> comp; 
			if(std::find(components.begin(), components.end(), comp) == components.end())
			   components.push_back(comp);	
		}

		size_t size = components.size() * 2;
		size_t k = 0;

		for(size_t i = 0; i < size; ++i) {
			if(k >= components.size()) break;

			read_line(fs, line);
			ro[components[k]+","+components[k]] = as<double>(line)/std::sqrt(2.0);

			read_line(fs, line);
			molmas[components[k]] = as<double>(line);

			++k;
		}	

		read_line(fs, line);
		read_line(fs, line);

        size = components.size();

		for(size_t i = 0; i < size; ++i) {
			for(size_t k = i + 1; k < size; ++k) {
				read_line(fs, line);
				ro[components[i]+","+components[k]] = as<double>(line)/std::sqrt(2.0);
			}
		}

		fs.close();
}

std::string CEParamsReader::ReadTBPotential( const std::string &comp1,
					  const std::string &comp2) const {
   std::ifstream fs(_cefile, std::ifstream::in);

   if(!fs.is_open()) return ""; // add throw?

   std::string line;
   std::string result = "";
   bool flag1 = false, flag2 = false;
   int counter = 0;
   std::vector<std::string> v_result;
   
   while(!fs.eof()) {
	   read_line(fs, line);
	   
	   if(flag1 && flag2) {

		   v_result.push_back(line);
		   --counter;
		   if(counter == 0) break;
		   continue;
	   }
	   
	   if(contains_substr(line,"T-B")) {
		   flag1 = true;
		   counter = 5;
		   continue;
	   }

	   if( contains_substr(line,"//" + comp1 + "-" + comp2)|| 
	       contains_substr(line,"//" + comp2 + "-" + comp1)) {
		   flag2 = true;
		   continue;
	   }
   }
   
   if(v_result.size() == 5) {
	   result += "gupt " + comp1 + " " + comp2 + " "  ;
	   std::stringstream params;
	   double r0 = 0.0;
       auto value = ro.find(comp1+","+comp2);

       if(value != ro.end()) {
		   r0 = value->second;
	   }

	   if( value == ro.end() && comp1 != comp2 ) {
		   value = ro.find(comp2+","+comp1);
		   if(value != ro.end()) r0 = value->second;
	   }

       params << r0;
	   params << " ";
	   params << v_result[0] + " ";
	   params << v_result[2] + " ";
	   params << v_result[1] + " ";
	   params << v_result[3] + " ";
	   params << v_result[4] + " ";
	   result += params.str();
   }
   
   fs.close();			
   return result;							
}

void CEParamsReader::GetComponents(std::vector<std::string> &comps) const {
	comps = components;
}

double CEParamsReader::GetMolMass(const std::string &comp) const {
	double result = 0.0;
	auto value = molmas.find(comp);
	if(value != molmas.end()) result = value->second;
	return result;
}


