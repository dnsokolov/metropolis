#include "commands/testcommand.hpp"
#include <iostream>
#include <vector>
#include "sceneinfo.h"

#include "ceparamsreader.hpp"

std::string TestCommand::GetName() {
    return "test";
}

std::string TestCommand::GetHelp() {
    std::string help =
    "'mtp test' - command for developing and testing";
    return help;
}


void TestCommand::Run(const std::vector<std::string> &args) {
    if(args.size() > 2) {
		CEParamsReader pr(args[0]);
		std::vector<std::string> comps;
		pr.GetComponents(comps);

		std::cout << "---" << std::endl;
		
		for(const std::string &a : comps) {
			std::cout << a << " " << pr.GetMolMass(a) << std::endl;
		}
		std::cout << "---" << std::endl;
		
		std::string comp1 = args[1];
		std::string comp2 = args[2];
		
		std::string result = pr.ReadTBPotential(comp1,comp2);
		std::cout << result << std::endl;
	}  
}

