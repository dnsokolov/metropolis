#include "commands/zcommand.hpp"

std::string ZCommand::GetName() {
    return "z";
}

std::string ZCommand::GetHelp() {
    std::string help =
    "'mtp z cutradius' - get Z(t).dat for ens-file in current directory, cutradius - "
    "cutting radius";
    return help;
}

void ZCommand::Run(const std::vector<std::string> &args) {
    if(args.size() == 0) {
        std::cout << "Wrong usage. You must specifies cutradius. Example: mtp z 3.5" << std::endl;
        return;
    }

    double cutradius = as<double>(args[0]);
    std::string ensfile = "";
    std::string ext;

    std::filesystem::path current_path = std::filesystem::current_path();
    for (const auto &entry : std::filesystem::directory_iterator(current_path)) {
            if(entry.is_regular_file()) {
                ext = entry.path().extension().string();
                std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

                if(ext == ".ens") {
                    ensfile = entry.path().filename().string();
                    break;
                }
            }
    }

    if(ensfile == "") {
        std::cout << "Cannot find ens-file in current directory (" << current_path.string() << ")" << std::endl;
        return;
    }

    StatEnsemble stat(ensfile);
    CoordNumberFrameHandler coordnumber(cutradius);

    std::vector<IFrameHandler*> handlers;
    handlers.push_back((IFrameHandler *) &coordnumber);

    stat.ForEach(handlers);
    std::map<double,std::pair<double,double>> functable;
    coordnumber.GetZT(functable);

    save_data("Z(T).dat", functable);
    std::cout << "File is saved in Z(T).dat" << std::endl;
}