#include "commands/averagecommand.hpp"

std::string AverageCommand::GetName() {
    return "average";
}

std::string AverageCommand::GetHelp() {
    std::string help = 
    "'mtp average [list of numbers]' - averaging numbers in [list of numbers]";
    return help;
}

void AverageCommand::Run(const std::vector<std::string> &args) {
    if(args.size() == 0) {
        std::cout << "--------------------" << std::endl;
        std::cout << "Averaging result" << std::endl;
        std::cout << "--------------------" << std::endl;
        std::cout << "Mean  = " << 0.0 << std::endl;
        std::cout << "Sigma = " << 0.0 << std::endl;
        std::cout << "--------------------" << std::endl;
        return;
    }

    std::vector<double> values;
    for(const std::string &v: args) {
        values.push_back(as<double>(v));
    }

    double mean = 0.0, sigma = 0.0;
    average_data(values, mean, sigma);
    
    std::cout << "--------------------" << std::endl;
    std::cout << "Averaging result" << std::endl;
    std::cout << "--------------------" << std::endl;
    std::cout << "Mean  = " << mean << std::endl;
    std::cout << "Sigma = " << sigma << std::endl;
    std::cout << "--------------------" << std::endl;
}