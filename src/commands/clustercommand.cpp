#include "commands/clustercommand.hpp"


std::string ClusterCommand::GetName() {
    return "cluster";
}

std::string ClusterCommand::GetHelp() {
    std::string help =
    "'mtp cluster [filename.xyz] [frame]' - loads a cluster and launches "
     "REPL for manipulating on it. If file 'filename.xyz' is not specified - creates new empty cluster."
	 "If parameter 'frame' is not specified, then load frame number 0. For loading last frame "
	 "parameter 'frame' set to -1.";
    return help;
}

void get_help(const std::string &comand, const std::vector<IModifier *> &modifiers) {
	if(comand == "") {
		std::cout << "'exit' - exit from REPL." << std::endl << std::endl;
        std::cout << "'save [filename.xyz]' - save cluster to file 'filename.xyz'. "
        "If file is not specified, are possible two cases: 1. the cluster will be saved to the file from which it is loaded, "
        "2. crash with an error."  
		<< std::endl << std::endl; 
		
		auto end = modifiers.end();
		for(auto i = modifiers.begin(); i != end; ++i) {
			std::cout << (*i)->GetHelp() << std::endl << std::endl;
		}
	}
	else {
		if(comand == "exit") {
			std::cout << "'exit' - exit from REPL." << std::endl;
			return;
		}
		
		if(comand == "save") {
			std::cout << "'save [filename.xyz]' - save cluster to file 'filename.xyz'. "
        "If file is not specified, are possible two cases: 1. the cluster will be saved to the file from which it is loaded, "
        "2. crash with an error."  
		<< std::endl; 
			return;
		}
		
		auto end = modifiers.end();
		for(auto i = modifiers.begin(); i != end; ++i) {
			if((*i)->GetName() == comand) {
				std::cout << (*i)->GetHelp() << std::endl;
				return;
			}
		}
		
		std::cout << "Comand or modifier '" << comand << "' is not found" << std::endl;
	}
}

void ClusterCommand::Run(const std::vector<std::string> &args) {
	std::string cmdline;
	std::string filename = "";
	int frame = 0;
	size_t time = 0;
	std::vector<std::string> rlines;
	size_t rstr_number = 0;
	//std::ifstream input = std::cin;
	
	if(args.size() >= 1) filename = args[0];
	if(args.size() >= 2) frame = as<int>(args[1]);
	
	Cluster cluster;
	
	std::cout << "---------------------------" << std::endl;
	std::cout << "         CLUSTER REPL" << std::endl;
	std::cout << "(C) Sokolov Denis 2019-2020" << std::endl;
	std::cout << "---------------------------" << std::endl << std::endl;
	
	std::cout << "Enter 'help' for more information" << std::endl;
	std::cout << "Enter 'exit' for exit" << std::endl;
	std::cout << std::endl;
	
	if(std::filesystem::exists(filename)) {
		cluster.LoadFromFile(filename, frame);
		std::cout << "Cluster is loaded from file '" << filename << "'" << std::endl;
		std::cout << "Frame: " << frame << std::endl;
	}
	else {
		filename = "";
		std::cout << "Empty cluster is created" << std::endl; 
	}
	
	std::vector<std::string> cmds;
	std::vector<IModifier *> modifiers;
	int result;
	
	LoadModifier loadm;
	InfoModifier infom;
	DeleteModifier delm;
	SetModifier setm(pr);
	EqModifier eqm;
	
	modifiers.push_back((IModifier*)&loadm);
	modifiers.push_back((IModifier*)&infom);
	modifiers.push_back((IModifier*)&delm);
	modifiers.push_back((IModifier*)&setm);
	modifiers.push_back((IModifier*)&eqm);
	
	std::cout << std::endl << "Enter command or modifier name" << std::endl << std::endl;
	do {
		
		if(time == 0) {
		   std::cout << "% ";
		   std::getline(std::cin, cmdline);
		} 
		else {
			
			if(rlines.size() == 0) {
				time = 0;
				continue;
			}
			
			cmdline = rlines[rstr_number];
			std::cout << "% " << cmdline << std::endl;
			++rstr_number;
			
			if(rstr_number == rlines.size()) {
				rstr_number = 0;
				--time;
			}
		}
		
		split(cmdline, cmds, ' ');
		
		if(cmds.size() == 0) {
			std::cout << std::endl;
			continue;
		}
		
		if(cmds[0] == "exit") {
			std::cout << std::endl;
			break;
		}
		
		if(cmds[0] == "help") {
			std::string cmd_name = cmds.size() >= 2 ? cmds[1] : "";
            get_help(cmd_name, modifiers);
			std::cout << std::endl;
            continue; 
		}
		
		if(cmds[0] == "save") {
			std::string fname = cmds.size() >= 2 ? cmds[1] : filename;
			if(fname == "") {
				std::cout << "File name is not specified. Example: % save somefilename.xyz" << std::endl << std::endl;
				continue;
			}
			cluster.RefreshComment(true,true);
			cluster.SaveToFile(fname);
			std::cout << "Cluster is saved in '" << fname << "'" << std::endl;
			if(filename == "") filename = fname;
			std::cout << std::endl;
			continue;
		}
		
		if(cmds[0] == "repeat") {
			if(cmds.size() == 1) {
				std::cout << "The number of repetitions is not specified. Example: '% repeat 10' - repeate 10 times." << std::endl<< std::endl;
				continue;
			}
			
			time = as<size_t>(cmds[1]);
			std::string rline;
			rlines.clear();
			
			do {
				std::cout << "... ";
				std::getline(std::cin, rline);
				
				if(rline == "") break;
				rlines.push_back(rline);
				
			} while (true);
			std::cout << std::endl;
			
			continue;
		}
		
		auto end_modifiers = modifiers.end();
		result = -1;
		
		for(auto modifier = modifiers.begin(); modifier != end_modifiers; ++modifier) {
			if((*modifier)->GetName() == cmds[0]) {
				cmds.erase(cmds.begin());
				result = (*modifier)->Apply(cluster, cmds);
				
				if(time > 0 && result != 0) {
					time = 0;
					std::cout << "Modifier return '" << result << "'" << std::endl;
				}
				
				if(cmds[0] == "load") filename = cluster.GetFileName();
				break;
			}
		}
		
		if(result == -1) {
			std::cout << "Modifier '" << cmds[0] << "' is not found" << std::endl;
		}
		
		std::cout << std::endl;
		cmds.clear();
		
	} while (true);
}

