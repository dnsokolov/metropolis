#include "commands/caloriccommand.hpp"
#include "statensemble.hpp"
#include <filesystem>

std::string CaloricCommand::GetName() {
    return "caloric";
}

std::string CaloricCommand::GetHelp() {
    std::string help =
    "'mtp caloric filename.ens [output.dat]' - calculating and saving caloric curve from ensemble file."
	" If file 'output.dat' is specified, caloric curve will save in it.";
    return help;
}


void CaloricCommand::Run(const std::vector<std::string> &args) {
	if(args.size() == 0) {
		std::cout << "Not specified ens-file" << std::endl;
		std::cout << "Usage command:" << std::endl;
		std::cout << GetHelp() << std::endl;
		return;
	}
	
	StatEnsemble ens(args[0]);
	
	if(args.size() == 1) {
		if(!std::filesystem::exists(args[0])) {
			std::cout << "File '" << args[0] << "' is not exists" << std::endl;
            return; 
		}
		
		std::filesystem::path file_path(args[0]);
		std::string datfilename = file_path.replace_extension("").string() + "_caloric.dat";
		
		std::cout << "Saving caloric curve to '" << datfilename << "'..." << std::endl;
		ens.SaveCaloricCurve(datfilename);
		return;
	}
	
	if(args.size() > 1) {
		if(!std::filesystem::exists(args[0])) {
			std::cout << "File '" << args[0] << "' is not exists" << std::endl;
            return; 
		}
		
		std::string datfilename = args[1];
		
		std::cout << "Saving caloric curve to '" << datfilename << "'..." << std::endl;
		ens.SaveCaloricCurve(datfilename);
		return;
	}
}

