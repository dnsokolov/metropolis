#include "commands/newcommand.hpp"
#include "cluster.hpp"
#include "mtpsimulationconfig.hpp"
#include <algorithm>

std::string NewCommand::GetName() {
    return "new";
}

std::string NewCommand::GetHelp() {
    std::string help =
    "'mtp new [list of xyz-files]' - initilize metropolis simulation in current directory."
    " If directory contains xyz-files: '1.xyz', '2.xyz', '3.xyz', then "
    "command create configuration files: 'config_1.toml', 'config_2.toml', 'config_3.toml'. " 
    "If set [list of xyz-files], then command create configuration files from this list.";
    return help;
}


void NewCommand::Run(const std::vector<std::string> &args) {
    std::vector<std::string> files;
    bool cemode = false;
    std::filesystem::path current_path = std::filesystem::current_path();
    std::string ext, cefile = "";

    if(args.empty()) { 
        for (const auto &entry : std::filesystem::directory_iterator(current_path)) {
            if(entry.is_regular_file()) {
                ext = entry.path().extension().string();
                std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

                if(ext == ".xyz") {
                    files.push_back(entry.path().filename().string());
                }
            }
        }
    }
    else {
        for(const std::string &a : args) {
            if(a == "ce") {
                cemode = true;
                continue;
            }
        }

        if(args.size() == 1 && cemode) {
        for (const auto &entry : std::filesystem::directory_iterator(current_path)) {
            if(entry.is_regular_file()) {
                ext = entry.path().extension().string();
                std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

                if(cefile == "" && ext == ".dat") {
                    cefile = entry.path().filename().string();
                }

                if(ext == ".xyz") {
                    files.push_back(entry.path().filename().string());
                }
            }
        }
        }

        if(args.size() > 1 && cemode) {
        for (const auto &entry : std::filesystem::directory_iterator(current_path)) {
            if(entry.is_regular_file()) {
                ext = entry.path().extension().string();
                std::transform(ext.begin(), ext.end(), ext.begin(), ::tolower);

                if(cefile == "" && ext == ".dat") {
                    cefile = entry.path().filename().string();
                }
            }
        }

        for(const std::string &a : args) {
            if(a != "ce") {
                files.push_back(a);
            }
        }
        }

        if(!cemode) files = args;
    }

    auto end = files.end();
    std::string filename;
    std::filesystem::path file_path;
    Cluster cluster;
    MTPSimulationConfig config;
    std::string config_file;
    CEParamsReader *reader = nullptr;

    if(cemode && cefile != "") {
        reader = new CEParamsReader(cefile);
        std::cout << "'Cluster Evolution' loader is enabled" << std::endl;
    }

    for(auto i = files.begin(); i != end; ++i) {
        if(std::filesystem::exists(*i)) {
            std::cout << std::endl;
            std::cout << "Generating config for '" << *i << "'..." << std::endl;
            file_path = *i;
            filename = file_path.replace_extension("").string();
            config.directory = filename;
            config.xyz_dump_file = filename + "_dump.xyz";
            config.ans_dump_file = filename + "_dump.ens";
			config.caloric_file = filename + "_caloric.dat";
            config.load_cluster = *i;  
            cluster.LoadFromFile(*i);

            if(cemode && reader != nullptr) {
                config.FillMolMassesAndPotentials(cluster, *reader);
            }
            else {
                config.FillMolMassesAndPotentials(cluster, *pr);
            }
            
            config_file = "config_" + filename + ".toml";
            config.SaveTomlFile(config_file);
            std::cout << "'" << config_file <<"' is generated" << std::endl;
            std::cout << std::endl;
        }
        else {
            std::cout << "Can not find file '" << *i << "'" << std::endl;
        }
    }

    delete reader;
}