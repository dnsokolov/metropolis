#include "commands/runcommand.hpp"
#include "mtpsimulationconfig.hpp"

std::string RunCommand::GetName() {
    return "run";
}

std::string RunCommand::GetHelp() {
    std::string help =
    "'mtp run configfile.toml' - command for runnig metropolis simulation via configfile.toml\n"
    "'mtp run configfile.toml bench' - command for runnig metropolis simulation via configfile.toml with benchmarking";
    return help;
}

void RunCommand::Run(const std::vector<std::string> &args) {
    if(args.empty()) {
        std::cout << std::endl;
        std::cout << "Configuration file is not specified. Example: '> mtp run configfile'." << std::endl;
        std::cout << std::endl;
        return;
    }

    bool bencmarking{false};
    if(args.size() > 1 && args[1] == "bench") {
        bencmarking = true;
        std::cout << "Benchmarking mode enabled" << std::endl;
    }

    if(std::filesystem::exists(args[0])) {
        MTPSimulationConfig config;
        config.LoadFromToml(args[0]);

        if(config.IsError()) {
            return;
        }

        if(std::filesystem::exists(config.load_cluster)) {
            config.Run(bencmarking);
        }
        else {
            std::cout << std::endl;
            std::cout << "Can not find cluster file '" << config.load_cluster << "'" << std::endl;
            std::cout << std::endl;
            return;
        }
    }
    else {
        std::cout << std::endl;
        std::cout << "Can not find configuration file '" << args[0] << "'" << std::endl;
        std::cout << std::endl;
        return;
    }   
}