## Сборка программы в Linux

1. Установите CMake (в Ubuntu это можно сделать командой **sudo apt install cmake**).
2. Скачайте этот репозиторий (**git clone https://dnsokolov@bitbucket.org/dnsokolov/metropolis.git**).
3. В скаченной папке создайте папку build (**mkdir build**).
4. Перейдите в папку build (**cd build**).
5. Сгенерируйте Makefile (**cmake ..**)
6. Соберите проект (**make**).

---

## Сборка программы в Windows

1. Установите CMake ([Скачать CMake](https://cmake.org/download/)).
2. Скачайте этот репозиторий (**git clone https://dnsokolov@bitbucket.org/dnsokolov/metropolis.git**).
3. В скаченной папке создайте папку build.
4. Запустите CMake, в качестве источника выберите папку скаченного репозитория, в качестве папки для сборки выберите ранее созданную папку build.
5. Нажмите кнопку **Configure**, затем кнопку **Generate**. Для сборки проекта предполагается использование Visual Studio 2019.
6. Перейдите в папку build, найдите там сгенерированный файл **mtp.sln**. Откройте его в VS 2019. Соберите проект (используйте конфигурацию **Release**).
